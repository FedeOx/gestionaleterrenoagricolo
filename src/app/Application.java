package app;

import java.awt.EventQueue;
import view.Login;

/**
 * Classe contenente il main.
 */
public final class Application {

    private Application() { }

    /**
     * Main method.
     */
    public static void main(final String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Login();
            }
        });
    }

}
