package view.seller;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import model.Acquisto;
import model.Cliente;
import model.DettaglioAcquisto;
import model.Dipendente;
import model.Fattura;
import model.Prodotto;
import model.database.AcquistoTable;
import model.database.ClienteTable;
import model.database.DettaglioAcquistoTable;
import model.database.FatturaTable;
import model.database.ProdottoTable;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Toolkit;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.SystemColor;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import javax.swing.JList;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.SpinnerNumberModel;
import javax.swing.JCheckBox;
import javafx.util.Pair;
/**
 * DB seller main GUI.
 */
//CHECKSTYLE:OFF
@SuppressWarnings("all")
public class SellerMainWindow {
    private Prodotto selectedProduct;
    private List<Triple<Prodotto,Integer,Integer>> productsList;
    
    private Dipendente utente;
    private JTable tableClienti;
    private JTable tableProdotti;
    private JTable tableFatture;
    /**
     * Create the application.
     */
    public SellerMainWindow(Dipendente dip) {
        this.selectedProduct = null;
        this.productsList = new ArrayList<>();
        this.utente = dip;
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        JTextField textIdCliente;
        JTextField textNomeCliente;
        JTextField textCognomeCliente;
        JTextField textViaCliente;
        JTextField textCivicoCliente;
        JTextField textCittaCliente;
        JTextField textCapCliente;
        JTextField textTelCliente;
        JTextField textIdClienteSelezionato;
        JTextField textQuantita;
        JTextField textSpesaTotale;
        JTextField textClienteFatture;
        JButton btnInserisci;
        
        JFrame frame;
        frame = new JFrame("Seller Main Window");
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setBounds(0, 0,screen.width,screen.height - 30);
        frame.setExtendedState(Frame.MAXIMIZED_BOTH);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        
        JCheckBox chckbxOnlyBio = new JCheckBox("Only Bio");
        
        JLabel lblNomeUtente = new JLabel(this.utente.getNome());
        lblNomeUtente.setForeground(Color.BLUE);
        lblNomeUtente.setBounds(40, 11, 80, 14);
        frame.getContentPane().add(lblNomeUtente);
        
        JLabel lblCognomeUtente = new JLabel(this.utente.getCognome());
        lblCognomeUtente.setForeground(Color.BLUE);
        lblCognomeUtente.setBounds(130, 11, 102, 14);
        frame.getContentPane().add(lblCognomeUtente);

        JPanel panelNuovoCliente = new JPanel();
        panelNuovoCliente.setBounds(551, 59, 251, 256);
        frame.getContentPane().add(panelNuovoCliente);
        GridBagLayout gbl_panelNuovoCliente = new GridBagLayout();
        gbl_panelNuovoCliente.columnWidths = new int[]{0, 0, 0, 0, 0};
        gbl_panelNuovoCliente.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gbl_panelNuovoCliente.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_panelNuovoCliente.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        panelNuovoCliente.setLayout(gbl_panelNuovoCliente);

        JLabel lblNuovoCliente = new JLabel("Nuovo Cliente");
        GridBagConstraints gbc_lblNuovoCliente = new GridBagConstraints();
        gbc_lblNuovoCliente.insets = new Insets(0, 0, 5, 5);
        gbc_lblNuovoCliente.gridx = 1;
        gbc_lblNuovoCliente.gridy = 0;
        panelNuovoCliente.add(lblNuovoCliente, gbc_lblNuovoCliente);

        JLabel lblIdCliente = new JLabel("Id");
        GridBagConstraints gbc_lblIdCliente = new GridBagConstraints();
        gbc_lblIdCliente.insets = new Insets(0, 0, 5, 5);
        gbc_lblIdCliente.gridx = 0;
        gbc_lblIdCliente.gridy = 1;
        panelNuovoCliente.add(lblIdCliente, gbc_lblIdCliente);

        textIdCliente = new JTextField();
        GridBagConstraints gbc_textIdCliente = new GridBagConstraints();
        gbc_textIdCliente.insets = new Insets(0, 0, 5, 5);
        gbc_textIdCliente.gridx = 1;
        gbc_textIdCliente.gridy = 1;
        panelNuovoCliente.add(textIdCliente, gbc_textIdCliente);
        textIdCliente.setColumns(10);

        JLabel lblNome = new JLabel("Nome");
        GridBagConstraints gbc_lblNome = new GridBagConstraints();
        gbc_lblNome.insets = new Insets(0, 0, 5, 5);
        gbc_lblNome.gridx = 0;
        gbc_lblNome.gridy = 2;
        panelNuovoCliente.add(lblNome, gbc_lblNome);

        textNomeCliente = new JTextField();
        GridBagConstraints gbc_textNomeCliente = new GridBagConstraints();
        gbc_textNomeCliente.insets = new Insets(0, 0, 5, 5);
        gbc_textNomeCliente.gridx = 1;
        gbc_textNomeCliente.gridy = 2;
        panelNuovoCliente.add(textNomeCliente, gbc_textNomeCliente);
        textNomeCliente.setColumns(10);

        JLabel lblCognome = new JLabel("Cognome");
        GridBagConstraints gbc_lblCognome = new GridBagConstraints();
        gbc_lblCognome.insets = new Insets(0, 0, 5, 5);
        gbc_lblCognome.gridx = 0;
        gbc_lblCognome.gridy = 3;
        panelNuovoCliente.add(lblCognome, gbc_lblCognome);

        textCognomeCliente = new JTextField();
        GridBagConstraints gbc_textCognomeCliente = new GridBagConstraints();
        gbc_textCognomeCliente.insets = new Insets(0, 0, 5, 5);
        gbc_textCognomeCliente.gridx = 1;
        gbc_textCognomeCliente.gridy = 3;
        panelNuovoCliente.add(textCognomeCliente, gbc_textCognomeCliente);
        textCognomeCliente.setColumns(10);

        JLabel lblVia = new JLabel("Via");
        GridBagConstraints gbc_lblVia = new GridBagConstraints();
        gbc_lblVia.insets = new Insets(0, 0, 5, 5);
        gbc_lblVia.gridx = 0;
        gbc_lblVia.gridy = 4;
        panelNuovoCliente.add(lblVia, gbc_lblVia);

        textViaCliente = new JTextField();
        GridBagConstraints gbc_textViaCliente = new GridBagConstraints();
        gbc_textViaCliente.insets = new Insets(0, 0, 5, 5);
        gbc_textViaCliente.gridx = 1;
        gbc_textViaCliente.gridy = 4;
        panelNuovoCliente.add(textViaCliente, gbc_textViaCliente);
        textViaCliente.setColumns(10);

        JLabel lblNCivico = new JLabel("N.Civico");
        GridBagConstraints gbc_lblNCivico = new GridBagConstraints();
        gbc_lblNCivico.insets = new Insets(0, 0, 5, 5);
        gbc_lblNCivico.gridx = 0;
        gbc_lblNCivico.gridy = 5;
        panelNuovoCliente.add(lblNCivico, gbc_lblNCivico);

        textCivicoCliente = new JTextField();
        GridBagConstraints gbc_textCivicoCliente = new GridBagConstraints();
        gbc_textCivicoCliente.insets = new Insets(0, 0, 5, 5);
        gbc_textCivicoCliente.gridx = 1;
        gbc_textCivicoCliente.gridy = 5;
        panelNuovoCliente.add(textCivicoCliente, gbc_textCivicoCliente);
        textCivicoCliente.setColumns(10);

        JLabel lblCitt = new JLabel("Citt\u00E0");
        GridBagConstraints gbc_lblCitt = new GridBagConstraints();
        gbc_lblCitt.insets = new Insets(0, 0, 5, 5);
        gbc_lblCitt.gridx = 0;
        gbc_lblCitt.gridy = 6;
        panelNuovoCliente.add(lblCitt, gbc_lblCitt);

        textCittaCliente = new JTextField();
        GridBagConstraints gbc_textCittaCliente = new GridBagConstraints();
        gbc_textCittaCliente.insets = new Insets(0, 0, 5, 5);
        gbc_textCittaCliente.gridx = 1;
        gbc_textCittaCliente.gridy = 6;
        panelNuovoCliente.add(textCittaCliente, gbc_textCittaCliente);
        textCittaCliente.setColumns(10);

        JLabel lblCap = new JLabel("CAP");
        GridBagConstraints gbc_lblCap = new GridBagConstraints();
        gbc_lblCap.insets = new Insets(0, 0, 5, 5);
        gbc_lblCap.gridx = 0;
        gbc_lblCap.gridy = 7;
        panelNuovoCliente.add(lblCap, gbc_lblCap);

        textCapCliente = new JTextField();
        GridBagConstraints gbc_textCapCliente = new GridBagConstraints();
        gbc_textCapCliente.insets = new Insets(0, 0, 5, 5);
        gbc_textCapCliente.gridx = 1;
        gbc_textCapCliente.gridy = 7;
        panelNuovoCliente.add(textCapCliente, gbc_textCapCliente);
        textCapCliente.setColumns(10);

        JLabel lblTelefono = new JLabel("Telefono");
        GridBagConstraints gbc_lblTelefono = new GridBagConstraints();
        gbc_lblTelefono.insets = new Insets(0, 0, 5, 5);
        gbc_lblTelefono.gridx = 0;
        gbc_lblTelefono.gridy = 8;
        panelNuovoCliente.add(lblTelefono, gbc_lblTelefono);

        textTelCliente = new JTextField();
        GridBagConstraints gbc_textTelCliente = new GridBagConstraints();
        gbc_textTelCliente.insets = new Insets(0, 0, 5, 5);
        gbc_textTelCliente.gridx = 1;
        gbc_textTelCliente.gridy = 8;
        panelNuovoCliente.add(textTelCliente, gbc_textTelCliente);
        textTelCliente.setColumns(10);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(40, 59, 439, 287);
        frame.getContentPane().add(scrollPane);

        JLabel lblNewLabel = new JLabel("CLIENTI");
        scrollPane.setColumnHeaderView(lblNewLabel);

        JLabel lblClienti = new JLabel("CLIENTI");
        lblClienti.setBounds(40, 47, 46, 14);
        frame.getContentPane().add(lblClienti);

        JScrollPane scrollPane_1 = new JScrollPane();
        scrollPane_1.setBounds(40, 382, 439, 287);
        frame.getContentPane().add(scrollPane_1);

        JLabel lblCatalogoProdotti = new JLabel("CATALOGO PRODOTTI");
        lblCatalogoProdotti.setBounds(40, 370, 129, 14);
        frame.getContentPane().add(lblCatalogoProdotti);
        
        JPanel panelDatiAcquisto = new JPanel();
        panelDatiAcquisto.setBounds(551, 382, 179, 171);
        frame.getContentPane().add(panelDatiAcquisto);
        GridBagLayout gbl_panelDatiAcquisto = new GridBagLayout();
        gbl_panelDatiAcquisto.columnWidths = new int[]{0, 0, 0};
        gbl_panelDatiAcquisto.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
        gbl_panelDatiAcquisto.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
        gbl_panelDatiAcquisto.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        panelDatiAcquisto.setLayout(gbl_panelDatiAcquisto);
        
        JLabel lblDatiAcquisto = new JLabel("Dati Acquisto");
        GridBagConstraints gbc_lblDatiAcquisto = new GridBagConstraints();
        gbc_lblDatiAcquisto.insets = new Insets(0, 0, 5, 0);
        gbc_lblDatiAcquisto.gridx = 1;
        gbc_lblDatiAcquisto.gridy = 0;
        panelDatiAcquisto.add(lblDatiAcquisto, gbc_lblDatiAcquisto);
        
        JLabel lblIdClienteSelezionato = new JLabel("Id Cliente");
        GridBagConstraints gbc_lblIdClienteSelezionato = new GridBagConstraints();
        gbc_lblIdClienteSelezionato.insets = new Insets(0, 0, 5, 5);
        gbc_lblIdClienteSelezionato.gridx = 0;
        gbc_lblIdClienteSelezionato.gridy = 1;
        panelDatiAcquisto.add(lblIdClienteSelezionato, gbc_lblIdClienteSelezionato);
        
        textIdClienteSelezionato = new JTextField();
        GridBagConstraints gbc_textIdClienteSelezionato = new GridBagConstraints();
        gbc_textIdClienteSelezionato.insets = new Insets(0, 0, 5, 0);
        gbc_textIdClienteSelezionato.gridx = 1;
        gbc_textIdClienteSelezionato.gridy = 1;
        panelDatiAcquisto.add(textIdClienteSelezionato, gbc_textIdClienteSelezionato);
        textIdClienteSelezionato.setColumns(10);
        textIdClienteSelezionato.setEditable(false);
        
        JLabel lblQuantita = new JLabel("Quantita");
        GridBagConstraints gbc_lblQuantita = new GridBagConstraints();
        gbc_lblQuantita.insets = new Insets(0, 0, 5, 5);
        gbc_lblQuantita.gridx = 0;
        gbc_lblQuantita.gridy = 2;
        panelDatiAcquisto.add(lblQuantita, gbc_lblQuantita);
        
        textQuantita = new JTextField();
        GridBagConstraints gbc_textQuantita = new GridBagConstraints();
        gbc_textQuantita.insets = new Insets(0, 0, 5, 0);
        gbc_textQuantita.gridx = 1;
        gbc_textQuantita.gridy = 2;
        panelDatiAcquisto.add(textQuantita, gbc_textQuantita);
        textQuantita.setColumns(10);
        
        JLabel lblLista = new JLabel("Lista");
        lblLista.setBounds(762, 404, 46, 14);
        frame.getContentPane().add(lblLista);
        
        JScrollPane scrollPane_2 = new JScrollPane();
        scrollPane_2.setBounds(758, 417, 179, 117);
        frame.getContentPane().add(scrollPane_2);
        
        JList<String> listProdotti = new JList<>();
        scrollPane_2.setViewportView(listProdotti);
        DefaultListModel<String> listProdottiModel = new DefaultListModel<>();
        
        textSpesaTotale = new JTextField();
        textSpesaTotale.setBackground(new Color(173, 255, 47));
        textSpesaTotale.setBounds(851, 533, 86, 20);
        frame.getContentPane().add(textSpesaTotale);
        textSpesaTotale.setColumns(10);
        textSpesaTotale.setEditable(false);
        
        JLabel lblSpesa = new JLabel("Spesa");
        lblSpesa.setBounds(801, 537, 46, 14);
        frame.getContentPane().add(lblSpesa);
        
        JLabel lblSconto = new JLabel("Sconto*");
        GridBagConstraints gbc_lblSconto = new GridBagConstraints();
        gbc_lblSconto.insets = new Insets(0, 0, 5, 5);
        gbc_lblSconto.gridx = 0;
        gbc_lblSconto.gridy = 3;
        panelDatiAcquisto.add(lblSconto, gbc_lblSconto);
        
        JLabel lblFatture = new JLabel("FATTURE");
        lblFatture.setBounds(859, 35, 64, 14);
        frame.getContentPane().add(lblFatture);
        
        textClienteFatture = new JTextField();
        textClienteFatture.setBounds(1023, 331, 86, 20);
        frame.getContentPane().add(textClienteFatture);
        textClienteFatture.setColumns(10);
        
        JLabel lblCliente = new JLabel("IdCliente:");
        lblCliente.setBounds(963, 334, 64, 14);
        frame.getContentPane().add(lblCliente);

        chckbxOnlyBio.setBounds(489, 603, 97, 23);
        frame.getContentPane().add(chckbxOnlyBio);
        
        JScrollPane scrollPane_3 = new JScrollPane();
        scrollPane_3.setBounds(856, 59, 389, 256);
        frame.getContentPane().add(scrollPane_3);
        
        JScrollPane scrollPane_4 = new JScrollPane();
        scrollPane_4.setBounds(963, 394, 280, 218);
        frame.getContentPane().add(scrollPane_4);
        
        JList<String> listTop10 = new JList<>();
        scrollPane_4.setViewportView(listTop10);
        
        JLabel lblListaProdottiPi = new JLabel("Lista prodotti pi\u00F9 venduti");
        lblListaProdottiPi.setBounds(963, 382, 167, 14);
        frame.getContentPane().add(lblListaProdottiPi);

        JSpinner spinnerSconto = new JSpinner();
        GridBagConstraints gbc_spinner = new GridBagConstraints();
        gbc_spinner.insets = new Insets(0, 0, 5, 0);
        gbc_spinner.gridx = 1;
        gbc_spinner.gridy = 3;
        panelDatiAcquisto.add(spinnerSconto, gbc_spinner);
        spinnerSconto.setModel(new SpinnerNumberModel(0, 0, 100, 1));
        ((DefaultEditor) spinnerSconto.getEditor()).getTextField().setEditable(false);
        
        this.tableClienti = new JTable();
        this.tableClienti.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent e) {
                if(SellerMainWindow.this.productsList.isEmpty()) {
                    final JTable jTable= (JTable)e.getSource();
                    final int row = jTable.getSelectedRow();
                    final String valueInCell = jTable.getValueAt(row, 0).toString();
                    textIdClienteSelezionato.setText(valueInCell);
                }
            }
        });
        scrollPane.setViewportView(this.tableClienti);
        
        this.tableProdotti = new JTable();
        this.tableProdotti.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent e) {
                final JTable jTable= (JTable)e.getSource();
                final int row = jTable.getSelectedRow();
                SellerMainWindow.this.selectedProduct = ProdottoTable.getTable().findByPrimaryKey(Integer.parseInt(jTable.getValueAt(row, 0).toString()));
                textQuantita.setEnabled(true);
                spinnerSconto.setEnabled(true);
            }
        });
        scrollPane_1.setViewportView(this.tableProdotti);
        
        JButton btnVisClienti = new JButton("Visualizza");
        btnVisClienti.addActionListener(al -> printAllClienti());
        btnVisClienti.setBackground(SystemColor.activeCaption);
        btnVisClienti.setBounds(489, 316, 102, 30);
        frame.getContentPane().add(btnVisClienti);

        JButton btnNuovoCliente = new JButton("Nuovo");
        btnNuovoCliente.addActionListener(al -> switchComponents(panelNuovoCliente, true));
        btnNuovoCliente.setBackground(SystemColor.activeCaption);
        btnNuovoCliente.setBounds(700, 316, 102, 30);
        frame.getContentPane().add(btnNuovoCliente);

        btnInserisci = new JButton("Inserisci");
        btnInserisci.setBackground(SystemColor.desktop);
        btnInserisci.addActionListener(al -> {
            Cliente cliente = new Cliente();
            try {
                cliente.setIdCliente(Integer.parseInt(textIdCliente.getText()));
                cliente.setNome(textNomeCliente.getText());
                cliente.setCognome(textCognomeCliente.getText());
                cliente.setVia(textViaCliente.getText());
                cliente.setNumero(textCivicoCliente.getText());
                cliente.setCitta(textCittaCliente.getText());
                cliente.setCap(textCapCliente.getText());
                cliente.setTelefono(textTelCliente.getText());
                String res = ClienteTable.getTable().persist(cliente);
                printAllClienti();
                switchComponents(panelNuovoCliente, false);
                JOptionPane.showMessageDialog(frame, res);
            } catch (NumberFormatException e){
                JOptionPane.showMessageDialog(frame, "Errore: nella casella IdCliente va inserito un numero!");
            }
        });
        GridBagConstraints gbc_btnInserisci = new GridBagConstraints();
        gbc_btnInserisci.insets = new Insets(0, 0, 0, 5);
        gbc_btnInserisci.gridx = 1;
        gbc_btnInserisci.gridy = 9;
        panelNuovoCliente.add(btnInserisci, gbc_btnInserisci);

        JButton btnClear = new JButton("Clear");
        btnClear.setBackground(SystemColor.desktop);
        btnClear.addActionListener(al -> clearAllTextField(panelNuovoCliente));
        GridBagConstraints gbc_btnClear = new GridBagConstraints();
        gbc_btnClear.insets = new Insets(0, 0, 5, 5);
        gbc_btnClear.gridx = 2;
        gbc_btnClear.gridy = 1;
        panelNuovoCliente.add(btnClear, gbc_btnClear);
        
        JButton btnVisProdotti = new JButton("Visualizza");
        btnVisProdotti.setBackground(SystemColor.activeCaption);
        btnVisProdotti.addActionListener(al -> printAllProdotti(chckbxOnlyBio.isSelected()));
        btnVisProdotti.setBounds(489, 629, 102, 30);
        frame.getContentPane().add(btnVisProdotti);
        frame.setVisible(true);
        JButton btnRimuovi = new JButton("Rimuovi");
        btnRimuovi.setBackground(SystemColor.desktop);
        btnRimuovi.addActionListener(al -> {
            if (!listProdotti.isSelectionEmpty()) {
                String selectedItem = listProdotti.getSelectedValue().split(" - ")[0];
                Triple<Prodotto,Integer,Integer> prod = SellerMainWindow.this.productsList.stream().filter(p -> p.getLeft().getIdProdotto() == Integer.parseInt(selectedItem)).findFirst().get();
                SellerMainWindow.this.productsList.remove(prod);
                DefaultListModel<String> model = (DefaultListModel<String>) listProdotti.getModel();
                model.remove(listProdotti.getSelectedIndex());
                listProdotti.setModel(model);
            }
        });
        btnRimuovi.setBounds(851, 395, 89, 23);
        frame.getContentPane().add(btnRimuovi);
        
        JButton btnInserisciProd = new JButton("Inserisci");
        btnInserisciProd.setBackground(SystemColor.desktop);
        btnInserisciProd.addActionListener(al -> {
            if(textIdClienteSelezionato.getText().length() == 0 || textQuantita.getText().length() == 0){
                JOptionPane.showMessageDialog(frame, "ERROR: Mancanza di dati");
            } else {
                try {
                    this.productsList.add(new ImmutableTriple<>(this.selectedProduct, Integer.parseInt(textQuantita.getText()), Integer.parseInt(spinnerSconto.getValue().toString())));
                    listProdottiModel.addElement(this.selectedProduct.getIdProdotto() + " - Nome: " + this.selectedProduct.getNome() + " - Prezzo: "
                            + this.selectedProduct.getPrezzo() + " - Quantità: " + textQuantita.getText() + " - Sconto: " + spinnerSconto.getValue());
                    listProdotti.setModel(listProdottiModel);
                    textQuantita.setText("");
                    textQuantita.setEnabled(false);
                    spinnerSconto.setValue(0);
                    spinnerSconto.setEnabled(false);
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(frame, "Il campo quantità deve contentere un numero");
                }
            }
        });
        GridBagConstraints gbc_btnInserisciProd = new GridBagConstraints();
        gbc_btnInserisciProd.insets = new Insets(0, 0, 5, 0);
        gbc_btnInserisciProd.gridx = 1;
        gbc_btnInserisciProd.gridy = 4;
        panelDatiAcquisto.add(btnInserisciProd, gbc_btnInserisciProd);
        
        JButton btnInserisciAcquisto = new JButton("Registra acquisto");
        btnInserisciAcquisto.addActionListener(al -> {
            if(this.productsList.isEmpty()){
                JOptionPane.showMessageDialog(frame, "Registrazione impossibile: inserire almeno un prodotto");
            } else {
                double prezzoTotale = 0.0;
                int idAcquisto;
                Date actualDate = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                sdf.format(actualDate);
                for(Triple<Prodotto,Integer,Integer> tripla : this.productsList) {
                    if(tripla.getLeft().getScorte() < tripla.getMiddle()){
                        JOptionPane.showMessageDialog(frame, "Registrazione impossibile: insufficienti scorte per il prodotto: " + tripla.getLeft().getIdProdotto());
                        return;
                    }
                    prezzoTotale += (tripla.getLeft().getPrezzo() * (1.0 - (double)tripla.getRight()/100)) * (double)tripla.getMiddle(); //Calcolo del prezzo totale
                }
                //Creazione Acquisto.
                prezzoTotale = roundCents(prezzoTotale);
                textSpesaTotale.setText(Objects.toString(prezzoTotale));
                Acquisto acquisto = new Acquisto();
                idAcquisto = AcquistoTable.getTable().getMaxId() + 1; //Gli id degli acquisti partono da 1.
                acquisto.setIdAcquisto(idAcquisto);
                acquisto.setData(new Date());
                acquisto.setPrezzoTotale(prezzoTotale);
                acquisto.setIdDipendente(this.utente.getIdDipendente());
                acquisto.setIdCliente(Integer.parseInt(textIdClienteSelezionato.getText()));
                String res = AcquistoTable.getTable().persist(acquisto);
                
                //Creazione Dettagli_Acquisti.
                for(Triple<Prodotto,Integer,Integer> tripla : this.productsList) {
                    DettaglioAcquisto dett_acq = new DettaglioAcquisto();
                    dett_acq.setIdAcquisto(idAcquisto);
                    dett_acq.setIdProdotto(tripla.getLeft().getIdProdotto());
                    dett_acq.setQuantita(tripla.getMiddle());
                    dett_acq.setPrezzoUnitario(tripla.getLeft().getPrezzo());
                    dett_acq.setPrezzoTotale(tripla.getLeft().getPrezzo() * tripla.getMiddle());
                    dett_acq.setSconto(Optional.ofNullable(tripla.getRight()));
                    DettaglioAcquistoTable.getTable().persist(dett_acq);
                    tripla.getLeft().setScorte(tripla.getLeft().getScorte() - tripla.getMiddle());
                    ProdottoTable.getTable().update(tripla.getLeft());
                }
                
                //Creazione Fattura.
                Fattura fatt = new Fattura();
                fatt.setNumero(FatturaTable.getTable().getMaxNumero() + 1);
                fatt.setDataEmissione(actualDate);
                fatt.setImporto(prezzoTotale);
                fatt.setIdCliente(Integer.parseInt(textIdClienteSelezionato.getText()));
                FatturaTable.getTable().persist(fatt);
                String[] nomiAttributi = FatturaTable.getTable().getAttributes();
                DefaultTableModel model = new DefaultTableModel(nomiAttributi, 0) {
                    private static final long serialVersionUID = -9140705710062485906L;
                    @Override
                    public boolean isCellEditable(int row, int column) {
                        return false;
                    }
                };
                model.addRow(fatt.concatAttributes().toArray());
                tableFatture.setModel(model);
                printAllProdotti(false);
                listProdottiModel.clear();
                listProdotti.setModel(listProdottiModel);
                SellerMainWindow.this.productsList.clear();
                JOptionPane.showMessageDialog(frame, res);
            }
        });
        btnInserisciAcquisto.setBounds(593, 553, 137, 30);
        frame.getContentPane().add(btnInserisciAcquisto);
        btnInserisciAcquisto.setBackground(SystemColor.activeCaption);
        
        tableFatture = new JTable();
        scrollPane_3.setViewportView(tableFatture);
        
        JButton btnVisualizzaFatture = new JButton("Visualizza");
        btnVisualizzaFatture.addActionListener(al -> {
                try {
                    printAllFatture(textClienteFatture.getText().length() == 0 ? Optional.empty() : Optional.of(Integer.parseInt(textClienteFatture.getText())));
                } catch (NumberFormatException e ) {
                    JOptionPane.showMessageDialog(frame, "Il campo IdCliente deve contenere l'ID del cliente");
                }
            });
        btnVisualizzaFatture.setBackground(SystemColor.activeCaption);
        btnVisualizzaFatture.setBounds(851, 326, 102, 30);
        frame.getContentPane().add(btnVisualizzaFatture);

        switchComponents(panelNuovoCliente, false);
        textQuantita.setEnabled(false);
        spinnerSconto.setEnabled(false);
        
        JButton btnTop = new JButton("Top 10");
        btnTop.addActionListener(al -> {
            DefaultListModel<String> model = new DefaultListModel<>();
            model.clear();
            listTop10.setModel(model);
            List<Pair<Prodotto,Integer>> prodotti = ProdottoTable.getTable().top10();
            for(Pair<Prodotto,Integer> coppia : prodotti) {
                model.addElement(coppia.getKey().getIdProdotto() + " - Nome: " + coppia.getKey().getNome() + " Quantità venduta: " + coppia.getValue());
            }
            listTop10.setModel(model);
        });
        btnTop.setBackground(SystemColor.activeCaption);
        btnTop.setBounds(963, 611, 102, 30);
        frame.getContentPane().add(btnTop);
    }

    private void switchComponents(JPanel panel, boolean enable) {
        for (Component c : panel.getComponents()) {
            c.setEnabled(enable);
        }
    }

    private void clearAllTextField(JPanel panel) {
        for (Component c : panel.getComponents()) {
            if(c instanceof JTextField){
                ((JTextField) c).setText("");
            }
        }
    }
    
    private void printAllClienti() {
        List<Cliente> totClienti = ClienteTable.getTable().findAll();
        String[] nomiAttributi = ClienteTable.getTable().getAttributes();
        DefaultTableModel model = this.createNotEditableModel(nomiAttributi);
        for (Cliente cl : totClienti) {
            model.addRow(cl.concatAttributes().toArray());
        }
        tableClienti.setModel(model);
    }

    private void printAllProdotti(boolean bio) {
        List<Prodotto> totProdotti = null;
        totProdotti = ProdottoTable.getTable().findAllCustom(bio);
        String[] nomiAttributi = ProdottoTable.getTable().getAttributesCustom();
        DefaultTableModel model = this.createNotEditableModel(nomiAttributi);
        for (Prodotto prod : totProdotti) {
            model.addRow(prod.concatAttributesCustom().toArray());
        }
        tableProdotti.setModel(model);
    }
    
    private void printAllFatture(final Optional<Integer> cliente) {
        List<Fattura> totFatture = null;
        totFatture = FatturaTable.getTable().findAll(cliente);
        String[] nomiAttributi = FatturaTable.getTable().getAttributes();
        DefaultTableModel model = this.createNotEditableModel(nomiAttributi);
        for(Fattura fatt : totFatture) {
            model.addRow(fatt.concatAttributes().toArray());
        }
        tableFatture.setModel(model);
    }
    
    private double roundCents(double value){
        return Math.round(value * 100.0) / 100.0 ;
    }
    
    private DefaultTableModel createNotEditableModel(String[] nomiAttributi) {
        return new DefaultTableModel(nomiAttributi, 0) {
            private static final long serialVersionUID = -9140705710062485906L;
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
    }
}
