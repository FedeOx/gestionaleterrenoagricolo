package view.worker;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.swing.JFrame;
import model.DettaglioLavoro;
import model.Dipendente;
import model.Guida;
import model.Lavoro;
import model.Macchinario;
import model.Mezzo;
import model.Prodotto;
import model.ProdottoSeminato;
import model.Traino;
import model.database.DettaglioLavoroTable;
import model.database.GuidaTable;
import model.database.ProdottoSeminatoTable;
import model.database.ProdottoTable;
import model.database.TrainoTable;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javafx.util.Pair;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextField;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import java.awt.SystemColor;

/**
 * DB worker GUI for specify operation of a detail in case of a seeding or a treatment.
 */
//CHECKSTYLE:OFF
@SuppressWarnings("all")
public class SpecifySeedingWindow {
    private Lavoro lavoro;
    private Macchinario macchinario;
    private Prodotto selectedProd;
    private Dipendente dipendente;
    private Mezzo mezzo;
    private JTable tableOpzioni;
    private List<Pair<Prodotto,Integer>> listProdottiQuantita;
    private JList<String> listProdScelti;
    /**
     * Create the application.
     */
    public SpecifySeedingWindow(final Lavoro lav, final Macchinario macch, final Dipendente dip, final Mezzo mezzo) {
        this.lavoro = lav;
        this.macchinario = macch;
        this.selectedProd = null;
        this.dipendente = dip;
        this.mezzo = mezzo;
        this.listProdScelti = new JList<>();
        this.listProdottiQuantita = new ArrayList<>();
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        JTextField textQuantita = new JTextField();
        textQuantita.setEnabled(false);
        JFrame frame;
        frame = new JFrame("Specify Seeding");
        frame.setBounds(100, 100, 729, 360);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(251, 27, 452, 284);
        frame.getContentPane().add(scrollPane);

        tableOpzioni = new JTable();
        scrollPane.setViewportView(tableOpzioni);
        frame.setVisible(true);
        List<Prodotto> totProdotti = ProdottoTable.getTable().findAll();
        String[] nomiAttributi = ProdottoTable.getTable().getAttributes();
        DefaultTableModel m = createNotEditableModel(nomiAttributi);
        for (Prodotto prod : totProdotti) {
            m.addRow(prod.concatAttributes().toArray());
        }
        tableOpzioni.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                final int row = tableOpzioni.getSelectedRow();
                if(row != -1) {
                    textQuantita.setEnabled(true);
                    selectedProd = ProdottoTable.getTable().findByPrimaryKey(Integer.parseInt(tableOpzioni.getValueAt(row, 0).toString()));
                } else {
                    textQuantita.setEnabled(false);
                    textQuantita.setText("");
                }
            }
        });
        tableOpzioni.setModel(m);
        
        JScrollPane scrollPane_1 = new JScrollPane();
        scrollPane_1.setBounds(10, 113, 231, 153);
        frame.getContentPane().add(scrollPane_1);
        

        scrollPane_1.setViewportView(listProdScelti);
        
        JButton btnSalva = new JButton("Salva");
        btnSalva.setBackground(SystemColor.activeCaption);
        btnSalva.addActionListener(al -> {
            try {               
                    Calendar cal = Calendar.getInstance(); 
                    cal.set(Calendar.MINUTE, 0);
                    cal.set(Calendar.SECOND, 0);
                    cal.set(Calendar.MILLISECOND, 0);
                    Date actualDate = cal.getTime();
                    int idDettaglioLavoro = DettaglioLavoroTable.getTable().getMaxId() + 1;

                    Traino checkTraino = TrainoTable.getTable().findByPrimaryKey(this.macchinario.getIdMacchinario(), this.mezzo.getTelaio(), actualDate);
                    Guida checkGuida = GuidaTable.getTable().findByPrimaryKey(this.mezzo.getTelaio(), actualDate, this.dipendente.getIdDipendente());
                    DettaglioLavoro checkDettLav = DettaglioLavoroTable.getTable().findByPrimaryKey(idDettaglioLavoro);
                    DettaglioLavoro checkDettLav2 = DettaglioLavoroTable.getTable().findByAlternativeKey(actualDate, this.lavoro.getIdLavoro(), this.macchinario.getIdMacchinario());
                    if(checkTraino != null || checkGuida != null || checkDettLav != null || checkDettLav2 != null) {
                        JOptionPane.showMessageDialog(frame, "Inserimento impossibile: vincoli non rispettati");
                        return;
                    }
                    
                    //Registrazione del traino.
                    Traino traino = new Traino();
                    traino.setTelaioMezzo(this.mezzo.getTelaio());
                    traino.setData(actualDate);
                    traino.setIdMacchinario(this.macchinario.getIdMacchinario());
                    TrainoTable.getTable().persist(traino);
                    
                    //Registrazione della guida.
                    Guida guida = new Guida();
                    guida.setTelaioMezzo(this.mezzo.getTelaio());
                    guida.setData(actualDate);
                    guida.setIdDipendente(this.dipendente.getIdDipendente());
                    GuidaTable.getTable().persist(guida);
                    
                    //Registrazione del dettaglioLavoro.
                    DettaglioLavoro dl = new DettaglioLavoro();
                    dl.setIdDettaglioLavoro(idDettaglioLavoro);
                    dl.setDataInizio(actualDate);
                    dl.setIdLavoro(this.lavoro.getIdLavoro());
                    dl.setIdMacchinario(this.macchinario.getIdMacchinario());
                    dl.setDataFine(Optional.empty());
                    DettaglioLavoroTable.getTable().persist(dl);

                    //Registrazione prodotto seminato.
                    for(Pair<Prodotto,Integer> coppia : this.listProdottiQuantita) {
                        ProdottoSeminato ps = new ProdottoSeminato();
                        ps.setIdProdSem(ProdottoSeminatoTable.getTable().getMaxId() + 1);
                        ps.setQuantitaPiantata(coppia.getValue());
                        ps.setDataSemina(actualDate);
                        ps.setRaccoltoEffettivo(Optional.empty());
                        ps.setIdDettaglioLavoro(idDettaglioLavoro);
                        ps.setIdProdotto(coppia.getKey().getIdProdotto());
                        ps.setTelaioMezzo(this.mezzo.getTelaio());
                        ProdottoSeminatoTable.getTable().persist(ps);
                    }
                    JOptionPane.showMessageDialog(frame, "Inserimento andato a buon fine");
                    frame.dispose();
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(frame, "Il campo quantità deve contenere un numero");
            }
        });
        btnSalva.setEnabled(false);
        btnSalva.setBounds(10, 277, 231, 34);
        frame.getContentPane().add(btnSalva);
        
        textQuantita.setBounds(76, 48, 86, 20);
        frame.getContentPane().add(textQuantita);
        textQuantita.setColumns(10);
        
        JLabel lblQuantita = new JLabel("Quantit\u00E0");
        lblQuantita.setBounds(25, 51, 51, 14);
        frame.getContentPane().add(lblQuantita);

        JButton btnInserisci = new JButton("Inserisci in lista");
        btnInserisci.setBounds(133, 79, 105, 23);
        frame.getContentPane().add(btnInserisci);
        btnInserisci.setBackground(SystemColor.desktop);
        
        JButton btnRemove = new JButton("Remove");
        btnRemove.setBounds(19, 79, 104, 23);
        frame.getContentPane().add(btnRemove);
        btnRemove.setBackground(SystemColor.desktop);
        btnRemove.addActionListener(al -> {
            if(!listProdScelti.isSelectionEmpty()){
                String selectedItem = listProdScelti.getSelectedValue().split(" - ")[0];
                listProdottiQuantita.remove(Integer.parseInt(selectedItem));
                if(this.listProdottiQuantita.isEmpty()){
                    btnSalva.setEnabled(false);
                }
                this.buildList();
            }
        });
        btnInserisci.addActionListener(al -> {
            if(textQuantita.getText().length() > 0){
                try {
                    this.listProdottiQuantita.add(new Pair<>(this.selectedProd, Integer.parseInt(textQuantita.getText())));
                    this.buildList();
                    btnSalva.setEnabled(true);
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(frame, "Inserire un valore numerico nel campo quantità");
                }
            } else {
                JOptionPane.showMessageDialog(frame, "Inserire quantità");
            }
        });
    }

    private DefaultTableModel createNotEditableModel(String[] nomiAttributi) {
        return new DefaultTableModel(nomiAttributi, 0) {
            private static final long serialVersionUID = -9140705710062485906L;
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
    }
    
    private void buildList() {
        DefaultListModel<String> model = new DefaultListModel<>();
        model.clear();
        this.listProdScelti.setModel(model);
        for(int i=0; i<this.listProdottiQuantita.size(); i++) {
            model.addElement(i + " - Id Prodotto: " + this.listProdottiQuantita.get(i).getKey().getIdProdotto() + " - Quantita: " + this.listProdottiQuantita.get(i).getValue());
        }
        listProdScelti.setModel(model);
    }
}
