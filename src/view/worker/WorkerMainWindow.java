package view.worker;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.SystemColor;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import javafx.util.Pair;
import model.Dipendente;
import model.Guida;
import model.Lavoro;
import model.Mezzo;
import model.Prodotto;
import model.ProdottoSeminato;
import model.database.GuidaTable;
import model.database.LavoroTable;
import model.database.MacchinarioTable;
import model.database.MezzoTable;
import model.database.ProdottoSeminatoTable;
import model.database.ProdottoTable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

/**
 * DB worker main GUI.
 */
//CHECKSTYLE:OFF
@SuppressWarnings("all")
public class WorkerMainWindow {
    private Dipendente utente;
    private Lavoro selectedWork;
    private Mezzo selectedVehicle;
    private JTable tableMezzi;
    private JTable tableRaccogli;
    private JTextField textQuantitaRaccolta;
    private JTable tableLavori;
    private JList<String> listProdMancanti = new JList<>();
    private JTable tableRicerche;
    private JTextField textIdProdSem;
    private JTextField textData;
    /**
     * Create the application.
     */
    public WorkerMainWindow(Dipendente utente) {
        this.utente = utente;
        this.selectedWork = null;
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        JFrame frame;
        JButton btnEseguiRaccolto = new JButton("Esegui");
        frame = new JFrame("Worker Main Window");
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setBounds(0, 0,screen.width,screen.height - 30);
        frame.setExtendedState(Frame.MAXIMIZED_BOTH);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(755, 49, 596, 280);
        frame.getContentPane().add(scrollPane);
        
        JLabel lblNomeUtente = new JLabel(this.utente.getNome());
        lblNomeUtente.setForeground(Color.BLUE);
        lblNomeUtente.setBounds(22, 11, 69, 14);
        frame.getContentPane().add(lblNomeUtente);
        
        JLabel lblCognomeUtente = new JLabel(this.utente.getCognome());
        lblCognomeUtente.setForeground(Color.BLUE);
        lblCognomeUtente.setBounds(101, 11, 86, 14);
        frame.getContentPane().add(lblCognomeUtente);
        frame.setVisible(true);
        
        JScrollPane scrollPane_1 = new JScrollPane();
        scrollPane_1.setBounds(10, 49, 596, 280);
        frame.getContentPane().add(scrollPane_1);
        
        tableMezzi = new JTable();
        tableMezzi.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e) {
                final JTable jTable= (JTable)e.getSource();
                final int row = jTable.getSelectedRow();
                if(row != -1) {
                    WorkerMainWindow.this.selectedVehicle = MezzoTable.getTable().findByPrimaryKey(jTable.getValueAt(row, 0).toString());
                } else {
                    WorkerMainWindow.this.selectedVehicle = null;
                }
            }
        });
        scrollPane_1.setViewportView(tableMezzi);
        
        JButton btnEseguiLavoro = new JButton("Esegui lavoro");
        btnEseguiLavoro.addActionListener(al -> {
            if(this.selectedWork != null && this.selectedVehicle != null) {
                EventQueue.invokeLater(() -> new InsertWorkDetailWindow(WorkerMainWindow.this.selectedWork, WorkerMainWindow.this.selectedVehicle, WorkerMainWindow.this.utente));
            } else {
                JOptionPane.showMessageDialog(frame, "Mancanza di dati");
            }
        });
        btnEseguiLavoro.setEnabled(false);
        btnEseguiLavoro.setBackground(SystemColor.desktop);
        btnEseguiLavoro.setBounds(616, 98, 129, 24);
        frame.getContentPane().add(btnEseguiLavoro);
        
        JButton btnRaccogli = new JButton("Inserisci Raccolto");
        scrollPane.setViewportView(tableRaccogli);
        
        tableLavori = new JTable();
        tableLavori.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                    final JTable jTable= (JTable)e.getSource();
                    final int row = jTable.getSelectedRow();
                    if(row != -1) {
                        WorkerMainWindow.this.selectedWork = LavoroTable.getTable().findByPrimaryKey(Integer.parseInt(jTable.getValueAt(row, 0).toString()));
                    } else {
                        WorkerMainWindow.this.selectedWork = null;
                    }
            }
        });
        scrollPane.setViewportView(tableLavori);

        btnRaccogli.addActionListener(al -> {
            List<ProdottoSeminato> totProdotti = ProdottoSeminatoTable.getTable().findAll();
            DefaultTableModel m = createNotEditableModel(ProdottoSeminatoTable.getTable().getAttributes());
            for (ProdottoSeminato prod : totProdotti) {
                m.addRow(prod.concatAttributes().toArray());
            }
            tableRaccogli.setModel(m);
            
            List<Mezzo> totMezzi = MezzoTable.getTable().findAllWithFilter("Da Raccolta");
            String[] nomiAttributi = MezzoTable.getTable().getAttributes();
            DefaultTableModel model = createNotEditableModel(nomiAttributi);
            for (Mezzo me : totMezzi) {
                model.addRow(me.concatAttributes().toArray());
            }
            tableMezzi.setModel(model);
            
            tableLavori.setModel(new DefaultTableModel());
            btnEseguiRaccolto.setEnabled(true);
            btnEseguiLavoro.setEnabled(false);
        });
        List<Prodotto> totProdotti = ProdottoTable.getTable().findAll();
        String[] nomiAttributi = ProdottoTable.getTable().getAttributes();
        DefaultTableModel m = createNotEditableModel(nomiAttributi);
        for (Prodotto prod : totProdotti) {
            m.addRow(prod.concatAttributes().toArray());
        }
        btnRaccogli.setBackground(SystemColor.activeCaption);
        btnRaccogli.setBounds(10, 340, 143, 38);
        frame.getContentPane().add(btnRaccogli);
        
        JScrollPane scrollPane_2 = new JScrollPane();
        scrollPane_2.setBounds(10, 424, 442, 265);
        frame.getContentPane().add(scrollPane_2);
        
        tableRaccogli = new JTable();
        scrollPane_2.setViewportView(tableRaccogli);
        
        JScrollPane scrollPane_3 = new JScrollPane();
        scrollPane_3.setBounds(462, 424, 170, 265);
        frame.getContentPane().add(scrollPane_3);
        
        scrollPane_3.setViewportView(listProdMancanti);
       
        btnEseguiRaccolto.addActionListener(al -> {
            if(textQuantitaRaccolta.getText().length() != 0) {
                final int[] rowsRaccogli = tableRaccogli.getSelectedRows();
                final int rowMezzo = tableMezzi.getSelectedRow();
                if (rowsRaccogli.length > 0 && rowMezzo != -1) {
                    try {
                        Calendar cal = Calendar.getInstance(); 
                        cal.set(Calendar.MINUTE, 0);
                        cal.set(Calendar.SECOND, 0);
                        cal.set(Calendar.MILLISECOND, 0);
                        Date actualDate = cal.getTime();
                        
                        if(GuidaTable.getTable().findByPrimaryKey(selectedVehicle.getTelaio(), actualDate, utente.getIdDipendente()) != null) {
                            JOptionPane.showMessageDialog(frame, "Vincoli non rispettati: guida già esistente");
                            return;
                        }
                        //Registrazione della guida.
                        Guida guida = new Guida();
                        guida.setTelaioMezzo(selectedVehicle.getTelaio());
                        guida.setData(actualDate);
                        guida.setIdDipendente(utente.getIdDipendente());
                        GuidaTable.getTable().persist(guida);
                        
                        DefaultListModel<String> model = new DefaultListModel<>();
                        model.clear();
                        this.listProdMancanti.setModel(model);
                        
                        int quantitaRaccolta = Integer.parseInt(textQuantitaRaccolta.getText());
                            for (int row : rowsRaccogli) {
                                int selectedId = Integer.parseInt(tableRaccogli.getValueAt(row, 0).toString());
                                ProdottoSeminato ps = ProdottoSeminatoTable.getTable().findByPrimaryKey(selectedId);
                                if(quantitaRaccolta <= ps.getQuantitaPiantata()){
                                    ps.setRaccoltoEffettivo(Optional.of(Integer.parseInt(textQuantitaRaccolta.getText())));
                                    ProdottoSeminatoTable.getTable().update(ps); //Aggiornamento dell'oggetto prodotto seminato selezionato, viene aggiunta il raccolto effettivo.
                                    Prodotto prodInCatalogo = ProdottoTable.getTable().findByPrimaryKey(ps.getIdProdotto());
                                    prodInCatalogo.setScorte(prodInCatalogo.getScorte() + ps.getRaccoltoEffettivo().get()); //Modifica del quantitativo delle scorte in magazzino.
                                    ProdottoTable.getTable().update(prodInCatalogo);
                                    
                                    if (ps!=null) {
                                        model.addElement("Id Prodotto: " + ps.getIdProdSem() + " - Quantita Mancante: " + (ps.getQuantitaPiantata() - ps.getRaccoltoEffettivo().get()));
                                    }
                                    listProdMancanti.setModel(model);
                                } else {
                                    JOptionPane.showMessageDialog(frame, "Operazione impossibile per il primo prodotto selezionato quantità raccolta > quantità piantata.");
                                }
                            }
                            JOptionPane.showMessageDialog(frame, "Operazione eseguita con successo.");
                    } catch (NumberFormatException e) {
                        JOptionPane.showMessageDialog(frame, "Inserire un numero nel campo quantità raccolta.");
                    }
                } else {
                    JOptionPane.showMessageDialog(frame, "Mancanza di dati");
                }
            } else {
                JOptionPane.showMessageDialog(frame, "Inserire la quantità.");
            }
        });
        btnEseguiRaccolto.setEnabled(false);
        btnEseguiRaccolto.setBackground(SystemColor.desktop);
        btnEseguiRaccolto.setBounds(10, 378, 89, 23);
        frame.getContentPane().add(btnEseguiRaccolto);
        
        textQuantitaRaccolta = new JTextField();
        textQuantitaRaccolta.setBounds(156, 365, 86, 20);
        frame.getContentPane().add(textQuantitaRaccolta);
        textQuantitaRaccolta.setColumns(10);
        
        JLabel lblQuantit = new JLabel("Quantit\u00E0 raccolta:");
        lblQuantit.setBounds(157, 351, 118, 14);
        frame.getContentPane().add(lblQuantit);
        
        JButton btnNewButton = new JButton("Inserisci Lavoro");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                List<Lavoro> totLavori = LavoroTable.getTable().findAll();
                String[] nomiAttributi = LavoroTable.getTable().getAttributes();
                DefaultTableModel model = createNotEditableModel(nomiAttributi);
                for (Lavoro lav : totLavori) {
                    model.addRow(lav.concatAttributes().toArray());
                }
                tableLavori.setModel(model);
                
                List<Mezzo> totMezzi = MezzoTable.getTable().findAllWithFilter("Trattore");
                nomiAttributi = MezzoTable.getTable().getAttributes();
                model = createNotEditableModel(nomiAttributi);
                for (Mezzo me : totMezzi) {
                    model.addRow(me.concatAttributes().toArray());
                }
                tableMezzi.setModel(model);
                
                tableRaccogli.setModel(new DefaultTableModel());
                btnEseguiRaccolto.setEnabled(false);
                btnEseguiLavoro.setEnabled(true);
            }
        });
        btnNewButton.setBackground(SystemColor.activeCaption);
        btnNewButton.setBounds(616, 49, 129, 39);
        frame.getContentPane().add(btnNewButton);
        
        JLabel lblProdottiMancanti = new JLabel("Prodotti mancanti");
        lblProdottiMancanti.setBounds(462, 410, 109, 14);
        frame.getContentPane().add(lblProdottiMancanti);
        
        JLabel lblProdottiSeminati = new JLabel("Prodotti attualmente seminati");
        lblProdottiSeminati.setBounds(10, 410, 178, 14);
        frame.getContentPane().add(lblProdottiSeminati);
        
        JLabel lblNewLabel = new JLabel("Mezzi");
        lblNewLabel.setBounds(10, 34, 46, 14);
        frame.getContentPane().add(lblNewLabel);
        
        JLabel lblLavori = new JLabel("Lavori");
        lblLavori.setBounds(754, 34, 46, 14);
        frame.getContentPane().add(lblLavori);
        
        JScrollPane scrollPane_4 = new JScrollPane();
        scrollPane_4.setBounds(755, 424, 595, 265);
        frame.getContentPane().add(scrollPane_4);
        
        tableRicerche = new JTable();
        scrollPane_4.setViewportView(tableRicerche);
        
        JComboBox<String> comboRicerche = new JComboBox<>();
        comboRicerche.setModel(new DefaultComboBoxModel<String>(new String[] {"UTILIZZO MEZZI AGRICOLI", "UTILIZZO MACCHINARI", "TRATTAMENTI MANCANTI", "PRODOTTI MATURI"}));
        comboRicerche.setBounds(755, 379, 200, 20);
        comboRicerche.addActionListener(al -> {
                if(comboRicerche.getSelectedItem().toString().equals("UTILIZZO MEZZI AGRICOLI") || comboRicerche.getSelectedItem().toString().equals("UTILIZZO MACCHINARI")){
                    textData.setEnabled(true);
                    textIdProdSem.setEnabled(false);
                } else if(comboRicerche.getSelectedItem().toString().equals("TRATTAMENTI MANCANTI")) {
                    textData.setEnabled(false);
                    textIdProdSem.setEnabled(true);
                } else {
                    textData.setEnabled(false);
                    textIdProdSem.setEnabled(false);
                }
        });
        frame.getContentPane().add(comboRicerche);
        
        JPanel panel = new JPanel();
        panel.setBounds(965, 340, 215, 80);
        frame.getContentPane().add(panel);
        GridBagLayout gbl_panel = new GridBagLayout();
        gbl_panel.columnWidths = new int[]{0, 0, 0};
        gbl_panel.rowHeights = new int[]{0, 0, 0, 0};
        gbl_panel.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
        gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        panel.setLayout(gbl_panel);
        
        JLabel lblDati = new JLabel("Dati ricerca");
        GridBagConstraints gbc_lblDati = new GridBagConstraints();
        gbc_lblDati.insets = new Insets(0, 0, 5, 0);
        gbc_lblDati.gridx = 1;
        gbc_lblDati.gridy = 0;
        panel.add(lblDati, gbc_lblDati);
        
        JLabel lblNewLabel_1 = new JLabel("IdProdSem");
        GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
        gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel_1.gridx = 0;
        gbc_lblNewLabel_1.gridy = 1;
        panel.add(lblNewLabel_1, gbc_lblNewLabel_1);
        
        textIdProdSem = new JTextField();
        textIdProdSem.setEnabled(false);
        GridBagConstraints gbc_textIdProdSem = new GridBagConstraints();
        gbc_textIdProdSem.insets = new Insets(0, 0, 5, 0);
        gbc_textIdProdSem.gridx = 1;
        gbc_textIdProdSem.gridy = 1;
        panel.add(textIdProdSem, gbc_textIdProdSem);
        textIdProdSem.setColumns(10);
        
        JLabel lblData_1 = new JLabel("Data");
        GridBagConstraints gbc_lblData_1 = new GridBagConstraints();
        gbc_lblData_1.insets = new Insets(0, 0, 0, 5);
        gbc_lblData_1.gridx = 0;
        gbc_lblData_1.gridy = 2;
        panel.add(lblData_1, gbc_lblData_1);
        
        textData = new JTextField();
        textData.setEnabled(false);
        GridBagConstraints gbc_textData = new GridBagConstraints();
        gbc_textData.gridx = 1;
        gbc_textData.gridy = 2;
        panel.add(textData, gbc_textData);
        textData.setColumns(10);
        
        JButton btnEseguiRicerche = new JButton("Esegui");
        btnEseguiRicerche.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(comboRicerche.getSelectedItem().toString().equals("UTILIZZO MEZZI AGRICOLI") || comboRicerche.getSelectedItem().toString().equals("UTILIZZO MACCHINARI")){
                    if(textData.getText().length() != 0){
                        try {
                            Pair<List<String>,Vector<Vector<String>>> utilizzo = null;
                            if(comboRicerche.getSelectedItem().toString().equals("UTILIZZO MEZZI AGRICOLI")) {
                                utilizzo = MezzoTable.getTable().agriculturalMeansUsage(new SimpleDateFormat("dd/MM/yyyy").parse(textData.getText()));
                            } else if(comboRicerche.getSelectedItem().toString().equals("UTILIZZO MACCHINARI")){
                                utilizzo = MacchinarioTable.getTable().machineryUsage(new SimpleDateFormat("dd/MM/yyyy").parse(textData.getText()));
                            }
                            DefaultTableModel model = createNotEditableModel(utilizzo.getKey().toArray());
                            for (Vector<String> vec : utilizzo.getValue()) {
                                model.addRow(vec);
                            }
                            tableRicerche.setModel(model);
                        } catch (ParseException ex) {
                            JOptionPane.showMessageDialog(frame, "Esprimere la data nella seguente sintatti: dd/MM/yyyy");
                        }
                    } else {
                        JOptionPane.showMessageDialog(frame, "Inserire il filtro per data");
                    }
                } else if(comboRicerche.getSelectedItem().toString().equals("TRATTAMENTI MANCANTI")) {
                    if(textIdProdSem.getText().length() != 0){
                        Pair<List<String>,Vector<Vector<String>>> utilizzo = ProdottoSeminatoTable.getTable().missingTreatments(Integer.parseInt(textIdProdSem.getText()));
                        DefaultTableModel model = createNotEditableModel(utilizzo.getKey().toArray());
                        for (Vector<String> vec : utilizzo.getValue()) {
                             model.addRow(vec);
                        }
                        tableRicerche.setModel(model);
                    } else {
                        JOptionPane.showMessageDialog(frame, "Inserire il filtro per IdProdSem");
                    }
                } else if(comboRicerche.getSelectedItem().toString().equals("PRODOTTI MATURI")) {
                    Pair<List<String>,Vector<Vector<String>>> utilizzo = ProdottoSeminatoTable.getTable().readyToCollect();
                    DefaultTableModel model = createNotEditableModel(utilizzo.getKey().toArray());
                    for (Vector<String> vec : utilizzo.getValue()) {
                         model.addRow(vec);
                    }
                    tableRicerche.setModel(model);
                }
            }
        });
        btnEseguiRicerche.setBackground(SystemColor.desktop);
        btnEseguiRicerche.setBounds(755, 401, 89, 23);
        frame.getContentPane().add(btnEseguiRicerche);
    }

    private DefaultTableModel createNotEditableModel(String[] nomiAttributi) {
        return new DefaultTableModel(nomiAttributi, 0) {
            private static final long serialVersionUID = -9140705710062485906L;
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
    }

    private DefaultTableModel createNotEditableModel(Object[] nomiAttributi) {
        return new DefaultTableModel(nomiAttributi, 0) {
            private static final long serialVersionUID = -9140705710062485906L;
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
    }
}
