package view.worker;

import javax.swing.JFrame;

import model.DettaglioLavoro;
import model.Dipendente;
import model.EsecTrattamento;
import model.Guida;
import model.Lavoro;
import model.Macchinario;
import model.Mezzo;
import model.ProdottoSeminato;
import model.Traino;
import model.database.DettaglioLavoroTable;
import model.database.EsecTrattamentoTable;
import model.database.GuidaTable;
import model.database.ProdottoSeminatoTable;
import model.database.TrainoTable;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javafx.util.Pair;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Vector;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * DB worker GUI for specify what products the treatment is intended for.
 */
//CHECKSTYLE:OFF
@SuppressWarnings("all")
public class SpecifyTreatmentWindow {
    private Lavoro lavoro;
    private Macchinario macchinario;
    private Dipendente dipendente;
    private Mezzo mezzo;
    private JTable tableProdottiPiantati;
    private List<ProdottoSeminato> prodSelezionati;
    private ProdottoSeminato selectedProd;
    private JList<String> listSelezionati = new JList<>();

    /**
     * Create the application.
     */
    public SpecifyTreatmentWindow(Lavoro lav, Macchinario macch, Dipendente dip, Mezzo mez) {
        this.lavoro = lav;
        this.macchinario = macch;
        this.dipendente = dip;
        this.mezzo = mez;
        this.prodSelezionati = new ArrayList<>();
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        JFrame frame;
        frame = new JFrame("Specify Treatments");
        frame.setBounds(100, 100, 633, 258);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(144, 11, 452, 203);
        frame.getContentPane().add(scrollPane);
        
        //Creazione tabella
        tableProdottiPiantati = new JTable();
        scrollPane.setViewportView(tableProdottiPiantati);
        Pair<List<String>,Vector<Vector<String>>> totProdotti = ProdottoSeminatoTable.getTable().findAllWithFilter(this.lavoro.getIdLavoro());
        DefaultTableModel m = createNotEditableModel(totProdotti.getKey().toArray());
        for (Vector<String> vec : totProdotti.getValue()) {
            m.addRow(vec);
        }
        tableProdottiPiantati.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e) {
                final JTable jTable= (JTable)e.getSource();
                final int row = jTable.getSelectedRow();
                if(row != -1) {
                    SpecifyTreatmentWindow.this.selectedProd = ProdottoSeminatoTable.getTable().findByPrimaryKey(Integer.parseInt(jTable.getValueAt(row, 0).toString()));
                } else {
                    SpecifyTreatmentWindow.this.selectedProd = null;
                }
            }
        });
        tableProdottiPiantati.setModel(m);
        
        JScrollPane scrollPane_1 = new JScrollPane();
        scrollPane_1.setBounds(10, 68, 129, 109);
        frame.getContentPane().add(scrollPane_1);
        scrollPane_1.setViewportView(listSelezionati);
        
        JButton btnSalva = new JButton("Salva");
        btnSalva.addActionListener(al -> {
            Calendar cal = Calendar.getInstance(); 
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            Date actualDate = cal.getTime();
            int idDettaglioLavoro = DettaglioLavoroTable.getTable().getMaxId() + 1;

            Traino checkTraino = TrainoTable.getTable().findByPrimaryKey(this.macchinario.getIdMacchinario(), this.mezzo.getTelaio(), actualDate);
            Guida checkGuida = GuidaTable.getTable().findByPrimaryKey(this.mezzo.getTelaio(), actualDate, this.dipendente.getIdDipendente());
            DettaglioLavoro checkDettLav = DettaglioLavoroTable.getTable().findByPrimaryKey(idDettaglioLavoro);
            DettaglioLavoro checkDettLav2 = DettaglioLavoroTable.getTable().findByAlternativeKey(actualDate, this.lavoro.getIdLavoro(), this.macchinario.getIdMacchinario());
            for (ProdottoSeminato ps : this.prodSelezionati) { 
                EsecTrattamento checkEsecTratt1 = EsecTrattamentoTable.getTable().findByPrimayKey(ps.getIdProdSem(), idDettaglioLavoro);
                if(checkEsecTratt1 != null){
                    JOptionPane.showMessageDialog(frame, "Inserimento impossibile: escuzione trattamento già presente");
                    return;
                }//Controllo utilizzato per evitare che uno stesso trattamento venga ripetuto più volte sullo stesso prodotto seminato.
                List<Lavoro> checkEsecTratt2 = EsecTrattamentoTable.getTable().treatmentsAlreadyMade(ps.getIdProdSem());
                for(Lavoro lav : checkEsecTratt2){
                    if(lav.getIdLavoro() == this.lavoro.getIdLavoro()) {
                        JOptionPane.showMessageDialog(frame, "Inserimento impossibile: uno dei prodotto seminati selezionari ha già ricevuto questo trattamento");
                        return;
                    }
                }
            }
            if(checkTraino != null || checkGuida != null || checkDettLav != null || checkDettLav2 != null) {
                JOptionPane.showMessageDialog(frame, "Inserimento impossibile: vincoli non rispettati");
                return;
            }
            
            //Registrazione del traino.
            Traino traino = new Traino();
            traino.setTelaioMezzo(this.mezzo.getTelaio());
            traino.setData(actualDate);
            traino.setIdMacchinario(this.macchinario.getIdMacchinario());
            TrainoTable.getTable().persist(traino);
            
            //Registrazione della guida.
            Guida guida = new Guida();
            guida.setTelaioMezzo(this.mezzo.getTelaio());
            guida.setData(actualDate);
            guida.setIdDipendente(this.dipendente.getIdDipendente());
            GuidaTable.getTable().persist(guida);
            
            //Registrazione del dettaglioLavoro.
            DettaglioLavoro dl = new DettaglioLavoro();
            dl.setIdDettaglioLavoro(idDettaglioLavoro);
            dl.setDataInizio(actualDate);
            dl.setIdLavoro(this.lavoro.getIdLavoro());
            dl.setIdMacchinario(this.macchinario.getIdMacchinario());
            dl.setDataFine(Optional.empty());
            DettaglioLavoroTable.getTable().persist(dl);
            
          //Registrazione prodotto seminato.
            for(ProdottoSeminato ps : this.prodSelezionati) {
                EsecTrattamento et = new EsecTrattamento();
                et.setIdDettaglioLavoro(idDettaglioLavoro);
                et.setIdProdSem(ps.getIdProdSem());
                EsecTrattamentoTable.getTable().persist(et);
            }
            JOptionPane.showMessageDialog(frame, "Inserimento andato a buon fine");
            frame.dispose();
        });
        btnSalva.setEnabled(false);
        btnSalva.setBounds(10, 188, 124, 23);
        frame.getContentPane().add(btnSalva);
        frame.setVisible(true);
        
        JButton btnInserisci = new JButton("Inserisci");
        btnInserisci.addActionListener(al -> {
            if(this.selectedProd != null){
                try {
                    this.prodSelezionati.add(this.selectedProd);
                    this.buildList();
                    btnSalva.setEnabled(true);
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(frame, "Inserire un valore numerico nel campo quantità");
                }
            } else {
                JOptionPane.showMessageDialog(frame, "Inserire quantità");
            }
        });
        btnInserisci.setBounds(53, 15, 89, 23);
        frame.getContentPane().add(btnInserisci);
        
        JButton btnRimuovi = new JButton("Rimuovi");
        btnRimuovi.addActionListener(al -> {
            if(!this.listSelezionati.isSelectionEmpty()){
                String selectedItem = this.listSelezionati.getSelectedValue().split(" - ")[0];
                this.prodSelezionati.remove(Integer.parseInt(selectedItem));
                if(this.prodSelezionati.isEmpty()){
                    btnSalva.setEnabled(false);
                }
                this.buildList();
            }
        });
        btnRimuovi.setBounds(53, 37, 89, 23);
        frame.getContentPane().add(btnRimuovi);
    }

    private DefaultTableModel createNotEditableModel(Object[] nomiAttributi) {
        return new DefaultTableModel(nomiAttributi, 0) {
            private static final long serialVersionUID = -9140705710062485906L;
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
    }

    private void buildList() {
        DefaultListModel<String> model = new DefaultListModel<>();
        model.clear();
        this.listSelezionati.setModel(model);
        for(int i=0; i<this.prodSelezionati.size(); i++) {
            model.addElement(i + " - Id: " + this.prodSelezionati.get(i).getIdProdSem() + " - Quantita: " + this.prodSelezionati.get(i).getQuantitaPiantata());
        }
        listSelezionati.setModel(model);
    }
}
