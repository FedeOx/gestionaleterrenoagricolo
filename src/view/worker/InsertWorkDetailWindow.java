package view.worker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import model.DettaglioLavoro;
import model.Dipendente;
import model.Guida;
import model.Lavoro;
import model.Macchinario;
import model.Mezzo;
import model.Traino;
import model.database.DettaglioLavoroTable;
import model.database.GuidaTable;
import model.database.MacchinarioTable;
import model.database.TrainoTable;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.Component;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextField;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

/**
 * DB worker GUI for insertion of works details.
 */
//CHECKSTYLE:OFF
@SuppressWarnings("all")
public class InsertWorkDetailWindow {
    private Lavoro lavoro;
    private Mezzo mezzo;
    private Dipendente dipendente;
    private Macchinario selectedMachinery;
    /**
     * Create the application.
     */
    public InsertWorkDetailWindow(final Lavoro lav, final Mezzo mezzo, final Dipendente dipendente) {
        this.lavoro = lav;
        this.mezzo = mezzo;
        this.dipendente = dipendente;
        this.selectedMachinery = null;
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        JTextField textIdDett;
        JTextField textDataDet;
        JTextField textIdLav;
        JTextField textIdMacch;
        
        JFrame frame;
        frame = new JFrame("Insert Work Detail");
        frame.setBounds(100, 100, 650, 315);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 36, 201, 61);
        frame.getContentPane().add(scrollPane);
 
        JLabel lblInfoLavoro = new JLabel("Info Lavoro");
        lblInfoLavoro.setBounds(10, 23, 64, 14);
        frame.getContentPane().add(lblInfoLavoro);
        frame.setVisible(true);
        
        JLabel lblMacchinariNecessari = new JLabel("Macchinari necessari");
        lblMacchinariNecessari.setBounds(221, 23, 158, 14);
        frame.getContentPane().add(lblMacchinariNecessari);
        
        JScrollPane scrollPane_1 = new JScrollPane();
        scrollPane_1.setBounds(221, 36, 403, 230);
        frame.getContentPane().add(scrollPane_1);
        
        JPanel panelAnteprima = new JPanel();
        panelAnteprima.setBounds(10, 123, 201, 143);
        frame.getContentPane().add(panelAnteprima);
        GridBagLayout gbl_panelAnteprima = new GridBagLayout();
        gbl_panelAnteprima.columnWidths = new int[]{0, 0, 0};
        gbl_panelAnteprima.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
        gbl_panelAnteprima.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
        gbl_panelAnteprima.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        panelAnteprima.setLayout(gbl_panelAnteprima);
        
        JLabel lblIdDettaglio = new JLabel("Id");
        GridBagConstraints gbc_lblIdDettaglio = new GridBagConstraints();
        gbc_lblIdDettaglio.insets = new Insets(0, 0, 5, 5);
        gbc_lblIdDettaglio.anchor = GridBagConstraints.EAST;
        gbc_lblIdDettaglio.gridx = 0;
        gbc_lblIdDettaglio.gridy = 0;
        panelAnteprima.add(lblIdDettaglio, gbc_lblIdDettaglio);
        
        JLabel lblAnteprimaDettaglio = new JLabel("Anteprima dettaglio");
        lblAnteprimaDettaglio.setBounds(10, 108, 130, 14);
        frame.getContentPane().add(lblAnteprimaDettaglio);
        
        textIdDett = new JTextField();
        GridBagConstraints gbc_textIdDett = new GridBagConstraints();
        gbc_textIdDett.insets = new Insets(0, 0, 5, 0);
        gbc_textIdDett.gridx = 1;
        gbc_textIdDett.gridy = 0;
        panelAnteprima.add(textIdDett, gbc_textIdDett);
        textIdDett.setColumns(10);
        textIdDett.setEditable(false);
        
        JLabel lblData = new JLabel("Data");
        GridBagConstraints gbc_lblData = new GridBagConstraints();
        gbc_lblData.insets = new Insets(0, 0, 5, 5);
        gbc_lblData.gridx = 0;
        gbc_lblData.gridy = 1;
        panelAnteprima.add(lblData, gbc_lblData);
        
        textDataDet = new JTextField();
        GridBagConstraints gbc_textDataDet = new GridBagConstraints();
        gbc_textDataDet.insets = new Insets(0, 0, 5, 0);
        gbc_textDataDet.gridx = 1;
        gbc_textDataDet.gridy = 1;
        panelAnteprima.add(textDataDet, gbc_textDataDet);
        textDataDet.setColumns(10);
        textDataDet.setEditable(false);
        
        JLabel lblIdLavoro = new JLabel("IdLav.");
        GridBagConstraints gbc_lblIdLavoro = new GridBagConstraints();
        gbc_lblIdLavoro.insets = new Insets(0, 0, 5, 5);
        gbc_lblIdLavoro.gridx = 0;
        gbc_lblIdLavoro.gridy = 2;
        panelAnteprima.add(lblIdLavoro, gbc_lblIdLavoro);
        
        textIdLav = new JTextField();
        GridBagConstraints gbc_textIdLav = new GridBagConstraints();
        gbc_textIdLav.insets = new Insets(0, 0, 5, 0);
        gbc_textIdLav.gridx = 1;
        gbc_textIdLav.gridy = 2;
        panelAnteprima.add(textIdLav, gbc_textIdLav);
        textIdLav.setColumns(10);
        textIdLav.setEditable(false);
        
        JLabel lblIdMacchinario = new JLabel("IdMacch.");
        GridBagConstraints gbc_lblIdMacchinario = new GridBagConstraints();
        gbc_lblIdMacchinario.insets = new Insets(0, 0, 5, 5);
        gbc_lblIdMacchinario.gridx = 0;
        gbc_lblIdMacchinario.gridy = 3;
        panelAnteprima.add(lblIdMacchinario, gbc_lblIdMacchinario);
        
        textIdMacch = new JTextField();
        GridBagConstraints gbc_textIdMacch = new GridBagConstraints();
        gbc_textIdMacch.insets = new Insets(0, 0, 5, 0);
        gbc_textIdMacch.gridx = 1;
        gbc_textIdMacch.gridy = 3;
        panelAnteprima.add(textIdMacch, gbc_textIdMacch);
        textIdMacch.setColumns(10);
        textIdMacch.setEditable(false);

        JList<String> listLavoro = new JList<>();
        scrollPane.setViewportView(listLavoro);
        DefaultListModel<String> model = new DefaultListModel<>();
        model.clear();
        listLavoro.setModel(model);
        model.addElement(Objects.toString(this.lavoro.getIdLavoro()));
        model.addElement(this.lavoro.getNome());
        listLavoro.setModel(model);

        JButton btnInserisciDettaglio = new JButton("Inserisci dettaglio");
        btnInserisciDettaglio.setEnabled(false);
        GridBagConstraints gbc_btnInserisciDettaglio = new GridBagConstraints();
        gbc_btnInserisciDettaglio.gridx = 1;
        gbc_btnInserisciDettaglio.gridy = 4;
        panelAnteprima.add(btnInserisciDettaglio, gbc_btnInserisciDettaglio);
        btnInserisciDettaglio.addActionListener(al -> {
            if(textIdDett.getText().length()>0 && textIdLav.getText().length()>0 && textIdLav.getText().length()>0 && textIdMacch.getText().length()>0){
                try {
                    if (InsertWorkDetailWindow.this.lavoro.getTipologia().equals("Lavorazione Terreno")) {
                        Calendar cal = Calendar.getInstance(); 
                        cal.set(Calendar.MINUTE, 0);
                        cal.set(Calendar.SECOND, 0);
                        cal.set(Calendar.MILLISECOND, 0);
                        Date actualDate = cal.getTime();
                        Traino checkTraino = TrainoTable.getTable().findByPrimaryKey(this.selectedMachinery.getIdMacchinario(), this.mezzo.getTelaio(), actualDate);
                        Guida checkGuida = GuidaTable.getTable().findByPrimaryKey(this.mezzo.getTelaio(), actualDate, this.dipendente.getIdDipendente());
                        DettaglioLavoro checkdl = DettaglioLavoroTable.getTable().findByPrimaryKey(Integer.parseInt(textIdDett.getText()));
                        DettaglioLavoro checkDettLav2 = DettaglioLavoroTable.getTable().findByAlternativeKey(actualDate, this.lavoro.getIdLavoro(), this.selectedMachinery.getIdMacchinario());
                        if(checkTraino != null || checkGuida != null || checkdl != null || checkDettLav2!=null) {
                            JOptionPane.showMessageDialog(frame, "Inserimento impossibile: vincoli non rispettati");
                            return;
                        }
                        
                        //Registrazione del traino.
                        Traino traino = new Traino();
                        traino.setTelaioMezzo(this.mezzo.getTelaio());
                        traino.setData(actualDate);
                        traino.setIdMacchinario(this.selectedMachinery.getIdMacchinario());
                        TrainoTable.getTable().persist(traino);
                        
                        //Registrazione della guida.
                        Guida guida = new Guida();
                        guida.setTelaioMezzo(this.mezzo.getTelaio());
                        guida.setData(actualDate);
                        guida.setIdDipendente(this.dipendente.getIdDipendente());
                        GuidaTable.getTable().persist(guida);
                        
                        //Registrazione del dettaglioLavoro.
                        DettaglioLavoro dl = new DettaglioLavoro();
                        dl.setIdDettaglioLavoro(Integer.parseInt(textIdDett.getText()));
                        dl.setDataInizio(actualDate);
                        dl.setIdLavoro(Integer.parseInt(textIdLav.getText()));
                        dl.setIdMacchinario(Integer.parseInt(textIdMacch.getText()));
                        dl.setDataFine(Optional.empty());
                        DettaglioLavoroTable.getTable().persist(dl);
                        JOptionPane.showMessageDialog(frame, "Inserimento avvenuto correttamente");
                        frame.dispose();
                    } else if(InsertWorkDetailWindow.this.lavoro.getTipologia().equals("Semina")){
                        new SpecifySeedingWindow(lavoro, selectedMachinery, dipendente, mezzo);
                        frame.dispose();
                    } else {
                        new SpecifyTreatmentWindow(lavoro, selectedMachinery, dipendente, mezzo);
                        frame.dispose();
                    }
                } catch (NumberFormatException e){
                    JOptionPane.showMessageDialog(frame, "Errore");
                }
            } else {
                JOptionPane.showMessageDialog(frame, "Mancanza di dati");
            }
        });
        btnInserisciDettaglio.setBackground(SystemColor.activeCaption);

        JTable tableMacchinari = new JTable();
        scrollPane_1.setViewportView(tableMacchinari);
        List<Macchinario> totMacchinari = MacchinarioTable.getTable().findAllWithFilter(this.lavoro.getIdLavoro());
        String[] nomiAttributi = MacchinarioTable.getTable().getAttributes();
        DefaultTableModel m = createNotEditableModel(nomiAttributi);
        for (Macchinario macc : totMacchinari) {
            m.addRow(macc.concatAttributes().toArray());
        }
        tableMacchinari.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                final int row = tableMacchinari.getSelectedRow();
                if(row != -1) {
                    btnInserisciDettaglio.setEnabled(true);
                    InsertWorkDetailWindow.this.selectedMachinery = MacchinarioTable.getTable().findByPrimaryKey(Integer.parseInt(tableMacchinari.getValueAt(row, 0).toString()));
                    textIdDett.setText(Objects.toString(DettaglioLavoroTable.getTable().getMaxId() + 1));
                    textDataDet.setText(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
                    textIdLav.setText(Objects.toString(InsertWorkDetailWindow.this.lavoro.getIdLavoro()));
                    textIdMacch.setText(Objects.toString(InsertWorkDetailWindow.this.selectedMachinery.getIdMacchinario()));
                } else {
                    clearAllTextField(panelAnteprima);
                }
            }
        });
        tableMacchinari.setModel(m);
    }

    private DefaultTableModel createNotEditableModel(String[] nomiAttributi) {
        return new DefaultTableModel(nomiAttributi, 0) {
            private static final long serialVersionUID = -9140705710062485906L;
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
    }

    private void clearAllTextField(JPanel panel) {
        for (Component c : panel.getComponents()) {
            if(c instanceof JTextField){
                ((JTextField) c).setText("");
            }
        }
    }
}
