package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import javax.swing.JLabel;
import javax.swing.JTextField;

import model.Dipendente;
import model.database.DipendenteTable;
import view.admin.AdminMainWindow;
import view.seller.SellerMainWindow;
import view.worker.WorkerMainWindow;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import java.awt.SystemColor;

/**
 * Initial GUI.
 */
public class Login {

    private boolean admin;
    private static final String PSW = "123456";

    /**
     * Create the application.
     */
    public Login() {
        this.admin = false;
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        final JFrame frame;
        final JLabel lblId = new JLabel("id");
        final JLabel lblPsw = new JLabel("psw");
        final JTextField textId = new JTextField();
        final JPasswordField passwordField = new JPasswordField();

        frame = new JFrame("Login");
        frame.setBounds(100, 100, 450, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        final JMenuBar menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);

        final JMenu mnNewMenu = new JMenu("Utente");
        menuBar.add(mnNewMenu);

        final JMenuItem mntmAdmin = new JMenuItem("Admin");
        mntmAdmin.addActionListener(e -> {
            passwordField.setVisible(true);
            lblPsw.setVisible(true);
            this.admin = true;
            passwordField.setText("");
        });
        mnNewMenu.add(mntmAdmin);

        final JMenuItem mntmDipendente = new JMenuItem("Dipendente");
        mntmDipendente.addActionListener(e -> {
            passwordField.setVisible(false);
            lblPsw.setVisible(false);
            this.admin = false;
            passwordField.setText("");
        });
        mnNewMenu.add(mntmDipendente);
        frame.getContentPane().setLayout(null);

        lblId.setBounds(154, 90, 46, 14);
        frame.getContentPane().add(lblId);

        lblPsw.setBounds(154, 115, 46, 14);
        lblPsw.setVisible(false);
        frame.getContentPane().add(lblPsw);

        textId.setBounds(183, 87, 89, 20);
        frame.getContentPane().add(textId);
        textId.setColumns(10);

        passwordField.setBounds(183, 112, 89, 20);
        passwordField.setVisible(false);
        frame.getContentPane().add(passwordField);

        final JButton btnLogin = new JButton("Login");
        btnLogin.setBackground(SystemColor.desktop);
        btnLogin.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent arg0) {
                if (admin) {
                    if (checkPsw(new String(passwordField.getPassword()))) {
                        EventQueue.invokeLater(() -> new AdminMainWindow());
                    } else {
                        JOptionPane.showMessageDialog(frame, "ERROR: Incorrect Password");
                    }
                } else {
                    try {
                        final Dipendente dip = DipendenteTable.getTable().findByPrimaryKey(Integer.parseInt(textId.getText()));
                        if (dip != null) {
                            switch (dip.getTipologia()) {
                                case "Operaio":
                                    EventQueue.invokeLater(() -> new WorkerMainWindow(dip)); 
                                break;
                                case "Venditore":
                                    EventQueue.invokeLater(() -> new SellerMainWindow(dip)); 
                                break;
                                default:
                            }
                        } else {
                            JOptionPane.showMessageDialog(frame, "ERROR: Incorrect Id");
                        }
                    } catch (NumberFormatException e) {
                        JOptionPane.showMessageDialog(frame, "ERROR: Id must be a number");
                        textId.setText("");
                    }
                }
                passwordField.setText("");
            }
        });
        btnLogin.setBounds(183, 146, 89, 23);
        frame.getContentPane().add(btnLogin);

        frame.setVisible(true);
    }

    private boolean checkPsw(final String psw) {
        return psw.equals(PSW);
    }
}
