package view.admin;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Frame;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import model.Cliente;
import model.Dipendente;
import model.Lavoro;
import model.Macchinario;
import model.Mezzo;
import model.Prodotto;
import model.database.ClienteTable;
import model.database.DipendenteTable;
import model.database.LavoroTable;
import model.database.MacchinarioTable;
import model.database.MezzoTable;
import model.database.ProdottoTable;

import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Optional;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.DefaultComboBoxModel;
import java.awt.SystemColor;
import java.awt.Toolkit;

/**
 * DB administrator GUI.
 */
//CHECKSTYLE:OFF
@SuppressWarnings("all")
public class AdminMainWindow {
    private JTable table;
    /**
     * Create the application.
     */
    public AdminMainWindow() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        final JFrame frame;
        frame = new JFrame("Admin Main Window");
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setBounds(0, 0,screen.width,screen.height - 30);
        frame.setExtendedState(Frame.MAXIMIZED_BOTH);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        JTextField textIdDip;
        JTextField textNomeDip;
        JTextField textCognomeDip;
        JTextField textViaDip;
        JTextField textNumeroDip;
        JTextField textCittaDip;
        JTextField textTelefonoDip;
        JTextField textStipendioDip;
        JTextField textIdCliente;
        JTextField textNomeCliente;
        JTextField textCognomeCliente;
        JTextField textNumeroCliente;
        JTextField textCittaCliente;
        JTextField textCAPCliente;
        JTextField textTelefonoCliente;
        JTextField textViaCliente;
        JTextField textTelaio;
        JTextField textNomeMezzo;
        JTextField textTarga;
        JTextField textTipologiaMezzo;
        JTextField textIdMacchinario;
        JTextField textNomeMacchinario;
        JTextField textIdProdotto;
        JTextField textNomeProdotto;
        JTextField textPrezzoProdotto;
        JTextField textTempoMat;
        JTextField textTerrenoIdeale;
        JTextField textScorte;
        JTextField textIdLavoro;
        JTextField textNomeLavoro;
        JPanel panelDipendenti = new JPanel();
        JPanel panelClienti = new JPanel();
        JPanel panelMacchinari = new JPanel();
        JPanel panelMezzi = new JPanel();
        JPanel panelProdotti = new JPanel();
        JPanel panelLavori = new JPanel();
        JCheckBox chckbxBio = new JCheckBox("Bio");

        JMenuBar menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);

        JMenuItem mntmInserimentoDipendete = new JMenuItem("Inserimento Dipendete");
        mntmInserimentoDipendete.addActionListener(al -> {
            switchComponents(panelDipendenti, true);
            switchComponents(panelClienti, false);
            switchComponents(panelMezzi, false);
            switchComponents(panelMacchinari, false);
            switchComponents(panelProdotti, false);
            switchComponents(panelLavori, false);
        });
        menuBar.add(mntmInserimentoDipendete);

        JMenuItem mntmInserimentoClienete = new JMenuItem("Inserimento Cliente");
        mntmInserimentoClienete.addActionListener(al -> {
            switchComponents(panelDipendenti, false);
            switchComponents(panelClienti, true);
            switchComponents(panelMezzi, false);
            switchComponents(panelMacchinari, false);
            switchComponents(panelProdotti, false);
            switchComponents(panelLavori, false);
        });
        menuBar.add(mntmInserimentoClienete);

        JMenuItem mntmInserimentoMezzo = new JMenuItem("Inserimento Mezzo");
        mntmInserimentoMezzo.addActionListener(al -> {
            switchComponents(panelDipendenti, false);
            switchComponents(panelClienti, false);
            switchComponents(panelMezzi, true);
            switchComponents(panelMacchinari, false);
            switchComponents(panelProdotti, false);
            switchComponents(panelLavori, false);
        });
        menuBar.add(mntmInserimentoMezzo);

        JMenuItem mntmInserimentoMacchinario = new JMenuItem("Inserimento Macchinario");
        mntmInserimentoMacchinario.addActionListener(al -> {
            switchComponents(panelDipendenti, false);
            switchComponents(panelClienti, false);
            switchComponents(panelMezzi, false);
            switchComponents(panelMacchinari, true);
            switchComponents(panelProdotti, false);
            switchComponents(panelLavori, false);
        });
        menuBar.add(mntmInserimentoMacchinario);

        JMenuItem mntmInserimentoProdotto = new JMenuItem("Inserimento Prodotto");
        mntmInserimentoProdotto.addActionListener(al -> {
            switchComponents(panelDipendenti, false);
            switchComponents(panelClienti, false);
            switchComponents(panelMezzi, false);
            switchComponents(panelMacchinari, false);
            switchComponents(panelProdotti, true);
            switchComponents(panelLavori, false);
        });
        menuBar.add(mntmInserimentoProdotto);

        JMenuItem mntmInserimentoLavoro = new JMenuItem("Inserimento Lavoro");
        mntmInserimentoLavoro.addActionListener(al -> {
            switchComponents(panelDipendenti, false);
            switchComponents(panelClienti, false);
            switchComponents(panelMezzi, false);
            switchComponents(panelMacchinari, false);
            switchComponents(panelProdotti, false);
            switchComponents(panelLavori, true);
        });
        menuBar.add(mntmInserimentoLavoro);
        frame.getContentPane().setLayout(null);

        panelDipendenti.setBounds(10, 11, 262, 287);
        frame.getContentPane().add(panelDipendenti);
        GridBagLayout gbl_panelDipendenti = new GridBagLayout();
        gbl_panelDipendenti.columnWidths = new int[]{66, 51, 0, 0};
        gbl_panelDipendenti.rowHeights = new int[]{14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gbl_panelDipendenti.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_panelDipendenti.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        panelDipendenti.setLayout(gbl_panelDipendenti);

        JLabel lblDipendenti = new JLabel("Dipendenti");
        GridBagConstraints gbc_lblDipendenti = new GridBagConstraints();
        gbc_lblDipendenti.insets = new Insets(0, 0, 5, 5);
        gbc_lblDipendenti.anchor = GridBagConstraints.NORTHWEST;
        gbc_lblDipendenti.gridx = 1;
        gbc_lblDipendenti.gridy = 0;
        panelDipendenti.add(lblDipendenti, gbc_lblDipendenti);
                                                                                        
        JLabel lblIdDip = new JLabel("Id");
        GridBagConstraints gbc_lblIdDip = new GridBagConstraints();
        gbc_lblIdDip.insets = new Insets(0, 0, 5, 5);
        gbc_lblIdDip.gridx = 0;
        gbc_lblIdDip.gridy = 1;
        panelDipendenti.add(lblIdDip, gbc_lblIdDip);
        
        textIdDip = new JTextField();
        GridBagConstraints gbc_textIdDip = new GridBagConstraints();
        gbc_textIdDip.insets = new Insets(0, 0, 5, 5);
        gbc_textIdDip.gridx = 1;
        gbc_textIdDip.gridy = 1;
        panelDipendenti.add(textIdDip, gbc_textIdDip);
        textIdDip.setColumns(10);
        
        GridBagConstraints gbc_btnClearDipendente = new GridBagConstraints();
        gbc_btnClearDipendente.insets = new Insets(0, 0, 5, 0);
        gbc_btnClearDipendente.gridx = 2;
        gbc_btnClearDipendente.gridy = 1;

        JLabel lblNomeDip = new JLabel("Nome");
        GridBagConstraints gbc_lblNomeDip = new GridBagConstraints();
        gbc_lblNomeDip.insets = new Insets(0, 0, 5, 5);
        gbc_lblNomeDip.gridx = 0;
        gbc_lblNomeDip.gridy = 2;
        panelDipendenti.add(lblNomeDip, gbc_lblNomeDip);

        textNomeDip = new JTextField();
        GridBagConstraints gbc_textNomeDip = new GridBagConstraints();
        gbc_textNomeDip.insets = new Insets(0, 0, 5, 5);
        gbc_textNomeDip.gridx = 1;
        gbc_textNomeDip.gridy = 2;
        panelDipendenti.add(textNomeDip, gbc_textNomeDip);
        textNomeDip.setColumns(10);
        
        JLabel lblCognomeDip = new JLabel("Cognome");
        GridBagConstraints gbc_lblCognomeDip = new GridBagConstraints();
        gbc_lblCognomeDip.insets = new Insets(0, 0, 5, 5);
        gbc_lblCognomeDip.gridx = 0;
        gbc_lblCognomeDip.gridy = 3;
        panelDipendenti.add(lblCognomeDip, gbc_lblCognomeDip);

        textCognomeDip = new JTextField();
        GridBagConstraints gbc_textCognomeDip = new GridBagConstraints();
        gbc_textCognomeDip.insets = new Insets(0, 0, 5, 5);
        gbc_textCognomeDip.gridx = 1;
        gbc_textCognomeDip.gridy = 3;
        panelDipendenti.add(textCognomeDip, gbc_textCognomeDip);
        textCognomeDip.setColumns(10);
        
        JLabel lblViaDip = new JLabel("Via");
        GridBagConstraints gbc_lblViaDip = new GridBagConstraints();
        gbc_lblViaDip.insets = new Insets(0, 0, 5, 5);
        gbc_lblViaDip.gridx = 0;
        gbc_lblViaDip.gridy = 4;
        panelDipendenti.add(lblViaDip, gbc_lblViaDip);

        textViaDip = new JTextField();
        GridBagConstraints gbc_textViaDip = new GridBagConstraints();
        gbc_textViaDip.insets = new Insets(0, 0, 5, 5);
        gbc_textViaDip.gridx = 1;
        gbc_textViaDip.gridy = 4;
        panelDipendenti.add(textViaDip, gbc_textViaDip);
        textViaDip.setColumns(10);

        JLabel lblNumeroDip = new JLabel("Numero");
        GridBagConstraints gbc_lblNumeroDip = new GridBagConstraints();
        gbc_lblNumeroDip.insets = new Insets(0, 0, 5, 5);
        gbc_lblNumeroDip.gridx = 0;
        gbc_lblNumeroDip.gridy = 5;
        panelDipendenti.add(lblNumeroDip, gbc_lblNumeroDip);

        textNumeroDip = new JTextField();
        GridBagConstraints gbc_textNumeroDip = new GridBagConstraints();
        gbc_textNumeroDip.insets = new Insets(0, 0, 5, 5);
        gbc_textNumeroDip.gridx = 1;
        gbc_textNumeroDip.gridy = 5;
        panelDipendenti.add(textNumeroDip, gbc_textNumeroDip);
        textNumeroDip.setColumns(10);

        JLabel lblCittDip = new JLabel("Citt\u00E0");
        GridBagConstraints gbc_lblCittDip = new GridBagConstraints();
        gbc_lblCittDip.insets = new Insets(0, 0, 5, 5);
        gbc_lblCittDip.gridx = 0;
        gbc_lblCittDip.gridy = 6;
        panelDipendenti.add(lblCittDip, gbc_lblCittDip);

        textCittaDip = new JTextField();
        GridBagConstraints gbc_textCittaDip = new GridBagConstraints();
        gbc_textCittaDip.insets = new Insets(0, 0, 5, 5);
        gbc_textCittaDip.gridx = 1;
        gbc_textCittaDip.gridy = 6;
        panelDipendenti.add(textCittaDip, gbc_textCittaDip);
        textCittaDip.setColumns(10);

        JLabel lblTelefonoDip = new JLabel("Telefono");
        GridBagConstraints gbc_lblTelefonoDip = new GridBagConstraints();
        gbc_lblTelefonoDip.insets = new Insets(0, 0, 5, 5);
        gbc_lblTelefonoDip.gridx = 0;
        gbc_lblTelefonoDip.gridy = 7;
        panelDipendenti.add(lblTelefonoDip, gbc_lblTelefonoDip);

        textTelefonoDip = new JTextField();
        GridBagConstraints gbc_textTelefonoDip = new GridBagConstraints();
        gbc_textTelefonoDip.insets = new Insets(0, 0, 5, 5);
        gbc_textTelefonoDip.gridx = 1;
        gbc_textTelefonoDip.gridy = 7;
        panelDipendenti.add(textTelefonoDip, gbc_textTelefonoDip);
        textTelefonoDip.setColumns(10);

        JLabel lblStipendioDip = new JLabel("Stipendio");
        GridBagConstraints gbc_lblStipendioDip = new GridBagConstraints();
        gbc_lblStipendioDip.insets = new Insets(0, 0, 5, 5);
        gbc_lblStipendioDip.gridx = 0;
        gbc_lblStipendioDip.gridy = 8;
        panelDipendenti.add(lblStipendioDip, gbc_lblStipendioDip);

        textStipendioDip = new JTextField();
        GridBagConstraints gbc_textStipendioDip = new GridBagConstraints();
        gbc_textStipendioDip.insets = new Insets(0, 0, 5, 5);
        gbc_textStipendioDip.gridx = 1;
        gbc_textStipendioDip.gridy = 8;
        panelDipendenti.add(textStipendioDip, gbc_textStipendioDip);
        textStipendioDip.setColumns(10);

        JLabel lblTipologiaDip = new JLabel("Tipologia");
        GridBagConstraints gbc_lblTipologiaDip = new GridBagConstraints();
        gbc_lblTipologiaDip.insets = new Insets(0, 0, 5, 5);
        gbc_lblTipologiaDip.gridx = 0;
        gbc_lblTipologiaDip.gridy = 9;
        panelDipendenti.add(lblTipologiaDip, gbc_lblTipologiaDip);

        JComboBox comboTipologiaDip = new JComboBox(new String[] {"Operaio", "Venditore"});
        GridBagConstraints gbc_comboTipologiaDip = new GridBagConstraints();
        gbc_comboTipologiaDip.insets = new Insets(0, 0, 5, 5);
        gbc_comboTipologiaDip.gridx = 1;
        gbc_comboTipologiaDip.gridy = 9;
        panelDipendenti.add(comboTipologiaDip, gbc_comboTipologiaDip);

        GridBagConstraints gbc_btnInserisciDipendente = new GridBagConstraints();
        gbc_btnInserisciDipendente.insets = new Insets(0, 0, 0, 5);
        gbc_btnInserisciDipendente.gridx = 1;
        gbc_btnInserisciDipendente.gridy = 10;
        
        panelClienti.setBounds(351, 11, 262, 287);
        frame.getContentPane().add(panelClienti);
        GridBagLayout gbl_panelClienti = new GridBagLayout();
        gbl_panelClienti.columnWidths = new int[]{0, 0, 0, 0, 0};
        gbl_panelClienti.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gbl_panelClienti.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_panelClienti.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        panelClienti.setLayout(gbl_panelClienti);
        
        JLabel lblClienti = new JLabel("Clienti");
        GridBagConstraints gbc_lblClienti = new GridBagConstraints();
        gbc_lblClienti.insets = new Insets(0, 0, 5, 5);
        gbc_lblClienti.gridx = 1;
        gbc_lblClienti.gridy = 0;
        panelClienti.add(lblClienti, gbc_lblClienti);
        
        JLabel lblIdCliente = new JLabel("Id");
        GridBagConstraints gbc_lblIdCliente = new GridBagConstraints();
        gbc_lblIdCliente.insets = new Insets(0, 0, 5, 5);
        gbc_lblIdCliente.gridx = 0;
        gbc_lblIdCliente.gridy = 1;
        panelClienti.add(lblIdCliente, gbc_lblIdCliente);
        
        textIdCliente = new JTextField();
        GridBagConstraints gbc_textIdCliente = new GridBagConstraints();
        gbc_textIdCliente.insets = new Insets(0, 0, 5, 5);
        gbc_textIdCliente.gridx = 1;
        gbc_textIdCliente.gridy = 1;
        panelClienti.add(textIdCliente, gbc_textIdCliente);
        textIdCliente.setColumns(10);
        

        GridBagConstraints gbc_btnClearCliente = new GridBagConstraints();
        gbc_btnClearCliente.insets = new Insets(0, 0, 5, 5);
        gbc_btnClearCliente.gridx = 2;
        gbc_btnClearCliente.gridy = 1;
        
        JLabel lblNomeCliente = new JLabel("Nome");
        GridBagConstraints gbc_lblNomeCliente = new GridBagConstraints();
        gbc_lblNomeCliente.insets = new Insets(0, 0, 5, 5);
        gbc_lblNomeCliente.gridx = 0;
        gbc_lblNomeCliente.gridy = 2;
        panelClienti.add(lblNomeCliente, gbc_lblNomeCliente);
        
        textNomeCliente = new JTextField();
        GridBagConstraints gbc_textNomeCliente = new GridBagConstraints();
        gbc_textNomeCliente.insets = new Insets(0, 0, 5, 5);
        gbc_textNomeCliente.gridx = 1;
        gbc_textNomeCliente.gridy = 2;
        panelClienti.add(textNomeCliente, gbc_textNomeCliente);
        textNomeCliente.setColumns(10);
        
        JLabel lblCognomeCliente = new JLabel("  Cognome ");
        GridBagConstraints gbc_lblCognomeCliente = new GridBagConstraints();
        gbc_lblCognomeCliente.insets = new Insets(0, 0, 5, 5);
        gbc_lblCognomeCliente.gridx = 0;
        gbc_lblCognomeCliente.gridy = 3;
        panelClienti.add(lblCognomeCliente, gbc_lblCognomeCliente);
        
        textCognomeCliente = new JTextField();
        GridBagConstraints gbc_textCognomeCliente = new GridBagConstraints();
        gbc_textCognomeCliente.insets = new Insets(0, 0, 5, 5);
        gbc_textCognomeCliente.gridx = 1;
        gbc_textCognomeCliente.gridy = 3;
        panelClienti.add(textCognomeCliente, gbc_textCognomeCliente);
        textCognomeCliente.setColumns(10);
        
        JLabel lblViaCliente = new JLabel("Via");
        GridBagConstraints gbc_lblViaCliente = new GridBagConstraints();
        gbc_lblViaCliente.insets = new Insets(0, 0, 5, 5);
        gbc_lblViaCliente.gridx = 0;
        gbc_lblViaCliente.gridy = 4;
        panelClienti.add(lblViaCliente, gbc_lblViaCliente);
        
        textViaCliente = new JTextField();
        GridBagConstraints gbc_textViaCliente = new GridBagConstraints();
        gbc_textViaCliente.insets = new Insets(0, 0, 5, 5);
        gbc_textViaCliente.gridx = 1;
        gbc_textViaCliente.gridy = 4;
        panelClienti.add(textViaCliente, gbc_textViaCliente);
        textViaCliente.setColumns(10);
        
        JLabel lblNumeroCliente = new JLabel("Numero");
        GridBagConstraints gbc_lblNumeroCliente = new GridBagConstraints();
        gbc_lblNumeroCliente.insets = new Insets(0, 0, 5, 5);
        gbc_lblNumeroCliente.gridx = 0;
        gbc_lblNumeroCliente.gridy = 5;
        panelClienti.add(lblNumeroCliente, gbc_lblNumeroCliente);
        
        textNumeroCliente = new JTextField();
        GridBagConstraints gbc_textNumeroCliente = new GridBagConstraints();
        gbc_textNumeroCliente.insets = new Insets(0, 0, 5, 5);
        gbc_textNumeroCliente.gridx = 1;
        gbc_textNumeroCliente.gridy = 5;
        panelClienti.add(textNumeroCliente, gbc_textNumeroCliente);
        textNumeroCliente.setColumns(10);
        
        JLabel lblCittCliente = new JLabel("Citt\u00E0");
        GridBagConstraints gbc_lblCittCliente = new GridBagConstraints();
        gbc_lblCittCliente.insets = new Insets(0, 0, 5, 5);
        gbc_lblCittCliente.gridx = 0;
        gbc_lblCittCliente.gridy = 6;
        panelClienti.add(lblCittCliente, gbc_lblCittCliente);
        
        textCittaCliente = new JTextField();
        GridBagConstraints gbc_textCittaCliente = new GridBagConstraints();
        gbc_textCittaCliente.insets = new Insets(0, 0, 5, 5);
        gbc_textCittaCliente.gridx = 1;
        gbc_textCittaCliente.gridy = 6;
        panelClienti.add(textCittaCliente, gbc_textCittaCliente);
        textCittaCliente.setColumns(10);
        
        JLabel lblCapCliente = new JLabel("CAP");
        GridBagConstraints gbc_lblCapCliente = new GridBagConstraints();
        gbc_lblCapCliente.insets = new Insets(0, 0, 5, 5);
        gbc_lblCapCliente.gridx = 0;
        gbc_lblCapCliente.gridy = 7;
        panelClienti.add(lblCapCliente, gbc_lblCapCliente);
        
        textCAPCliente = new JTextField();
        GridBagConstraints gbc_textCAPCliente = new GridBagConstraints();
        gbc_textCAPCliente.insets = new Insets(0, 0, 5, 5);
        gbc_textCAPCliente.gridx = 1;
        gbc_textCAPCliente.gridy = 7;
        panelClienti.add(textCAPCliente, gbc_textCAPCliente);
        textCAPCliente.setColumns(10);
        
        JLabel lblTelefonoCliente = new JLabel("Telefono");
        GridBagConstraints gbc_lblTelefonoCliente = new GridBagConstraints();
        gbc_lblTelefonoCliente.insets = new Insets(0, 0, 5, 5);
        gbc_lblTelefonoCliente.gridx = 0;
        gbc_lblTelefonoCliente.gridy = 8;
        panelClienti.add(lblTelefonoCliente, gbc_lblTelefonoCliente);
        
        textTelefonoCliente = new JTextField();
        GridBagConstraints gbc_textTelefonoCliente = new GridBagConstraints();
        gbc_textTelefonoCliente.insets = new Insets(0, 0, 5, 5);
        gbc_textTelefonoCliente.gridx = 1;
        gbc_textTelefonoCliente.gridy = 8;
        panelClienti.add(textTelefonoCliente, gbc_textTelefonoCliente);
        textTelefonoCliente.setText("");
        textTelefonoCliente.setColumns(10);
        
        GridBagConstraints gbc_btnInserisciCliente = new GridBagConstraints();
        gbc_btnInserisciCliente.insets = new Insets(0, 0, 5, 5);
        gbc_btnInserisciCliente.gridx = 1;
        gbc_btnInserisciCliente.gridy = 9;
        
        panelMezzi.setBounds(700, 11, 262, 240);
        frame.getContentPane().add(panelMezzi);
        GridBagLayout gbl_panelMezzi = new GridBagLayout();
        gbl_panelMezzi.columnWidths = new int[]{0, 0, 0, 0};
        gbl_panelMezzi.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gbl_panelMezzi.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_panelMezzi.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        panelMezzi.setLayout(gbl_panelMezzi);
        
        JLabel lblMezzi = new JLabel("Mezzi");
        GridBagConstraints gbc_lblMezzi = new GridBagConstraints();
        gbc_lblMezzi.insets = new Insets(0, 0, 5, 5);
        gbc_lblMezzi.gridx = 1;
        gbc_lblMezzi.gridy = 0;
        panelMezzi.add(lblMezzi, gbc_lblMezzi);
        
        JLabel lblTelaio = new JLabel("Telaio");
        GridBagConstraints gbc_lblTelaio = new GridBagConstraints();
        gbc_lblTelaio.insets = new Insets(0, 0, 5, 5);
        gbc_lblTelaio.gridx = 0;
        gbc_lblTelaio.gridy = 1;
        panelMezzi.add(lblTelaio, gbc_lblTelaio);
        
        textTelaio = new JTextField();
        GridBagConstraints gbc_textTelaio = new GridBagConstraints();
        gbc_textTelaio.insets = new Insets(0, 0, 5, 5);
        gbc_textTelaio.gridx = 1;
        gbc_textTelaio.gridy = 1;
        panelMezzi.add(textTelaio, gbc_textTelaio);
        textTelaio.setColumns(10);
        
        GridBagConstraints gbc_btnClearMezzo = new GridBagConstraints();
        gbc_btnClearMezzo.insets = new Insets(0, 0, 5, 0);
        gbc_btnClearMezzo.gridx = 2;
        gbc_btnClearMezzo.gridy = 1;
        
        JLabel lblNomeMezzo = new JLabel("Nome");
        GridBagConstraints gbc_lblNomeMezzo = new GridBagConstraints();
        gbc_lblNomeMezzo.insets = new Insets(0, 0, 5, 5);
        gbc_lblNomeMezzo.gridx = 0;
        gbc_lblNomeMezzo.gridy = 2;
        panelMezzi.add(lblNomeMezzo, gbc_lblNomeMezzo);
        
        textNomeMezzo = new JTextField();
        GridBagConstraints gbc_textNomeMezzo = new GridBagConstraints();
        gbc_textNomeMezzo.insets = new Insets(0, 0, 5, 5);
        gbc_textNomeMezzo.gridx = 1;
        gbc_textNomeMezzo.gridy = 2;
        panelMezzi.add(textNomeMezzo, gbc_textNomeMezzo);
        textNomeMezzo.setColumns(10);
        
        JLabel lblTarga = new JLabel("Targa*");
        GridBagConstraints gbc_lblTarga = new GridBagConstraints();
        gbc_lblTarga.insets = new Insets(0, 0, 5, 5);
        gbc_lblTarga.gridx = 0;
        gbc_lblTarga.gridy = 3;
        panelMezzi.add(lblTarga, gbc_lblTarga);
        
        textTarga = new JTextField();
        GridBagConstraints gbc_textTarga = new GridBagConstraints();
        gbc_textTarga.insets = new Insets(0, 0, 5, 5);
        gbc_textTarga.gridx = 1;
        gbc_textTarga.gridy = 3;
        panelMezzi.add(textTarga, gbc_textTarga);
        textTarga.setText("");
        textTarga.setColumns(10);
        
        JLabel lblTipologiaMezzo = new JLabel("  Tipologia");
        GridBagConstraints gbc_lblTipologiaMezzo = new GridBagConstraints();
        gbc_lblTipologiaMezzo.insets = new Insets(0, 0, 5, 5);
        gbc_lblTipologiaMezzo.gridx = 0;
        gbc_lblTipologiaMezzo.gridy = 4;
        panelMezzi.add(lblTipologiaMezzo, gbc_lblTipologiaMezzo);
        
        textTipologiaMezzo = new JTextField();
        GridBagConstraints gbc_textTipologiaMezzo = new GridBagConstraints();
        gbc_textTipologiaMezzo.insets = new Insets(0, 0, 5, 5);
        gbc_textTipologiaMezzo.gridx = 1;
        gbc_textTipologiaMezzo.gridy = 4;
        panelMezzi.add(textTipologiaMezzo, gbc_textTipologiaMezzo);
        textTipologiaMezzo.setText("");
        textTipologiaMezzo.setColumns(10);
        

        GridBagConstraints gbc_btnInserisciMezzo = new GridBagConstraints();
        gbc_btnInserisciMezzo.insets = new Insets(0, 0, 5, 5);
        gbc_btnInserisciMezzo.gridx = 1;
        gbc_btnInserisciMezzo.gridy = 5;
        
        panelMacchinari.setBounds(1043, 11, 262, 108);
        frame.getContentPane().add(panelMacchinari);
        GridBagLayout gbl_panelMacchinari = new GridBagLayout();
        gbl_panelMacchinari.columnWidths = new int[]{0, 0, 0, 0, 0};
        gbl_panelMacchinari.rowHeights = new int[]{0, 0, 0, 0, 0};
        gbl_panelMacchinari.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_panelMacchinari.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        panelMacchinari.setLayout(gbl_panelMacchinari);
        
        JLabel lblMacchinari = new JLabel("Macchinari");
        GridBagConstraints gbc_lblMacchinari = new GridBagConstraints();
        gbc_lblMacchinari.insets = new Insets(0, 0, 5, 5);
        gbc_lblMacchinari.gridx = 1;
        gbc_lblMacchinari.gridy = 0;
        panelMacchinari.add(lblMacchinari, gbc_lblMacchinari);
        
        JLabel lblIdMacchinario = new JLabel("Id");
        GridBagConstraints gbc_lblIdMacchinario = new GridBagConstraints();
        gbc_lblIdMacchinario.insets = new Insets(0, 0, 5, 5);
        gbc_lblIdMacchinario.gridx = 0;
        gbc_lblIdMacchinario.gridy = 1;
        panelMacchinari.add(lblIdMacchinario, gbc_lblIdMacchinario);
        
        textIdMacchinario = new JTextField();
        GridBagConstraints gbc_textIdMacchinario = new GridBagConstraints();
        gbc_textIdMacchinario.insets = new Insets(0, 0, 5, 5);
        gbc_textIdMacchinario.gridx = 1;
        gbc_textIdMacchinario.gridy = 1;
        panelMacchinari.add(textIdMacchinario, gbc_textIdMacchinario);
        textIdMacchinario.setColumns(10);
        
        GridBagConstraints gbc_btnClearMacchinario = new GridBagConstraints();
        gbc_btnClearMacchinario.gridwidth = 2;
        gbc_btnClearMacchinario.insets = new Insets(0, 0, 5, 5);
        gbc_btnClearMacchinario.gridx = 2;
        gbc_btnClearMacchinario.gridy = 1;
        
        JLabel lblNomeMacchinario = new JLabel("Nome");
        GridBagConstraints gbc_lblNomeMacchinario = new GridBagConstraints();
        gbc_lblNomeMacchinario.insets = new Insets(0, 0, 5, 5);
        gbc_lblNomeMacchinario.gridx = 0;
        gbc_lblNomeMacchinario.gridy = 2;
        panelMacchinari.add(lblNomeMacchinario, gbc_lblNomeMacchinario);
        
        textNomeMacchinario = new JTextField();
        GridBagConstraints gbc_textNomeMacchinario = new GridBagConstraints();
        gbc_textNomeMacchinario.insets = new Insets(0, 0, 5, 5);
        gbc_textNomeMacchinario.gridx = 1;
        gbc_textNomeMacchinario.gridy = 2;
        panelMacchinari.add(textNomeMacchinario, gbc_textNomeMacchinario);
        textNomeMacchinario.setColumns(10);
        frame.setVisible(true);


        GridBagConstraints gbc_btnInserisciMacchinario = new GridBagConstraints();
        gbc_btnInserisciMacchinario.insets = new Insets(0, 0, 0, 5);
        gbc_btnInserisciMacchinario.gridx = 1;
        gbc_btnInserisciMacchinario.gridy = 3;
        
        panelProdotti.setBounds(1002, 155, 323, 282);
        frame.getContentPane().add(panelProdotti);
        GridBagLayout gbl_panelProdotti = new GridBagLayout();
        gbl_panelProdotti.columnWidths = new int[]{0, 0, 0, 0};
        gbl_panelProdotti.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gbl_panelProdotti.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_panelProdotti.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        panelProdotti.setLayout(gbl_panelProdotti);
        
        JLabel lblCatalogoProdotti = new JLabel("Catalogo Prodotti");
        GridBagConstraints gbc_lblCatalogoProdotti = new GridBagConstraints();
        gbc_lblCatalogoProdotti.insets = new Insets(0, 0, 5, 5);
        gbc_lblCatalogoProdotti.gridx = 1;
        gbc_lblCatalogoProdotti.gridy = 0;
        panelProdotti.add(lblCatalogoProdotti, gbc_lblCatalogoProdotti);
        
        JLabel lblIdProdotto = new JLabel("Id");
        GridBagConstraints gbc_lblIdProdotto = new GridBagConstraints();
        gbc_lblIdProdotto.insets = new Insets(0, 0, 5, 5);
        gbc_lblIdProdotto.gridx = 0;
        gbc_lblIdProdotto.gridy = 1;
        panelProdotti.add(lblIdProdotto, gbc_lblIdProdotto);
        
        textIdProdotto = new JTextField();
        GridBagConstraints gbc_textIdProdotto = new GridBagConstraints();
        gbc_textIdProdotto.insets = new Insets(0, 0, 5, 5);
        gbc_textIdProdotto.gridx = 1;
        gbc_textIdProdotto.gridy = 1;
        panelProdotti.add(textIdProdotto, gbc_textIdProdotto);
        textIdProdotto.setColumns(10);
        
        GridBagConstraints gbc_btnClearProdotto = new GridBagConstraints();
        gbc_btnClearProdotto.insets = new Insets(0, 0, 5, 0);
        gbc_btnClearProdotto.gridx = 2;
        gbc_btnClearProdotto.gridy = 1;
        
        JLabel lblNomeProdotto = new JLabel("Nome");
        GridBagConstraints gbc_lblNomeProdotto = new GridBagConstraints();
        gbc_lblNomeProdotto.insets = new Insets(0, 0, 5, 5);
        gbc_lblNomeProdotto.gridx = 0;
        gbc_lblNomeProdotto.gridy = 2;
        panelProdotti.add(lblNomeProdotto, gbc_lblNomeProdotto);
        
        textNomeProdotto = new JTextField();
        GridBagConstraints gbc_textNomeProdotto = new GridBagConstraints();
        gbc_textNomeProdotto.insets = new Insets(0, 0, 5, 5);
        gbc_textNomeProdotto.gridx = 1;
        gbc_textNomeProdotto.gridy = 2;
        panelProdotti.add(textNomeProdotto, gbc_textNomeProdotto);
        textNomeProdotto.setColumns(10);
        
        JLabel lblPrezzoProdotto = new JLabel("Prezzo");
        GridBagConstraints gbc_lblPrezzoProdotto = new GridBagConstraints();
        gbc_lblPrezzoProdotto.insets = new Insets(0, 0, 5, 5);
        gbc_lblPrezzoProdotto.gridx = 0;
        gbc_lblPrezzoProdotto.gridy = 3;
        panelProdotti.add(lblPrezzoProdotto, gbc_lblPrezzoProdotto);
        
        textPrezzoProdotto = new JTextField();
        GridBagConstraints gbc_textPrezzoProdotto = new GridBagConstraints();
        gbc_textPrezzoProdotto.insets = new Insets(0, 0, 5, 5);
        gbc_textPrezzoProdotto.gridx = 1;
        gbc_textPrezzoProdotto.gridy = 3;
        panelProdotti.add(textPrezzoProdotto, gbc_textPrezzoProdotto);
        textPrezzoProdotto.setColumns(10);
        
        JLabel lblPeriodoTipico = new JLabel("Periodo tipico");
        GridBagConstraints gbc_lblPeriodoTipico = new GridBagConstraints();
        gbc_lblPeriodoTipico.insets = new Insets(0, 0, 5, 5);
        gbc_lblPeriodoTipico.gridx = 0;
        gbc_lblPeriodoTipico.gridy = 4;
        panelProdotti.add(lblPeriodoTipico, gbc_lblPeriodoTipico);
        
        JComboBox comboPeriodoTipico = new JComboBox();
        GridBagConstraints gbc_comboPeriodoTipico = new GridBagConstraints();
        gbc_comboPeriodoTipico.insets = new Insets(0, 0, 5, 5);
        gbc_comboPeriodoTipico.gridx = 1;
        gbc_comboPeriodoTipico.gridy = 4;
        panelProdotti.add(comboPeriodoTipico, gbc_comboPeriodoTipico);
        comboPeriodoTipico.setModel(new DefaultComboBoxModel(new String[] {"Primavera", "Estate", "Autunno", "Inverno"}));
        
        JLabel lblTempoMaturazione = new JLabel("Tempo matur.");
        GridBagConstraints gbc_lblTempoMaturazione = new GridBagConstraints();
        gbc_lblTempoMaturazione.insets = new Insets(0, 0, 5, 5);
        gbc_lblTempoMaturazione.gridx = 0;
        gbc_lblTempoMaturazione.gridy = 5;
        panelProdotti.add(lblTempoMaturazione, gbc_lblTempoMaturazione);
        
        textTempoMat = new JTextField();
        GridBagConstraints gbc_textTempoMat = new GridBagConstraints();
        gbc_textTempoMat.insets = new Insets(0, 0, 5, 5);
        gbc_textTempoMat.gridx = 1;
        gbc_textTempoMat.gridy = 5;
        panelProdotti.add(textTempoMat, gbc_textTempoMat);
        textTempoMat.setColumns(10);
        
        panelLavori.setBounds(1002, 448, 323, 184);
        frame.getContentPane().add(panelLavori);
        GridBagLayout gbl_panelLavori = new GridBagLayout();
        gbl_panelLavori.columnWidths = new int[]{0, 0, 0, 0};
        gbl_panelLavori.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
        gbl_panelLavori.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_panelLavori.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        panelLavori.setLayout(gbl_panelLavori);
        
        JLabel lblLavori = new JLabel("Lavori");
        GridBagConstraints gbc_lblLavori = new GridBagConstraints();
        gbc_lblLavori.insets = new Insets(0, 0, 5, 5);
        gbc_lblLavori.gridx = 1;
        gbc_lblLavori.gridy = 0;
        panelLavori.add(lblLavori, gbc_lblLavori);
        
        JLabel lblIdlavoro = new JLabel("IdLavoro");
        GridBagConstraints gbc_lblIdlavoro = new GridBagConstraints();
        gbc_lblIdlavoro.insets = new Insets(0, 0, 5, 5);
        gbc_lblIdlavoro.gridx = 0;
        gbc_lblIdlavoro.gridy = 1;
        panelLavori.add(lblIdlavoro, gbc_lblIdlavoro);
        
        textIdLavoro = new JTextField();
        GridBagConstraints gbc_textIdLavoro = new GridBagConstraints();
        gbc_textIdLavoro.insets = new Insets(0, 0, 5, 5);
        gbc_textIdLavoro.gridx = 1;
        gbc_textIdLavoro.gridy = 1;
        panelLavori.add(textIdLavoro, gbc_textIdLavoro);
        textIdLavoro.setColumns(10);
        
        GridBagConstraints gbc_btnClearLavoro = new GridBagConstraints();
        gbc_btnClearLavoro.insets = new Insets(0, 0, 5, 0);
        gbc_btnClearLavoro.gridx = 2;
        gbc_btnClearLavoro.gridy = 1;
        
        JLabel lblNomeLavoro = new JLabel("Nome");
        GridBagConstraints gbc_lblNomeLavoro = new GridBagConstraints();
        gbc_lblNomeLavoro.insets = new Insets(0, 0, 5, 5);
        gbc_lblNomeLavoro.gridx = 0;
        gbc_lblNomeLavoro.gridy = 2;
        panelLavori.add(lblNomeLavoro, gbc_lblNomeLavoro);
        
        textNomeLavoro = new JTextField();
        GridBagConstraints gbc_textNomeLavoro = new GridBagConstraints();
        gbc_textNomeLavoro.insets = new Insets(0, 0, 5, 5);
        gbc_textNomeLavoro.gridx = 1;
        gbc_textNomeLavoro.gridy = 2;
        panelLavori.add(textNomeLavoro, gbc_textNomeLavoro);
        textNomeLavoro.setColumns(10);
        
        JLabel lblTipologiaLavoro = new JLabel("Tipologia");
        GridBagConstraints gbc_lblTipologiaLavoro = new GridBagConstraints();
        gbc_lblTipologiaLavoro.insets = new Insets(0, 0, 5, 5);
        gbc_lblTipologiaLavoro.gridx = 0;
        gbc_lblTipologiaLavoro.gridy = 3;
        panelLavori.add(lblTipologiaLavoro, gbc_lblTipologiaLavoro);
        
        JComboBox comboBoxTipologiaLavoro = new JComboBox(new String[] {"Semina", "Trattamento", "Lavorazione Terreno"});
        GridBagConstraints gbc_comboBoxTipologiaLavoro = new GridBagConstraints();
        gbc_comboBoxTipologiaLavoro.insets = new Insets(0, 0, 5, 5);
        gbc_comboBoxTipologiaLavoro.gridx = 1;
        gbc_comboBoxTipologiaLavoro.gridy = 3;
        panelLavori.add(comboBoxTipologiaLavoro, gbc_comboBoxTipologiaLavoro);
        
        JLabel lblTipologiaTrattamento = new JLabel("Tipologia t\r\nratt.*");
        GridBagConstraints gbc_lblTipologiaTrattamento = new GridBagConstraints();
        gbc_lblTipologiaTrattamento.insets = new Insets(0, 0, 5, 5);
        gbc_lblTipologiaTrattamento.gridx = 0;
        gbc_lblTipologiaTrattamento.gridy = 4;
        panelLavori.add(lblTipologiaTrattamento, gbc_lblTipologiaTrattamento);
        
        JComboBox comboBoxTipologiaTratt = new JComboBox(new String[] {"Chimico", "Biologico"});
        GridBagConstraints gbc_comboBoxTipologiaTratt = new GridBagConstraints();
        gbc_comboBoxTipologiaTratt.insets = new Insets(0, 0, 5, 5);
        gbc_comboBoxTipologiaTratt.gridx = 1;
        gbc_comboBoxTipologiaTratt.gridy = 4;
        panelLavori.add(comboBoxTipologiaTratt, gbc_comboBoxTipologiaTratt);
        
        GridBagConstraints gbc_btnInserisciLavoro = new GridBagConstraints();
        gbc_btnInserisciLavoro.insets = new Insets(0, 0, 0, 5);
        gbc_btnInserisciLavoro.gridx = 1;
        gbc_btnInserisciLavoro.gridy = 5;

        JLabel lblTerrenoIdeale = new JLabel("Terreno ideale");
        GridBagConstraints gbc_lblTerrenoIdeale = new GridBagConstraints();
        gbc_lblTerrenoIdeale.insets = new Insets(0, 0, 5, 5);
        gbc_lblTerrenoIdeale.gridx = 0;
        gbc_lblTerrenoIdeale.gridy = 6;
        panelProdotti.add(lblTerrenoIdeale, gbc_lblTerrenoIdeale);
        
        textTerrenoIdeale = new JTextField();
        GridBagConstraints gbc_textTerrenoIdeale = new GridBagConstraints();
        gbc_textTerrenoIdeale.insets = new Insets(0, 0, 5, 5);
        gbc_textTerrenoIdeale.gridx = 1;
        gbc_textTerrenoIdeale.gridy = 6;
        panelProdotti.add(textTerrenoIdeale, gbc_textTerrenoIdeale);
        textTerrenoIdeale.setColumns(10);
        
        JLabel lblScorte = new JLabel("Scorte");
        GridBagConstraints gbc_lblScorte = new GridBagConstraints();
        gbc_lblScorte.insets = new Insets(0, 0, 5, 5);
        gbc_lblScorte.gridx = 0;
        gbc_lblScorte.gridy = 7;
        panelProdotti.add(lblScorte, gbc_lblScorte);
        
        textScorte = new JTextField();
        GridBagConstraints gbc_textScorte = new GridBagConstraints();
        gbc_textScorte.insets = new Insets(0, 0, 5, 5);
        gbc_textScorte.gridx = 1;
        gbc_textScorte.gridy = 7;
        panelProdotti.add(textScorte, gbc_textScorte);
        textScorte.setColumns(10);
        
        GridBagConstraints gbc_chckbxBio = new GridBagConstraints();
        gbc_chckbxBio.insets = new Insets(0, 0, 5, 5);
        gbc_chckbxBio.gridx = 1;
        gbc_chckbxBio.gridy = 8;
        panelProdotti.add(chckbxBio, gbc_chckbxBio);

        GridBagConstraints gbc_btnInserisciProdotto = new GridBagConstraints();
        gbc_btnInserisciProdotto.insets = new Insets(0, 0, 0, 5);
        gbc_btnInserisciProdotto.gridx = 1;
        gbc_btnInserisciProdotto.gridy = 9;
        
        JComboBox comboBoxScelta = new JComboBox();
        comboBoxScelta.setModel(new DefaultComboBoxModel(new String[] {"Dipendenti", "Clienti", "Mezzi", "Macchinari", "Catalogo Prodotti", "Lavori"}));
        comboBoxScelta.setBounds(137, 373, 89, 23);
        frame.getContentPane().add(comboBoxScelta);
        
        JLabel lblVisualizza = new JLabel("Visualizza:");
        lblVisualizza.setBounds(69, 377, 70, 14);
        frame.getContentPane().add(lblVisualizza);
        
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(26, 429, 835, 203);
        frame.getContentPane().add(scrollPane);
        table = new JTable();
        scrollPane.setViewportView(table);

        JButton btnInserisciDipendente = new JButton("Inserisci");
        btnInserisciDipendente.setBackground(SystemColor.desktop);
        btnInserisciDipendente.addActionListener(al -> {
            Dipendente dip = new Dipendente();
            try {
                dip.setIdDipendente(Integer.parseInt(textIdDip.getText()));
                dip.setNome(textNomeDip.getText());
                dip.setCognome(textCognomeDip.getText());
                dip.setVia(textViaDip.getText());
                dip.setNumero(textNumeroDip.getText());
                dip.setCitta(textCittaDip.getText());
                dip.setTelefono(textTelefonoDip.getText());
                dip.setStipendio(Double.parseDouble(textStipendioDip.getText()));
                dip.setTipologia(comboTipologiaDip.getSelectedItem().toString());
                String res = DipendenteTable.getTable().persist(dip);
                printAllDipendenti();
                JOptionPane.showMessageDialog(frame, res);
            } catch (NumberFormatException e){
                JOptionPane.showMessageDialog(frame, "Errore: nelle caselle IdDipendente e Stipendio va inserito un numero!");
            }
        });
        panelDipendenti.add(btnInserisciDipendente, gbc_btnInserisciDipendente);

        JButton btnInserisciCliente = new JButton("Inserisci");
        btnInserisciCliente.setBackground(SystemColor.desktop);
        btnInserisciCliente.addActionListener(al -> {
            Cliente cliente = new Cliente();
            try {
                cliente.setIdCliente(Integer.parseInt(textIdCliente.getText()));
                cliente.setNome(textNomeCliente.getText());
                cliente.setCognome(textCognomeCliente.getText());
                cliente.setVia(textViaCliente.getText());
                cliente.setNumero(textNumeroCliente.getText());
                cliente.setCitta(textCittaCliente.getText());
                cliente.setCap(textCAPCliente.getText());
                cliente.setTelefono(textTelefonoCliente.getText());
                String res = ClienteTable.getTable().persist(cliente);
                printAllClienti();
                JOptionPane.showMessageDialog(frame, res);
            } catch (NumberFormatException e){
                JOptionPane.showMessageDialog(frame, "Errore: nella casella IdCliente va inserito un numero!");
            }
        });
        panelClienti.add(btnInserisciCliente, gbc_btnInserisciCliente);

        JButton btnInserisciMezzo = new JButton("Inserisci");
        btnInserisciMezzo.setBackground(SystemColor.desktop);
        btnInserisciMezzo.addActionListener(al -> {
            Mezzo mezzo = new Mezzo();
            mezzo.setTelaio(textTelaio.getText());
            mezzo.setNome(textNomeMezzo.getText());
            mezzo.setTarga(textTarga.getText().length() == 0 ? Optional.empty() : Optional.of(textTarga.getText()));
            mezzo.setTipologia(textTipologiaMezzo.getText());
            String res = MezzoTable.getTable().persist(mezzo);
            printAllMezzi();
            JOptionPane.showMessageDialog(frame, res);
        });
        panelMezzi.add(btnInserisciMezzo, gbc_btnInserisciMezzo);

        JButton btnInserisciMacchinario = new JButton("Inserisci");
        btnInserisciMacchinario.setBackground(SystemColor.desktop);
        btnInserisciMacchinario.addActionListener(al -> {
            try{
                Macchinario macchinario = new Macchinario();
                macchinario.setIdMacchinario(Integer.parseInt(textIdMacchinario.getText()));
                macchinario.setNome(textNomeMacchinario.getText());
                EventQueue.invokeLater(() -> new MacchinLavoroInsertWindow(null, macchinario));
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(frame, "Errore: nella casella IdMacchinario va inserito un numero!");
            }
        });
        panelMacchinari.add(btnInserisciMacchinario, gbc_btnInserisciMacchinario);
        
        JButton btnInserisciLavoro = new JButton("Inserisci");
        btnInserisciLavoro.setBackground(SystemColor.desktop);
        btnInserisciLavoro.addActionListener(al -> {
            try{
                Lavoro lavoro = new Lavoro();
                lavoro.setIdLavoro(Integer.parseInt(textIdLavoro.getText()));
                lavoro.setNome(textNomeLavoro.getText());
                String resComboTipologia = comboBoxTipologiaLavoro.getSelectedItem().toString();
                lavoro.setTipologia(resComboTipologia);
                if(resComboTipologia == "Trattamento") {
                    lavoro.setTipologiaTrattamento(Optional.of(comboBoxTipologiaTratt.getSelectedItem().toString()));
                } else {
                    lavoro.setTipologiaTrattamento(Optional.empty());
                }
                EventQueue.invokeLater(() -> new MacchinLavoroInsertWindow(lavoro, null));
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(frame, "Errore: nella casella IdLavoro va inserito un numero!");
            }
        });
        panelLavori.add(btnInserisciLavoro, gbc_btnInserisciLavoro);
        
        JButton btnInserisciProdotto = new JButton("Inserisci");
        btnInserisciProdotto.setBackground(SystemColor.desktop);
        btnInserisciProdotto.addActionListener(al -> {
            try{
                Prodotto prod = new Prodotto();
                prod.setIdProdotto(Integer.parseInt(textIdProdotto.getText()));
                prod.setNome(textNomeProdotto.getText());
                prod.setPrezzo(Double.parseDouble(textPrezzoProdotto.getText()));
                prod.setPeriodoTipico(comboPeriodoTipico.getSelectedItem().toString());
                prod.setTempoMaturazione(Integer.parseInt(textTempoMat.getText()));
                prod.setTerrenoIdeale(textTerrenoIdeale.getText());
                prod.setScorte(Integer.parseInt(textScorte.getText()));
                prod.setBio(chckbxBio.isSelected());
                EventQueue.invokeLater(() -> new InsertTreatmentWindow(prod)); 
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(frame, "Errore: nelle caselle IdProdotto, Prezzo, Tempo matur. e Scorte va inserito un numero!");
            }
        });
        panelProdotti.add(btnInserisciProdotto, gbc_btnInserisciProdotto);
        
        JButton btnClearDipendente = new JButton("Clear");
        btnClearDipendente.setBackground(SystemColor.desktop);
        btnClearDipendente.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                clearAllTextField(panelDipendenti);
            }
        });
        panelDipendenti.add(btnClearDipendente, gbc_btnClearDipendente);
        
        JButton btnClearCliente = new JButton("Clear");
        btnClearCliente.setBackground(SystemColor.desktop);
        btnClearCliente.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                clearAllTextField(panelClienti);
            }
        });
        panelClienti.add(btnClearCliente, gbc_btnClearCliente);
        
        JButton btnClearMezzo = new JButton("Clear");
        btnClearMezzo.setBackground(SystemColor.desktop);
        btnClearMezzo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                clearAllTextField(panelMezzi);
            }
        });
        panelMezzi.add(btnClearMezzo, gbc_btnClearMezzo);
        
        JButton btnClearMacchinario = new JButton("Clear");
        btnClearMacchinario.setBackground(SystemColor.desktop);
        btnClearMacchinario.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                clearAllTextField(panelMacchinari);
            }
        });
        panelMacchinari.add(btnClearMacchinario, gbc_btnClearMacchinario);
        
        JButton btnClearProdotto = new JButton("Clear");
        btnClearProdotto.setBackground(SystemColor.desktop);
        btnClearProdotto.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                clearAllTextField(panelProdotti);
            }
        });
        panelProdotti.add(btnClearProdotto, gbc_btnClearProdotto);

        JButton btnClearLavoro = new JButton("Clear");
        btnClearLavoro.setBackground(SystemColor.desktop);
        btnClearLavoro.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                clearAllTextField(panelLavori);
            }
        });
        panelLavori.add(btnClearLavoro, gbc_btnClearLavoro);
        
        JButton btnEsegui = new JButton("Esegui");
        btnEsegui.setBackground(SystemColor.activeCaption);
        btnEsegui.addActionListener(al -> {
	    switch (comboBoxScelta.getSelectedItem().toString()){
	        case "Dipendenti":
	            printAllDipendenti();
	        break;
	        case "Clienti":
	            printAllClienti();
	        break;
	        case "Mezzi":
	            printAllMezzi();
	        break;
	        case "Macchinari":
	            printAllMacchinari();
	        break;
	        case "Catalogo Prodotti":
	            printAllProdotti(false);
	        break;
	        case "Lavori":
	            printAllLavori();
	        break;
	        default:
	    }
        });
        btnEsegui.setBounds(248, 364, 106, 40);
        frame.getContentPane().add(btnEsegui);

        JButton btnControlli = new JButton("Controlli");
        btnControlli.addActionListener(al -> EventQueue.invokeLater(() -> new ChecksWindow()));
        btnControlli.setBackground(SystemColor.activeCaption);
        btnControlli.setBounds(376, 364, 106, 40);
        frame.getContentPane().add(btnControlli);

        switchComponents(panelDipendenti, false);
        switchComponents(panelClienti, false);
        switchComponents(panelMezzi, false);
        switchComponents(panelMacchinari, false);
        switchComponents(panelProdotti, false);
        switchComponents(panelLavori, false);
    }

    private void switchComponents(JPanel panel, boolean enable) {
        for (Component c : panel.getComponents()) {
            c.setEnabled(enable);
        }
    }

    private void clearAllTextField(JPanel panel) {
        for (Component c : panel.getComponents()) {
            if(c instanceof JTextField){
                ((JTextField) c).setText("");
            }
        }
    }
    
    private void printAllDipendenti() {
        List<Dipendente> totDipendenti = DipendenteTable.getTable().findAll();
        String[] nomiAttributi = DipendenteTable.getTable().getAttributes();
        DefaultTableModel model = createNotEditableModel(nomiAttributi);
        for (Dipendente dip : totDipendenti) {
            model.addRow(dip.concatAttributes().toArray());
        }
        table.setModel(model);
    }
    
    private void printAllClienti() {
        List<Cliente> totClienti = ClienteTable.getTable().findAll();
        String[] nomiAttributi = ClienteTable.getTable().getAttributes();
        DefaultTableModel model = createNotEditableModel(nomiAttributi);
        for (Cliente cl : totClienti){
            model.addRow(cl.concatAttributes().toArray());
        }
        table.setModel(model);
    }
    
    private void printAllMezzi() {
        List<Mezzo> totMezzi = MezzoTable.getTable().findAll();
        String[] nomiAttributi = MezzoTable.getTable().getAttributes();
        DefaultTableModel model = createNotEditableModel(nomiAttributi);
        for (Mezzo me : totMezzi) {
            model.addRow(me.concatAttributes().toArray());
        }
        table.setModel(model);
    }
    
    private void printAllMacchinari() {
        List<Macchinario> totMacchinari = MacchinarioTable.getTable().findAll();
        String[] nomiAttributi = MacchinarioTable.getTable().getAttributes();
        DefaultTableModel model = createNotEditableModel(nomiAttributi);
        for (Macchinario macc : totMacchinari) {
            model.addRow(macc.concatAttributes().toArray());
        }
        table.setModel(model);
    }
    
    private void printAllProdotti(boolean bio) {
        List<Prodotto> totProdotti = ProdottoTable.getTable().findAllCustom(bio);
        String[] nomiAttributi = ProdottoTable.getTable().getAttributesCustom();
        DefaultTableModel model = createNotEditableModel(nomiAttributi);
        for (Prodotto prod : totProdotti) {
            model.addRow(prod.concatAttributesCustom().toArray());
        }
        table.setModel(model);
    }
    
    private void printAllLavori() {
        List<Lavoro> totLavori = LavoroTable.getTable().findAll();
        String[] nomiAttributi = LavoroTable.getTable().getAttributes();
        DefaultTableModel model = createNotEditableModel(nomiAttributi);
        for (Lavoro lav : totLavori) {
            model.addRow(lav.concatAttributes().toArray());
        }
        table.setModel(model);
    }

    private DefaultTableModel createNotEditableModel(String[] nomiAttributi) {
        return new DefaultTableModel(nomiAttributi, 0) {
            private static final long serialVersionUID = -9140705710062485906L;
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
    }
}
