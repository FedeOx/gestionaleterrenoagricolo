package view.admin;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import model.Lavoro;
import model.MacchinLavoro;
import model.Macchinario;
import model.database.LavoroTable;
import model.database.MacchinLavoroTable;
import model.database.MacchinarioTable;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.SystemColor;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * DB administrator GUI for insertion of possible works associated machinery.
 */
//CHECKSTYLE:OFF
@SuppressWarnings("all")
public class MacchinLavoroInsertWindow {
    private JFrame frame;
    private Lavoro lavoro;
    private Macchinario macchinario;
    private JTable tableVisualizzazione;
    /**
     * Create the application.
     */
    public MacchinLavoroInsertWindow(Lavoro lavoro, Macchinario macchinario) {
        this.lavoro = lavoro;
        this.macchinario = macchinario;
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame("Insert Macchinario-Lavoro junction");
        frame.setBounds(100, 100, 640, 320);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        frame.setVisible(true);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(20, 47, 594, 224);
        frame.getContentPane().add(scrollPane);

        tableVisualizzazione = new JTable();
        scrollPane.setViewportView(tableVisualizzazione);
        String[] nomiAttributi = null;
        DefaultTableModel model = null;
        if(this.macchinario == null){
            List<Macchinario> totMacchinari = MacchinarioTable.getTable().findAll();
            nomiAttributi = MacchinarioTable.getTable().getAttributes();
            model = new DefaultTableModel(nomiAttributi, 0);
            for (Macchinario macch : totMacchinari) {
                model.addRow(macch.concatAttributes().toArray());
            }
        } else if(this.lavoro == null) {
            List<Lavoro> totLavori = LavoroTable.getTable().findAll();
            nomiAttributi = LavoroTable.getTable().getAttributes();
            model = new DefaultTableModel(nomiAttributi, 0);
            for (Lavoro lav : totLavori) {
                model.addRow(lav.concatAttributes().toArray());
            }
        }
        tableVisualizzazione.setModel(model);

        JButton btnAggiungiElementi = new JButton("Aggiungi elementi sezionati");
        btnAggiungiElementi.addActionListener(al -> {
            final int[] rows = tableVisualizzazione.getSelectedRows();
            if (this.macchinario == null && rows.length > 0) {
                int ris = LavoroTable.getTable().persist(this.lavoro);
                if(ris == 0) {
                    for (int row : rows) {
                        int selectedId = Integer.parseInt(tableVisualizzazione.getValueAt(row, 0).toString());
                        MacchinLavoro ml = new MacchinLavoro();
                        ml.setIdLavoro(this.lavoro.getIdLavoro());
                        ml.setIdMacchinario(selectedId);
                        MacchinLavoroTable.getTable().persist(ml);
                    }
                    JOptionPane.showMessageDialog(frame, "Lavoro inserito correttamento");
                } else {
                    JOptionPane.showMessageDialog(frame, "Errore di inserimento nel DB durante l'inserimento del lavoro");
                }
                frame.dispose();
            } else if(this.lavoro == null && rows.length > 0) {
                int ris = MacchinarioTable.getTable().persist(this.macchinario);
                if(ris == 0) {
                    for (int row : rows) {
                        int selectedId = Integer.parseInt(tableVisualizzazione.getValueAt(row, 0).toString());
                        MacchinLavoro ml = new MacchinLavoro();
                        ml.setIdLavoro(selectedId);
                        ml.setIdMacchinario(this.macchinario.getIdMacchinario());
                        MacchinLavoroTable.getTable().persist(ml);
                    }
                    JOptionPane.showMessageDialog(frame, "Macchinario inserito correttamento");
                } else {
                    JOptionPane.showMessageDialog(frame, "Errore di inserimento nel DB durante l'inserimento del macchinario");
                }
                frame.dispose();
            } else {
                JOptionPane.showMessageDialog(frame, "Selezionare almeno una riga");
            }
        });
        btnAggiungiElementi.setBackground(SystemColor.activeCaption);
        btnAggiungiElementi.setBounds(20, 11, 205, 35);
        frame.getContentPane().add(btnAggiungiElementi);
    }
    
}
