package view.admin;

import javax.swing.JFrame;

import model.Mezzo;
import model.Revisione;
import model.database.RevisioneTable;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.SystemColor;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.swing.JTextArea;

/**
 * DB administrator GUI for insertion a new invoice.
 */
//CHECKSTYLE:OFF
@SuppressWarnings("all")
public class InsertInvoiceWindow {
    private final Mezzo vehicle;
    private Revisione ultimaRevisione;
    private static final int ANNI_SCADENZA = 5;
    /**
     * Create the application.
     */
    public InsertInvoiceWindow(final Mezzo vehicle) {
        this.vehicle = vehicle;
        this.ultimaRevisione = null;
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        JTextField textCosto;
        JFrame frame;
        frame = new JFrame("Insert invoice");
        frame.setBounds(100, 100, 384, 300);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(27, 26, 324, 69);
        frame.getContentPane().add(scrollPane);
        
        JList<String> listInfoMezzo = new JList<>();
        scrollPane.setViewportView(listInfoMezzo);
        
        JLabel lblInfoMezzo = new JLabel("Info mezzo");
        lblInfoMezzo.setBounds(27, 11, 60, 14);
        frame.getContentPane().add(lblInfoMezzo);
        DefaultListModel<String> model = new DefaultListModel<>();
        
        textCosto = new JTextField();
        textCosto.setBounds(58, 120, 102, 20);
        frame.getContentPane().add(textCosto);
        textCosto.setColumns(10);
        
        JLabel lblCosto = new JLabel("Costo");
        lblCosto.setBounds(12, 123, 36, 14);
        frame.getContentPane().add(lblCosto);
        
        JLabel lblDescrizione = new JLabel("Descrizione");
        lblDescrizione.setBounds(209, 106, 60, 14);
        frame.getContentPane().add(lblDescrizione);
        
        JTextArea textDescrizione = new JTextArea();
        textDescrizione.setBounds(209, 118, 124, 103);
        frame.getContentPane().add(textDescrizione);
        frame.setVisible(true);
        
        model.clear();
        listInfoMezzo.setModel(model);
        model.addElement(this.vehicle.getTelaio());
        model.addElement(this.vehicle.getNome());
        model.addElement(this.vehicle.getTipologia());
        List<Revisione> revisioniEffettuate = RevisioneTable.getTable().findRevisionByVeihicleFrame(this.vehicle.getTelaio());
        if(!revisioniEffettuate.isEmpty()) {
            this.ultimaRevisione = revisioniEffettuate.stream().max(Comparator.comparing(Revisione::getData)).get();
            model.addElement("Ultima revisione: " + new SimpleDateFormat("dd/MM/yyyy").format(ultimaRevisione.getData()));
        }
        listInfoMezzo.setModel(model);
        
        JButton btnInserisci = new JButton("Inserisci");
        btnInserisci.addActionListener(al -> {
            Revisione rev = new Revisione();
            if(this.ultimaRevisione != null) {
                if((Integer.parseInt(new SimpleDateFormat("yyyy").format(this.ultimaRevisione.getData())) + ANNI_SCADENZA) >= Calendar.getInstance().get(Calendar.YEAR))
                {
                    JOptionPane.showMessageDialog(frame, "La revisione non è ancora scaduta");
                    return;
                }
            }
            try {
                rev.setTelaioMezzo(this.vehicle.getTelaio());
                rev.setData(new Date());
                rev.setCosto(Double.parseDouble(textCosto.getText()));
                rev.setDescrizione(textDescrizione.getText().length() == 0 ? Optional.empty() : Optional.of(textDescrizione.getText()));
                String ris = RevisioneTable.getTable().persist(rev);
                JOptionPane.showMessageDialog(frame, ris);
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(frame, "Il campo Costo deve contenere un numero!");
                }            
        });
        btnInserisci.setBackground(SystemColor.activeCaption);
        btnInserisci.setBounds(58, 170, 102, 31);
        frame.getContentPane().add(btnInserisci);
    }
}
