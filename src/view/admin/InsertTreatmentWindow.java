package view.admin;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import model.Lavoro;
import model.ProdTratt;
import model.Prodotto;
import model.database.LavoroTable;
import model.database.ProdTrattTable;
import model.database.ProdottoTable;

import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.SystemColor;

/**
 * DB administrator GUI for insertion a new invoice.
 */
//CHECKSTYLE:OFF
@SuppressWarnings("all")
public class InsertTreatmentWindow {
    private JFrame frame;
    private Prodotto prodotto;
    private JTable tableTrattamenti;
    /**
     * Create the application.
     */
    public InsertTreatmentWindow(Prodotto prod) {
        this.prodotto = prod;
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame("Insert Treatment");
        frame.setBounds(100, 100, 584, 300);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 35, 548, 216);
        frame.getContentPane().add(scrollPane);
        
        tableTrattamenti = new JTable();
        scrollPane.setViewportView(tableTrattamenti);
        List<Lavoro> totLavori = LavoroTable.getTable().findAllWithFilter("Trattamento");
        String[] nomiAttributi = LavoroTable.getTable().getAttributes();
        DefaultTableModel model = createNotEditableModel(nomiAttributi);
        for (Lavoro lav : totLavori) {
            model.addRow(lav.concatAttributes().toArray());
        }
        tableTrattamenti.setModel(model);
        
        JButton btnInserisci = new JButton("Inserisci");
        btnInserisci.setBackground(SystemColor.desktop);
        btnInserisci.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                final int[] rows = tableTrattamenti.getSelectedRows();
                if (rows.length > 0) {
                    int ris = ProdottoTable.getTable().persist(InsertTreatmentWindow.this.prodotto);
                    if (ris == 0) {
                        for (int row : rows) {
                            int selectedId = Integer.parseInt(tableTrattamenti.getValueAt(row, 0).toString());
                            Lavoro tratt = LavoroTable.getTable().findByPrimaryKey(selectedId);
                            ProdTratt pt = new ProdTratt();
                            pt.setIdLavoro(tratt.getIdLavoro());
                            pt.setIdProdotto(InsertTreatmentWindow.this.prodotto.getIdProdotto());
                            ProdTrattTable.getTable().persist(pt);
                        }
                        JOptionPane.showMessageDialog(frame, "Lavoro inserito correttamente");
                        frame.dispose();
                    } else if (ris == 1){
                        JOptionPane.showMessageDialog(frame, "Codice prodotto già esistente");
                    } else {
                        JOptionPane.showMessageDialog(frame, "Errore di inserimento nel DB");
                    }
                } else {
                    JOptionPane.showMessageDialog(frame, "Selezionare almeno una riga");
                }
            }
        });
        btnInserisci.setBounds(10, 11, 109, 23);
        frame.getContentPane().add(btnInserisci);
        frame.setVisible(true);
    }

    private DefaultTableModel createNotEditableModel(String[] nomiAttributi) {
        return new DefaultTableModel(nomiAttributi, 0) {
            private static final long serialVersionUID = -9140705710062485906L;
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
    }

}
