package view.admin;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import javafx.util.Pair;
import model.Mezzo;
import model.database.ClienteTable;
import model.database.FatturaTable;
import model.database.MezzoTable;
import model.database.RevisioneTable;

import javax.swing.JButton;
import java.awt.GridBagLayout;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

/**
 * DB administrator check GUI.
 */
//CHECKSTYLE:OFF
@SuppressWarnings("all")
public class ChecksWindow {
    private Mezzo selectedVehicle;
    private boolean flagFattura;
    /**
     * Create the application.
     */
    public ChecksWindow() {
        this.selectedVehicle = null;
        this.flagFattura = false;
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        JFrame frame;
        frame = new JFrame("Checks Window");
        frame.setBounds(100, 100, 690, 355);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        frame.setVisible(true);

        JTable table;    
        JTextField textMezzo;
        JComboBox comboBoxTipo = new JComboBox();

        final JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(236, 11, 410, 295);
        frame.getContentPane().add(scrollPane);

        table = new JTable();
        table.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                if(ChecksWindow.this.flagFattura){
                    final JTable jTable= (JTable)e.getSource();
                    final int row = jTable.getSelectedRow();
                    ChecksWindow.this.selectedVehicle = MezzoTable.getTable().findByPrimaryKey(jTable.getValueAt(row, 0).toString());
                    if(e.getClickCount()==2){
                        EventQueue.invokeLater(() -> new InsertInvoiceWindow(ChecksWindow.this.selectedVehicle)); 
                    }
                }
            }
        });
        scrollPane.setViewportView(table);

        final JPanel panel = new JPanel();
        panel.setBounds(10, 11, 216, 148);
        frame.getContentPane().add(panel);
        GridBagLayout gbl_panel = new GridBagLayout();
        gbl_panel.columnWidths = new int[]{0, 0, 0};
        gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
        gbl_panel.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
        gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        panel.setLayout(gbl_panel);

        final JLabel lblControlloRevisione = new JLabel("Controllo revisione");
        GridBagConstraints gbc_lblControlloRevisione = new GridBagConstraints();
        gbc_lblControlloRevisione.insets = new Insets(0, 0, 5, 0);
        gbc_lblControlloRevisione.gridx = 1;
        gbc_lblControlloRevisione.gridy = 0;
        panel.add(lblControlloRevisione, gbc_lblControlloRevisione);

        final JLabel lblMezzo = new JLabel("Mezzo");
        GridBagConstraints gbc_lblMezzo = new GridBagConstraints();
        gbc_lblMezzo.insets = new Insets(0, 0, 5, 5);
        gbc_lblMezzo.gridx = 0;
        gbc_lblMezzo.gridy = 1;
        panel.add(lblMezzo, gbc_lblMezzo);

        textMezzo = new JTextField();
        GridBagConstraints gbc_textMezzo = new GridBagConstraints();
        gbc_textMezzo.insets = new Insets(0, 0, 5, 0);
        gbc_textMezzo.gridx = 1;
        gbc_textMezzo.gridy = 1;
        panel.add(textMezzo, gbc_textMezzo);
        textMezzo.setColumns(10);
        
        final JButton btnEseguiContrRev = new JButton("Esegui");
        btnEseguiContrRev.setBackground(SystemColor.desktop);
        btnEseguiContrRev.addActionListener(al -> {
            this.flagFattura = false;
            Pair<List<String>,Vector<Vector<String>>> res;
            DefaultTableModel model = null;
            if(textMezzo.getText().length() > 0 ){
                res = RevisioneTable.getTable().findRevisionByVeihicleName(textMezzo.getText());
                model = createNotEditableModel(res.getKey().toArray());
                for (Vector<String> vec : res.getValue()) {
                    model.addRow(vec);
                }
            } else {
                res = RevisioneTable.getTable().findRevisionByVeihicleType(comboBoxTipo.getSelectedItem().toString());
                model = createNotEditableModel(res.getKey().toArray());
                for (Vector<String> vec : res.getValue()) {
                    model.addRow(vec);
                }
            }
            table.setModel(model);
        });
        
        JLabel lblTipologia = new JLabel("Tipologia");
        GridBagConstraints gbc_lblTipologia = new GridBagConstraints();
        gbc_lblTipologia.insets = new Insets(0, 0, 5, 5);
        gbc_lblTipologia.gridx = 0;
        gbc_lblTipologia.gridy = 2;
        panel.add(lblTipologia, gbc_lblTipologia);
        
        GridBagConstraints gbc_comboBoxTipo = new GridBagConstraints();
        gbc_comboBoxTipo.insets = new Insets(0, 0, 5, 0);
        gbc_comboBoxTipo.gridx = 1;
        gbc_comboBoxTipo.gridy = 2;
        panel.add(comboBoxTipo, gbc_comboBoxTipo);
        comboBoxTipo.setModel(new DefaultComboBoxModel(new String[] {"Trattore", "Da Raccolta"}));
        GridBagConstraints gbc_btnEseguiContrRev = new GridBagConstraints();
        gbc_btnEseguiContrRev.insets = new Insets(0, 0, 5, 0);
        gbc_btnEseguiContrRev.gridx = 1;
        gbc_btnEseguiContrRev.gridy = 3;
        panel.add(btnEseguiContrRev, gbc_btnEseguiContrRev);
        
        JButton btnVisualizzaMezzi = new JButton("Visualizza TOT mezzi");
        btnVisualizzaMezzi.setBackground(SystemColor.desktop);
        btnVisualizzaMezzi.addActionListener(al -> {
            this.flagFattura = true;
            List<Mezzo> totMezzi = MezzoTable.getTable().findAll();
            String[] nomiAttributi = MezzoTable.getTable().getAttributes();
            DefaultTableModel model = createNotEditableModel(nomiAttributi);
            for (Mezzo me : totMezzi) {
                model.addRow(me.concatAttributes().toArray());
            }
            table.setModel(model);
        });
        GridBagConstraints gbc_btnVisualizzaMezzi = new GridBagConstraints();
        gbc_btnVisualizzaMezzi.gridx = 1;
        gbc_btnVisualizzaMezzi.gridy = 4;
        panel.add(btnVisualizzaMezzi, gbc_btnVisualizzaMezzi);

        final JButton btnFatturatoAnnuo = new JButton("Fatturato Annuo");
        btnFatturatoAnnuo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                flagFattura = false;
                Pair<List<String>,Vector<Vector<String>>> fatt = FatturaTable.getTable().getAnnualRevenue();
                DefaultTableModel model = createNotEditableModel(fatt.getKey().toArray());
                for (Vector<String> vec : fatt.getValue()) {
                    model.addRow(vec);
                }
                table.setModel(model);
            }
        });
        btnFatturatoAnnuo.setBackground(SystemColor.activeCaption);
        btnFatturatoAnnuo.setBounds(10, 181, 155, 40);
        frame.getContentPane().add(btnFatturatoAnnuo);

        final JButton btnSpesaMedia = new JButton("Speda Media Annua");
        btnSpesaMedia.addActionListener(al -> {
            this.flagFattura = false;
            Pair<List<String>,Vector<Vector<String>>> speseAnnue = ClienteTable.getTable().getAnnualExpenses();
            DefaultTableModel model = createNotEditableModel(speseAnnue.getKey().toArray());
            for (Vector<String> vec : speseAnnue.getValue()) {
                model.addRow(vec);
            }
            table.setModel(model);
        });
        btnSpesaMedia.setBackground(SystemColor.activeCaption);
        btnSpesaMedia.setBounds(10, 232, 155, 40);
        frame.getContentPane().add(btnSpesaMedia);
    }
    
    private DefaultTableModel createNotEditableModel(Object[] nomiAttributi) {
        return new DefaultTableModel(nomiAttributi, 0) {
            private static final long serialVersionUID = -9140705710062485906L;
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
    }
}
