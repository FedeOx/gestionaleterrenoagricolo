package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import model.database.ProdottoSeminatoTable;

/**
 * Classe che rappresenta l'entità PRODOTTO_SEMINATO.
 */
public class ProdottoSeminato {
    private int idProdSem;
    private int quantitaPiantata;
    private Date dataSemina;
    private int idDettaglioLavoro;
    private Optional<Integer> raccoltoEffettivo;
    private int idProdotto;
    private String telaioMezzo;

    /**
     * @return l'idProdSem.
     */
    public int getIdProdSem() {
        return this.idProdSem;
    }
    /**
     * @param idProdSem l'idProdSem da settare.
     */
    public void setIdProdSem(final int idProdSem) {
        this.idProdSem = idProdSem;
    }
    /**
     * @return la quantitaPiantata.
     */
    public int getQuantitaPiantata() {
        return this.quantitaPiantata;
    }
    /**
     * @param quantitaPiantata la quantitaPiantata da settare.
     */
    public void setQuantitaPiantata(final int quantitaPiantata) {
        this.quantitaPiantata = quantitaPiantata;
    }
    /**
     * @return la dataSemina.
     */
    public Date getDataSemina() {
        return this.dataSemina;
    }
    /**
     * @param dataSemina la dataSemina da settare.
     */
    public void setDataSemina(final Date dataSemina) {
        this.dataSemina = dataSemina;
    }
    /**
     * @return l'idDettaglioLavoro.
     */
    public int getIdDettaglioLavoro() {
        return this.idDettaglioLavoro;
    }
    /**
     * @param idDettaglioLavoro l'idDettaglioLavoro da settare.
     */
    public void setIdDettaglioLavoro(final int idDettaglioLavoro) {
        this.idDettaglioLavoro = idDettaglioLavoro;
    }
    /**
     * @return il raccoltoEffettivo.
     */
    public Optional<Integer> getRaccoltoEffettivo() {
        return raccoltoEffettivo;
    }
    /**
     * @param raccoltoEffettivo il raccoltoEffettivo da settare.
     */
    public void setRaccoltoEffettivo(final Optional<Integer> raccoltoEffettivo) {
        this.raccoltoEffettivo = raccoltoEffettivo;
    }
    /**
     * @return il idProdotto.
     */
    public int getIdProdotto() {
        return idProdotto;
    }
    /**
     * @param idProdotto il idProdotto da settare.
     */
    public void setIdProdotto(final int idProdotto) {
        this.idProdotto = idProdotto;
    }
    /**
     * @return il telaioMezzo.
     */
    public String getTelaioMezzo() {
        return telaioMezzo;
    }
    /**
     * @param telaioMezzo il telaioMezzo da settare.
     */
    public void setTelaioMezzo(final String telaioMezzo) {
        this.telaioMezzo = telaioMezzo;
    }
    /**
     * Restituisce la lista degli attributi associati a questo oggetto.
     */
    public List<?> concatAttributes() {
        return new ArrayList<>(Arrays.asList(this.idProdSem, this.quantitaPiantata, this.dataSemina, this.idDettaglioLavoro,
                this.raccoltoEffettivo.orElse(0), this.idProdotto, this.telaioMezzo));
    }
    /**
     * Restituisce la lista degli attributi custom associati a questo oggetto per la visualizzazione del risultato del metotod {@link ProdottoSeminatoTable#findAllWithFilter()}.
     */
    public List<?> concatAttributesCustom() {
        return new ArrayList<>(Arrays.asList(this.idProdSem, this.idProdotto, this.dataSemina, this.quantitaPiantata));
    }
}
