package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import model.Guida;

/**
 * Classe che rappresenta la tabellla Guide nel DB.
 */
public final class GuidaTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyGuidaTable {
        private static final GuidaTable SINGLETON = new GuidaTable(); 
    }

    private GuidaTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Guide";
    }

    public static GuidaTable getTable() {
        return LazyGuidaTable.SINGLETON;
    }

    /**
     * Memorizza nel DB una guida.
     * @param guida la guida da memorizzare.
     * @return il risultato dell'operazione eseguita.
     */
    public String persist(final Guida guida) {
        if (findByPrimaryKey(guida.getTelaioMezzo(), guida.getData(), guida.getIdDipendente()) != null) {
            System.out.println("Errore:" + "Drive already exists");
            return "Guida già esistente";
        }
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "Insert into " + this.tableName + " (TelaioMezzo,Data,IdDipendente)"
                + " values(?,?,?)";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setString(1, guida.getTelaioMezzo());
            pStatement.setTimestamp(2, new java.sql.Timestamp(guida.getData().getTime()));
            pStatement.setInt(3, guida.getIdDipendente());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Guida inserito correttamente";
    }

    /**
     * Ricerca per chiave primaria.
     * @return la guida cercata se esiste, null altrimenti.
     */
    public Guida findByPrimaryKey(final String telaioMezzo, final Date data, final int idDipendente) {
        Guida guida = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select * from " + this.tableName + " where TelaioMezzo=? and Data=? and IdDipendente=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setString(1, telaioMezzo);
            pStatement.setTimestamp(2, new java.sql.Timestamp(data.getTime()));
            pStatement.setInt(3, idDipendente);
            result = pStatement.executeQuery();
            if (result.next()) {
                guida = new Guida();
                guida.setTelaioMezzo(result.getString("TelaioMezzo"));
                guida.setData(new Date(result.getDate("Data").getTime()));
                guida.setIdDipendente(result.getInt("IdDipendente"));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return guida;
    }
}
