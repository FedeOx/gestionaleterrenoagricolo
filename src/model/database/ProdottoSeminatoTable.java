package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Vector;

import javafx.util.Pair;
import model.ProdottoSeminato;

/**
 * Classe che rappresenta la tabellla Prodotti_Seminati nel db.
 */
public final class ProdottoSeminatoTable { 
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyProdottoTable {
        private static final ProdottoSeminatoTable SINGLETON = new ProdottoSeminatoTable();
    }

    private ProdottoSeminatoTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Prodotti_Seminati";
    }

    public static ProdottoSeminatoTable getTable() {
        return LazyProdottoTable.SINGLETON;
    }

    /**
     * Memorizza un prodotto seminato nel DB.
     * @param prodSem il prodotto da memorizzare.
     * @return il risultato dell'operazione compiuta.
     */
    public String persist(final ProdottoSeminato prodSem) {
        if (findByPrimaryKey(prodSem.getIdProdSem()) != null) {
            System.out.println("Errore:" + "Product already exists");
            return "Codice prodotto già esistente";
        }
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "Insert into " + this.tableName + " (IdProdSem,QuantitaPiantata,DataSemina,RaccoltoEffettivo,IdDettaglioLavoro,IdProdotto,TelaioMezzo)"
                + " values(?,?,?,?,?,?,?)";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, prodSem.getIdProdSem());
            pStatement.setInt(2, prodSem.getQuantitaPiantata());
            pStatement.setTimestamp(3, new java.sql.Timestamp(prodSem.getDataSemina().getTime()));
            pStatement.setInt(4, prodSem.getRaccoltoEffettivo().orElse(0));
            pStatement.setInt(5, prodSem.getIdDettaglioLavoro());
            pStatement.setInt(6, prodSem.getIdProdotto());
            pStatement.setString(7, prodSem.getTelaioMezzo());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Prodotto inserito correttamente";
    }

    /**
     * Ricerca prodotto seminato per chiave primaria.
     * @param idProdSem chiave primaria del prodotto.
     * @return il prodotto seminato cercato, null se non esiste.
     */
    public ProdottoSeminato findByPrimaryKey(final int idProdSem) {
        ProdottoSeminato prod = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select * from " + this.tableName + " where IdProdSem=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, idProdSem);
            result = pStatement.executeQuery();
            if (result.next()) {
                prod = new ProdottoSeminato();
                prod.setIdProdSem(result.getInt("IdProdSem"));
                prod.setQuantitaPiantata(result.getInt("QuantitaPiantata"));
                prod.setDataSemina(new java.util.Date(result.getDate("DataSemina").getTime()));
                prod.setRaccoltoEffettivo(Optional.ofNullable(result.getInt("RaccoltoEffettivo")));
                prod.setIdDettaglioLavoro(result.getInt("IdDettaglioLavoro"));
                prod.setIdProdotto(result.getInt("IdProdotto"));
                prod.setTelaioMezzo(result.getString("TelaioMezzo"));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return prod;
    }

    /**
     * @return il massimo tra gli id di prodotti seminati memorizzati.
     */
    public int getMaxId() {
        int id = 0;
        final Connection connection = new DBConnection().getSqlServerConnection();
        Statement statement = null;
        ResultSet result = null;
        final String query = "select max(IdProdSem) as MaxId from " + this.tableName;
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(query);
            while (result.next()) {
                id = result.getInt("MaxId"); //Calcolo automatico dell'IdProdSem, si prende il max nel DB e lo si incrementa successivamente di 1.
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return id;
    }

    /**
     * Restituisce la lista di tutti i prodotti seminati registrati nel DB CHE NON SONO STATI RACCOLTI.
     * @return la lista dei prodotti seminati.
     */
    public List<ProdottoSeminato> findAll() {
        List<ProdottoSeminato>  prodotti = null;
        ProdottoSeminato prod = null;
        PreparedStatement pStatement = null;
        String query;
        query = "select * from " + this.tableName + " where RaccoltoEffettivo=0"; //Solo quelli che non sono stati ancora raccolti.
        ResultSet result = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        try {
            pStatement = connection.prepareStatement(query);
            result = pStatement.executeQuery();
            prodotti = new LinkedList<>();
            while (result.next()) {
                prod = new ProdottoSeminato();
                prod.setIdProdSem(result.getInt("IdProdSem"));
                prod.setQuantitaPiantata(result.getInt("QuantitaPiantata"));
                prod.setDataSemina(new java.util.Date(result.getDate("DataSemina").getTime()));
                prod.setRaccoltoEffettivo(Optional.ofNullable(result.getInt("RaccoltoEffettivo")));
                prod.setIdDettaglioLavoro(result.getInt("IdDettaglioLavoro"));
                prod.setIdProdotto(result.getInt("IdProdotto"));
                prod.setTelaioMezzo(result.getString("TelaioMezzo"));
                prodotti.add(prod);
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return prodotti;
    }

    /**
     * Restituisce la lista di tutti i prodotti seminati su cui è possibile applicare il trattamento fornito in input e che non sono stati già raccolti.
     * @param idLavoro il trattamento per cui fare la ricerca.
     * @return la lista dei prodotti seminati su cui è possibile applicare il trattamento fornito in input.
     */
    public Pair<List<String>, Vector<Vector<String>>> findAllWithFilter(final int idLavoro) {
        PreparedStatement pStatement = null;
        Vector<Vector<String>> finalVec = null;
        List<String> columnsNames = null;
        String query;
        query = "select IdProdSem, DataSemina, Nome"
                + " from Prod_Tratt PT join Catalogo_Prodotti CP on PT.IdProdotto=CP.IdProdotto join " + this.tableName + " PS on CP.IdProdotto = PS.IdProdotto"
                + " where IdLavoro = ? and RaccoltoEffettivo=0";
        ResultSet result = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, idLavoro);
            result = pStatement.executeQuery();
            final ResultSetMetaData rsmd = result.getMetaData();
            columnsNames = new ArrayList<>();
            for (int i = 1; i <= rsmd.getColumnCount(); ++i) {
                columnsNames.add(rsmd.getColumnName(i));
            }
            finalVec = new Vector<>();
            while (result.next()) {
                final Vector<String> vec = new Vector<>();
                vec.add(Objects.toString(result.getInt("IdProdSem")));
                vec.add(new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date(result.getDate("DataSemina").getTime())));
                vec.add(result.getString("Nome"));
                finalVec.add(vec);
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return new Pair<>(columnsNames, finalVec);
    }

    /**
     * Modifica un elemento nel DB.
     * @param prodSem il prodotto seminato da modificare.
     * @return il risultato dell'operazione compiuta.
     */
    public String update(final ProdottoSeminato prodSem) {
        final ProdottoSeminato oldProdotto = findByPrimaryKey(prodSem.getIdProdSem());
        if (oldProdotto == null) {
            return "Il prodotto inserito non esiste";
        }
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "update " + this.tableName + " set QuantitaPiantata=?,DataSemina=?,RaccoltoEffettivo=?,IdDettaglioLavoro=?,IdProdotto=?,TelaioMezzo=? where IdProdSem=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, prodSem.getQuantitaPiantata());
            pStatement.setTimestamp(2, new java.sql.Timestamp(prodSem.getDataSemina().getTime()));
            pStatement.setInt(3, prodSem.getRaccoltoEffettivo().orElse(0));
            pStatement.setInt(4, prodSem.getIdDettaglioLavoro());
            pStatement.setInt(5, prodSem.getIdProdotto());
            pStatement.setString(6, prodSem.getTelaioMezzo());
            pStatement.setInt(7, prodSem.getIdProdSem());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore" + e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) { 
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                    System.out.println("Errore" + e.getMessage());
            }
        }
        return "Prodotto modificato correttamente";
    }

    /**
     * Restituisce tutti i trattamento mancanti su una certo prodotto seminato.
     * @return l'intestazione della tabella JTable e i dati.
     */
    public Pair<List<String>, Vector<Vector<String>>> missingTreatments(final int idProdSem) {
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement statement = null;
        Vector<Vector<String>> finalVec = null;
        List<String> columnsNames = null;
        ResultSet result = null;
        final String query = "select PT.IdLavoro, L.Nome"
        + " from " + this.tableName + " PS join Catalogo_Prodotti CP on PS.IdProdotto=CP.IdProdotto join Prod_Tratt PT on CP.IdProdotto=PT.IdProdotto join Lavori L on PT.IdLavoro = L.IdLavoro"
        + " where PS.RaccoltoEffettivo = 0 and PS.IdProdSem = ?" //Selezione di tutti i trattamenti previsti per i prodotti seminati non raccolti.
        + " except"
        + " select DL.IdLavoro, L.Nome" 
        + " from " + this.tableName + " PS join Esec_Trattamenti ET on PS.IdProdSem=ET.IdProdSem join Dettagli_Lavori DL on ET.IdDettaglioLavoro=DL.IdDettaglioLavoro join Lavori L on DL.IdLavoro = L.IdLavoro"
        + " where PS.RaccoltoEffettivo = 0 and PS.IdProdSem = ?"; //Selezione di tutti i trattamenti effettivamente eseguiti sui seminati non raccolti.

        try {
            statement = connection.prepareStatement(query);
            statement.setInt(1, idProdSem);
            statement.setInt(2, idProdSem);
            result = statement.executeQuery();
            final ResultSetMetaData rsmd = result.getMetaData();
            columnsNames = new ArrayList<>(); 
            for (int i = 1; i <= rsmd.getColumnCount(); ++i) {
                columnsNames.add(rsmd.getColumnName(i));
            }
            finalVec = new Vector<>();
            while (result.next()) {
                final Vector<String> vec = new Vector<>();
                vec.add(result.getString("IdLavoro"));
                vec.add(result.getString("Nome"));
                finalVec.add(vec);
            }
        } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore" + e.getMessage());
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                    if (result != null) {
                        result.close();
                    }
                } catch (SQLException e) {
                    new Exception(e.getMessage());
                         System.out.println("Errore" + e.getMessage());
                }
            }
            return new Pair<>(columnsNames, finalVec);
    }

    /**
     * Restituisce tutti i prodotti seminati che sono pronti da raccogliere.
     * @return l'intestazione della tabella JTable e i dati.
     */
    public Pair<List<String>, Vector<Vector<String>>> readyToCollect() {
        final Connection connection = this.dataSource.getSqlServerConnection();
        Statement statement = null;
        Vector<Vector<String>> finalVec = null;
        List<String> columnsNames = null;
        ResultSet result = null;
        //Selezione di tutti i prodotti non già raccolti che sono pronti da raccoglere.
        final String query = "select PS.IdProdSem, CP.Nome, PS.QuantitaPiantata, PS.DataSemina"
        + " from " + this.tableName + " PS join Catalogo_Prodotti CP on PS.IdProdotto = CP.IdProdotto"
        + " where PS.RaccoltoEffettivo = 0 and CP.TempoMaturazione <= DATEDIFF(d,PS.DataSemina, GETDATE())";
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(query);
            final ResultSetMetaData rsmd = result.getMetaData();
            columnsNames = new ArrayList<>(); 
            for (int i = 1; i <= rsmd.getColumnCount(); ++i) {
                columnsNames.add(rsmd.getColumnName(i));
            }
            finalVec = new Vector<>();
            while (result.next()) {
                final Vector<String> vec = new Vector<>();
                vec.add(result.getString("IdProdSem"));
                vec.add(result.getString("Nome"));
                vec.add(result.getString("QuantitaPiantata"));
                vec.add(result.getString("DataSemina"));
                finalVec.add(vec);
            }
        } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore" + e.getMessage());
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                    if (result != null) {
                        result.close();
                    }
                } catch (SQLException e) {
                    new Exception(e.getMessage());
                         System.out.println("Errore" + e.getMessage());
                }
            }
            return new Pair<>(columnsNames, finalVec);
    }

    /**
     * @return i nomi degli attributi della tabella.
     */
    public String[] getAttributes() {
        return new String[] {"IdProdSem", "QuantitaPiantata", "DataSemina", "RaccoltoEffettivo", "IdDettaglioLavoro", "IdProdotto", "TelaioMezzo"};
    }

    /**
     * @return i nomi degli attributi della tabella customizzati per una specifica vista.
     */
    public String[] getAttributesCustom() {
        return new String[] {"IdProdSem", "IdProdotto", "DataSemina", "QuantitaPiantata"};
    }
}
