package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.Acquisto;

/**
 * Classe che rappresenta la tabellla Acquisti nel db.
 */
public final class AcquistoTable {
    private final DBConnection dataSource;
    private final String tableName;

    private static class LazyAcquistoTable {
        private static final AcquistoTable SINGLETON = new AcquistoTable();
    }

    private AcquistoTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Acquisti";
    }

    public static AcquistoTable getTable() {
        return LazyAcquistoTable.SINGLETON;
    }

    /**
     * Registra un acquisto nel DB.
     * @param acquisto l'acquisto da registrate.
     * @return il risultato dell'operazione compiuta.
     */
    public String persist(final Acquisto acquisto) {
        if (findByPrimaryKey(acquisto.getIdAcquisto()) != null) {
            System.out.println("Errore:" + "Purchase already exists");
            return "Codice acquisto già esistente";
        }
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "Insert into " + this.tableName + " (IdAcquisto,Data,PrezzoTotale,IdDipendente,IdCliente)"
                + " values(?,?,?,?,?)";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, acquisto.getIdAcquisto());
            pStatement.setDate(2, new java.sql.Date(acquisto.getData().getTime()));
            pStatement.setDouble(3, acquisto.getPrezzoTotale());
            pStatement.setInt(4, acquisto.getIdDipendente());
            pStatement.setInt(5, acquisto.getIdCliente());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Acquisto inserito correttamente";
    }

    /**
     * Ricerca per chiave primaria.
     * @param idAcquisto l'id da ricercare.
     * @return l'acquisto cercato se esiste, null altrimenti.
     */
    public Acquisto findByPrimaryKey(final int idAcquisto) {
        Acquisto acq = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select * from " + this.tableName + " where IdAcquisto=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, idAcquisto);
            result = pStatement.executeQuery();
            if (result.next()) {
                acq = new Acquisto();
                acq.setIdAcquisto(result.getInt("IdAcquisto"));
                acq.setData(new java.util.Date(result.getDate("Data").getTime()));
                acq.setPrezzoTotale(result.getDouble("PrezzoTotale"));
                acq.setIdDipendente(result.getInt("IdDipendente"));
                acq.setIdCliente(result.getInt("IdCliente"));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return acq;
    }

    /**
     * @return il massimo valore di IdAcquisto presente nel DB.
     */
    public int getMaxId() {
        int idAcquisto = 0;
        final Connection connection = new DBConnection().getSqlServerConnection();
        Statement statement = null;
        ResultSet result = null;
        final String query = "select max(IdAcquisto) as MaxId from " + this.tableName;
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(query);
            while (result.next()) {
                idAcquisto = result.getInt("MaxId"); //Calcolo automatico dell'IdAcquisto, si prende il max nel DB e lo si incrementa successivamente di 1.
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return idAcquisto;
    }
}
