package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.ProdTratt;

/**
 * Classe che rappresenta la tabella Prod_Tratt nel db.
 */
public final class ProdTrattTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyProdTrattTable {
        private static final ProdTrattTable SINGLETON = new ProdTrattTable();
    }

    private ProdTrattTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Prod_Tratt";
    }

    public static ProdTrattTable getTable() {
        return LazyProdTrattTable.SINGLETON;
    }

    /**
     * Memorizza un collegamento prodotto-trattamento nel DB.
     * @param pt il collegamento da memorizzare.
     * @return il risultato dell'operazione compiuta.
     */
    public String persist(final ProdTratt pt) {
        if (findByPrimaryKey(pt.getIdLavoro(), pt.getIdProdotto()) != null) {
            System.out.println("Errore:" + "Prod_Tratt already exists");
            return "Collegamento Prodotto-Trattamento già esistente";
        }
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "Insert into " + this.tableName + " (IdLavoro,IdProdotto)"
                + " values(?,?)";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, pt.getIdLavoro());
            pStatement.setInt(2, pt.getIdProdotto());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Collegamento Prodotto-Trattamento inserito correttamente";
    }

    /**
     * Ricerca collegamtno prodotto-trattamento per chiave primaria (idLavoro, idProdotto).
     * @return il collegamento cercato, null se non esiste.
     */
    public ProdTratt findByPrimaryKey(final int idLavoro, final int idProdotto) {
        ProdTratt pt = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select * from " + this.tableName + " where IdLavoro=? and IdProdotto=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, idLavoro);
            pStatement.setInt(2, idProdotto);
            result = pStatement.executeQuery();
            if (result.next()) {
                pt = new ProdTratt();
                pt.setIdProdotto(result.getInt("IdProdotto"));
                pt.setIdLavoro(result.getInt("IdLavoro"));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return pt;
    }
}
