package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Vector;
import javafx.util.Pair;
import model.Revisione;

/**
 * Classe che rappresenta la tabellla Revisioni nel db.
 */
public final class RevisioneTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyRevisioneTable {
        private static final RevisioneTable SINGLETON = new RevisioneTable();
    }

    private RevisioneTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Revisioni";
    }

    public static RevisioneTable getTable() {
        return LazyRevisioneTable.SINGLETON;
    }

    /**
     * Memorizza nel DB una nuova revisione.
     * @param rev la revisione da memorizzare.
     * @return il risultato dell'operazione compiuta.
     */
    public String persist(final Revisione rev) {
        if (findByPrimaryKey(rev.getTelaioMezzo(), rev.getData()) != null) {
            System.out.println("Errore:" + "Revision already exists");
            return "Revisione già esistente";
        }
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "Insert into " + this.tableName + " (TelaioMezzo,Data,Costo,Descrizione)"
                + " values(?,?,?,?)";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setString(1, rev.getTelaioMezzo());
            pStatement.setDate(2, new java.sql.Date(rev.getData().getTime()));
            pStatement.setDouble(3, rev.getCosto());
            pStatement.setString(4, rev.getDescrizione().orElse(null));
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Revisione inserita correttamente";
    }

    /**
     * Restituisce la reviosione cercata.
     * @param telaioMezzo della revisione da cercare.
     * @param data della revisione da cercare.
     * @return la revisione cercata se esiste, null altrimenti.
     */
    public Revisione findByPrimaryKey(final String telaioMezzo, final Date data) {
        Revisione rev = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select * from " + this.tableName + " where TelaioMezzo=? and Data=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setString(1, telaioMezzo);
            pStatement.setDate(2, new java.sql.Date(data.getTime()));
            result = pStatement.executeQuery();
            if (result.next()) {
                rev = new Revisione();
                rev.setTelaioMezzo(result.getString("TelaioMezzo"));
                rev.setData(new Date(result.getDate("Data").getTime()));
                rev.setCosto(result.getDouble("Costo"));
                rev.setDescrizione(Optional.ofNullable(result.getString("Descrizione")));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return rev;
    }

    /**
     * Ricerca tramite nome del veicolo.
     * @param nome il nome da cercare.
     * @return una coppia di valori usati per costruire la Jtable, consiste di intestazione e dati.
     */
    public Pair<List<String>, Vector<Vector<String>>> findRevisionByVeihicleName(final String nome) {
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement statement = null;
        Vector<Vector<String>> finalVec = null;
        List<String> columnsNames = null;
        ResultSet result = null;
        final String query = "select M.Nome,R.TelaioMezzo,R.Data as DataRevisione,R.Descrizione from " + tableName + " R join Mezzi M on R.TelaioMezzo=M.Telaio where M.Nome like ?";
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, "%" + nome + "%");
            result = statement.executeQuery();
            final ResultSetMetaData rsmd = result.getMetaData();
            columnsNames = new ArrayList<>(); 
            for (int i = 1; i <= rsmd.getColumnCount(); ++i) {
                columnsNames.add(rsmd.getColumnName(i));
            }
            finalVec = new Vector<>();
            while (result.next()) {
                final Vector<String> vec = new Vector<>();
                vec.add(result.getString("Nome"));
                vec.add(result.getString("TelaioMezzo"));
                vec.add(result.getString("DataRevisione"));
                vec.add(result.getString("Descrizione"));
                finalVec.add(vec);
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore" + e.getMessage());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                     System.out.println("Errore" + e.getMessage());
            }
        }
        return new Pair<>(columnsNames, finalVec);
    }
    /**
     * Ricerca tramite tipo veicolo.
     * @param tipologia la tipologia di veiocolo da cercare.
     * @return una coppia di valori usati per costruire la Jtable, consiste di intestazione e dati.
     */
    public Pair<List<String>, Vector<Vector<String>>> findRevisionByVeihicleType(final String tipologia) {
        int i = 0;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement statement = null;
        Vector<Vector<String>> finalVec = null;
        List<String> columnsNames = null;
        ResultSet result = null;
        final String query = "select M.Nome,R.TelaioMezzo,R.Data as DataRevisione,R.Descrizione from " + tableName + " R join Mezzi M on R.TelaioMezzo=M.Telaio where M.Tipologia = ?";
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, tipologia);
            result = statement.executeQuery();
            final ResultSetMetaData rsmd = result.getMetaData();
            columnsNames = new ArrayList<>(); 
            for (i = 1; i <= rsmd.getColumnCount(); ++i) {
                columnsNames.add(rsmd.getColumnName(i));
            }
            finalVec = new Vector<>();
            while (result.next()) {
                final Vector<String> vec = new Vector<>();
                vec.add(result.getString("Nome"));
                vec.add(result.getString("TelaioMezzo"));
                vec.add(result.getString("DataRevisione"));
                vec.add(result.getString("Descrizione"));
                finalVec.add(vec);
            }
        } catch (SQLException e) {
                new Exception(e.getMessage());
            System.out.println("Errore" + e.getMessage());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                     System.out.println("Errore" + e.getMessage());
            }
        }
        return new Pair<>(columnsNames, finalVec);
    }
    /**
     * Ricerca tramite numero di telaio.
     * @param telaio il telaio da cercare.
     * @return  una coppia di valori usati per costruire la Jtable, consiste di intestazione e dati.
     */
    public List<Revisione> findRevisionByVeihicleFrame(final String telaio) {
        List<Revisione> revisioni = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select * from " + this.tableName + " where TelaioMezzo=?";
        try {
            revisioni = new ArrayList<>();
            pStatement = connection.prepareStatement(query);
            pStatement.setString(1, telaio);
            result = pStatement.executeQuery();
            while (result.next()) {
                final Revisione rev = new Revisione();
                rev.setTelaioMezzo(result.getString("TelaioMezzo"));
                rev.setData(new Date(result.getDate("Data").getTime()));
                rev.setCosto(result.getDouble("Costo"));
                rev.setDescrizione(Optional.ofNullable(result.getString("Descrizione")));
                revisioni.add(rev);
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return revisioni;
    }

    /**
     * @return il massimo tra gli id di revisione memorizzati.
     */
    public int getMaxId() {
        int id = 0;
        final Connection connection = new DBConnection().getSqlServerConnection();
        Statement statement = null;
        ResultSet result = null;
        final String query = "select max(Id) as MaxId from " + this.tableName;
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(query);
            while (result.next()) {
                id = result.getInt("MaxId"); //Calcolo automatico del numero di revisione, si prende il max nel DB e lo si incrementa successivamente di 1.
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return id;
    }
}
