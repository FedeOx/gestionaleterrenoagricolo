package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import javafx.util.Pair;
import model.Prodotto;

/**
 * Classe che rappresenta la tabella Prodotti nel db.
 */
public final class ProdottoTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyProdottoTable {
        private static final ProdottoTable SINGLETON = new ProdottoTable();
    }

    private ProdottoTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Catalogo_Prodotti";
    }

    public static ProdottoTable getTable() {
        return LazyProdottoTable.SINGLETON;
    }

    /**
     * Memorizza un prodotto nel DB.
     * @param prod il prodotto da memorizzare.
     * @return 1 se il codice prodotto è già esistente, -1 se c'è statp un errore di acceso al DB, 0 se l'inserimento è andato a buon fine.
     */
    public int persist(final Prodotto prod) {
        if (findByPrimaryKey(prod.getIdProdotto()) != null) {
            System.out.println("Errore:" + "Product already exists");
            return 1;
        }
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "Insert into " + this.tableName + " (IdProdotto,Nome,Prezzo,PeriodoTipico,TempoMaturazione,TerrenoIdeale,Scorte,Bio)"
                + " values(?,?,?,?,?,?,?,?)";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, prod.getIdProdotto());
            pStatement.setString(2, prod.getNome());
            pStatement.setDouble(3, prod.getPrezzo());
            pStatement.setString(4, prod.getPeriodoTipico());
            pStatement.setInt(5, prod.getTempoMaturazione());
            pStatement.setString(6, prod.getTerrenoIdeale());
            pStatement.setInt(7, prod.getScorte());
            pStatement.setBoolean(8, prod.isBio());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return -1;
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return 0;
    }

    /**
     * Ricerca prodotto per chiave primaria.
     * @param idProdotto chiave primaria del prodotto.
     * @return il prodotto cercato, null se non esiste.
     */
    public Prodotto findByPrimaryKey(final int idProdotto) {
        Prodotto prod = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select * from " + this.tableName + " where IdProdotto=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, idProdotto);
            result = pStatement.executeQuery();
            if (result.next()) {
                prod = new Prodotto();
                prod.setIdProdotto(result.getInt("IdProdotto"));
                prod.setNome(result.getString("Nome"));
                prod.setPrezzo(result.getDouble("Prezzo"));
                prod.setPeriodoTipico(result.getString("PeriodoTipico"));
                prod.setTempoMaturazione(result.getInt("TempoMaturazione"));
                prod.setTerrenoIdeale(result.getString("TerrenoIdeale"));
                prod.setScorte(result.getInt("Scorte"));
                prod.setBio(result.getBoolean("Bio"));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return prod;
    }

    /**
     * Restituisce la lista di tutti i prodotti nel DB.
     * @return la lista dei prodotti.
     */
    public List<Prodotto> findAll() {
        List<Prodotto>  prodotti = null;
        Prodotto prod = null;
        PreparedStatement pStatement = null;
        String query;
        query = "select * from " + this.tableName;

        ResultSet result = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        try {
            pStatement = connection.prepareStatement(query);
            result = pStatement.executeQuery();
            prodotti = new LinkedList<>();
            while (result.next()) {
                prod = new Prodotto();
                prod.setIdProdotto(result.getInt("IdProdotto"));
                prod.setNome(result.getString("Nome"));
                prod.setPrezzo(result.getDouble("Prezzo"));
                prod.setPeriodoTipico(result.getString("PeriodoTipico"));
                prod.setTempoMaturazione(result.getInt("TempoMaturazione"));
                prod.setTerrenoIdeale(result.getString("TerrenoIdeale"));
                prod.setScorte(result.getInt("Scorte"));
                prod.setBio(result.getBoolean("Bio"));
                prodotti.add(prod);
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return prodotti;
    }

    /**
     * Restituisce la lista di tutti i prodotti nel DB specificando per ognuno i soli attributi IdProdotto, Nome, Prezzo, Scorte, Bio.
     * @param bio flag utilizzato per filtrare i prodotti per biologici.
     * @return la lista dei prodotti.
     */
    public List<Prodotto> findAllCustom(final boolean bio) {
        List<Prodotto>  prodotti = null;
        Prodotto prod = null;
        PreparedStatement pStatement = null;
        String query;
        if (bio) {
            query = "select IdProdotto,Nome,Prezzo,Scorte,Bio from " + this.tableName + " where Bio=1";
        } else {
            query = "select IdProdotto,Nome,Prezzo,Scorte,Bio from " + this.tableName;
        }
        ResultSet result = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        try {
            pStatement = connection.prepareStatement(query);
            result = pStatement.executeQuery();
            prodotti = new LinkedList<>();
            while (result.next()) {
                prod = new Prodotto();
                prod.setIdProdotto(result.getInt("IdProdotto"));
                prod.setNome(result.getString("Nome"));
                prod.setPrezzo(result.getDouble("Prezzo"));
                prod.setScorte(result.getInt("Scorte"));
                prod.setBio(result.getBoolean("Bio"));
                prodotti.add(prod);
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return prodotti;
    }
 
    /**
     * Modifica un elemento nel DB.
     * @param prod il prodotto da modificare.
     * @return il risultato dell'operazione compiuta.
     */
    public String update(final Prodotto prod) {
        final Prodotto oldProdotto = findByPrimaryKey(prod.getIdProdotto());
        if (oldProdotto == null) {
            return "Il prodotto inserito non esiste";
        }
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "update " + this.tableName + " set Nome=?,Prezzo=?,PeriodoTipico=?,TempoMaturazione=?,TerrenoIdeale=?,Scorte=?,Bio=? where IdProdotto=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setString(1, prod.getNome());
            pStatement.setDouble(2, prod.getPrezzo());
            pStatement.setString(3, prod.getPeriodoTipico());
            pStatement.setInt(4, prod.getTempoMaturazione());
            pStatement.setString(5, prod.getTerrenoIdeale());
            pStatement.setInt(6, prod.getScorte());
            pStatement.setBoolean(7, prod.isBio());
            pStatement.setInt(8, prod.getIdProdotto());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore" + e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) { 
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                    System.out.println("Errore" + e.getMessage());
            }
        }
        return "Prodotto modificato correttamente";
    }

    /**
     * @return una lista di coppie prodotto-quantità venduta.
     */
    public List<Pair<Prodotto, Integer>> top10() {
        List<Pair<Prodotto, Integer>> res = null;
        Prodotto prod = null;
        int qnt = 0;
        final Connection connection = this.dataSource.getSqlServerConnection();
        Statement statement = null;
        ResultSet result = null;
        final  String query = "with TopVend(IdProdotto,QuantitaVenduta) as (select top(10) IdProdotto,SUM(Quantita) as QuantitaVenduta"
                + " from Dettagli_Acquisti D join Acquisti A on D.IdAcquisto=A.IdAcquisto"
                + " where YEAR(A.Data)=YEAR(getDate())"
                + " group by IdProdotto order by SUM(Quantita) DESC)"
                + " select C.IdProdotto,C.Nome,T.QuantitaVenduta"
                + " from " + this.tableName + " C join TopVend T on C.IdProdotto=T.IdProdotto";
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(query);
            res = new LinkedList<>();
            while (result.next()) {
                prod = new Prodotto();
                prod.setIdProdotto(result.getInt("IdProdotto"));
                prod.setNome(result.getString("Nome"));
                qnt = result.getInt("QuantitaVenduta");
                res.add(new Pair<>(prod, qnt));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return res;
    }

    /**
     * @return i nomi degli attributi della tabella customizzati per le viste: venditori e amministratore.
     */
    public String[] getAttributesCustom() {
        return new String[] {"IdProdotto", "Nome", "Prezzo", "Scorte", "Bio"};
    }

    /**
     * @return i nomi degli attributi dellla tabella.
     */
    public String[] getAttributes() {
        return new String[] {"IdProdotto", "Nome", "Prezzo", "PeriodoTipico", "TempoMaturazione", "TerrenoIdeale", "Scorte", "Bio"};
    }
}
