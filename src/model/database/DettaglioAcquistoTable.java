package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import model.DettaglioAcquisto;

/**
 * Classe che rappresenta la tabellla Dettagli_Acquisti nel DB.
 */
public final class DettaglioAcquistoTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyDettaglioAcquistoTable {
        private static final DettaglioAcquistoTable SINGLETON = new DettaglioAcquistoTable();
    }

    private DettaglioAcquistoTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Dettagli_Acquisti";
    }

    public static DettaglioAcquistoTable getTable() {
        return LazyDettaglioAcquistoTable.SINGLETON;
    }

    /**
     * Registra nel DB un dettaglio acquisto.
     * @param dettaglioAcquisto il dettaglio acquisto da memorizzare.
     * @return il risultato dell'operazione compiuta.
     */
    public String persist(final DettaglioAcquisto dettaglioAcquisto) {
        if (findByPrimaryKey(dettaglioAcquisto.getIdAcquisto(), dettaglioAcquisto.getIdProdotto()) != null) {
            System.out.println("Errore:" + "Purchase detail already exists");
            return "Dettaglio acquisto già esistente";
        }
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "Insert into " + this.tableName + " (IdAcquisto,IdProdotto,Quantita,PrezzoUnitario,PrezzoTotale,Sconto)"
                + " values(?,?,?,?,?,?)";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, dettaglioAcquisto.getIdAcquisto());
            pStatement.setInt(2, dettaglioAcquisto.getIdProdotto());
            pStatement.setInt(3, dettaglioAcquisto.getQuantita());
            pStatement.setDouble(4, dettaglioAcquisto.getPrezzoUnitario());
            pStatement.setDouble(5, dettaglioAcquisto.getPrezzoTotale());
            pStatement.setInt(6, dettaglioAcquisto.getSconto().orElse(null));
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Dettaglio acquisto inserito correttamente";
    }

    /**
     * Ricerca dettaglio acquisto per chiave primaria.
     * @param idAcquisto la prima componente della chiave primaria.
     * @param idProdotto la seconda componente della chiave primaria.
     * @return il dettaglio acquisto cercato se esiste, null altrimenti.
     */
    public DettaglioAcquisto findByPrimaryKey(final int idAcquisto, final int idProdotto) {
        DettaglioAcquisto acq = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select * from " + this.tableName + " where IdAcquisto=? and IdProdotto = ?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, idAcquisto);
            pStatement.setInt(2, idProdotto);
            result = pStatement.executeQuery();
            if (result.next()) {
                acq = new DettaglioAcquisto();
                acq.setIdAcquisto(result.getInt("IdAcquisto"));
                acq.setIdProdotto(result.getInt("IdProdotto"));
                acq.setQuantita(result.getInt("Quantita"));
                acq.setPrezzoUnitario(result.getInt("PrezzoUnitario"));
                acq.setPrezzoTotale(result.getInt("PrezzoTotale"));
                acq.setSconto(Optional.ofNullable(result.getInt("Sconto")));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return acq;
    }
}
