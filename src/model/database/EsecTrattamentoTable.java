package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.EsecTrattamento;
import model.Lavoro;

/**
 * Classe che rappresenta la tabella Esec_Trattamenti nel db.
 */
public final class EsecTrattamentoTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyEsecTrattamentoTable {
        private static final EsecTrattamentoTable SINGLETON = new EsecTrattamentoTable();
    }

    private EsecTrattamentoTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Esec_Trattamenti";
    }

    public static EsecTrattamentoTable getTable() {
        return LazyEsecTrattamentoTable.SINGLETON;
    }

    /**
     * Memorizza l'esecuzione trattamento specificata in input nel BD.
     * @param et l'esecuzione trattamento da memorizzare.
     * @return il risultato dell'operazione compiuta.
     */
    public String persist(final EsecTrattamento et) {
        if (findByPrimayKey(et.getIdProdSem(), et.getIdDettaglioLavoro()) != null) {
            System.out.println("Errore:" + "Exec treatment already exists");
            return "Esecuzione trattamento già esistente";
        }
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "Insert into " + this.tableName + " (IdProdSem,IdDettaglioLavoro)"
                + " values(?,?)";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, et.getIdProdSem());
            pStatement.setInt(2, et.getIdDettaglioLavoro());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Esecuzione trattamento inserita correttamente";
    }

    /**
     * Ricerca per chiave primaria un'esecuzione trattamento.
     * @param idProdSem l'id del prodotto seminato.
     * @param idDettaglioLavoro l'id del dettaglio lavoro.
     * @return l'esecuzione trattamento cercata.
     */
    public EsecTrattamento findByPrimayKey(final int idProdSem, final int idDettaglioLavoro) {
        EsecTrattamento et = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select * from " + this.tableName + " where IdProdSem=? and IdDettaglioLavoro=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, idProdSem);
            pStatement.setInt(2, idDettaglioLavoro);
            result = pStatement.executeQuery();
            if (result.next()) {
                et = new EsecTrattamento();
                et.setIdProdSem(result.getInt("IdProdSem"));
                et.setIdDettaglioLavoro(result.getInt("IdDettaglioLavoro"));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return et;
    }

    /**
     * Ricerca tutti i trattamenti già effettuati sul prodotto seminato specificato..
     * @param idProdSem l'id del prodotto seminato.
     * @param idLavoro l'id del trattamento.
     * @return la lista dei trattamenti effettuati.
     */
    public List<Lavoro> treatmentsAlreadyMade(final int idProdSem) {
        List<Lavoro> lavList = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select DL.IdLavoro from Prodotti_Seminati PS join " + this.tableName + " ET on PS.IdProdSem=ET.IdProdSem join Dettagli_Lavori DL on ET.IdDettaglioLavoro = DL.IdDettaglioLavoro"
                + " where PS.IdProdSem = ?";
        try {
            lavList = new ArrayList<>();
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, idProdSem);
            result = pStatement.executeQuery();
            while (result.next()) {
                final Lavoro lav = new Lavoro();
                lav.setIdLavoro(result.getInt("IdLavoro"));
                lavList.add(lav);
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return lavList;
    }

}
