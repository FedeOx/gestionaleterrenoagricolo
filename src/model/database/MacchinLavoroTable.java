package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.MacchinLavoro;

/**
 * Classe che rappresenta la tabellla Macchin_Lavori nel db.
 */
public final class MacchinLavoroTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyMacchinLavoriTable {
        private static final MacchinLavoroTable SINGLETON = new MacchinLavoroTable();
    }

    private MacchinLavoroTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Macchin_Lavori";
    }

    public static MacchinLavoroTable getTable() {
        return LazyMacchinLavoriTable.SINGLETON;
    }

    /**
     * Registra una relazione tra macchinario e lavoro.
     * @param ml la relazione da registrare.
     * @return il risultato dell'operazione compiuta.
     */
    public String persist(final MacchinLavoro ml) {
        if (findByPrimaryKey(ml.getIdLavoro(), ml.getIdMacchinario()) != null) {
            System.out.println("Errore:" + "Macchin_Lavoro already exists");
            return "Associazione Macchinario-Lavoro già esistente";
        }
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "Insert into " + this.tableName + " values(?,?)";

        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, ml.getIdLavoro());
            pStatement.setInt(2, ml.getIdMacchinario());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Associazione Macchinario-Lavoro inserita correttamente";
    }
 
    /**
     * Ricerca per chiave primaria.
     * @param idLavoro la prima componente della chiave.
     * @param idMacchinario la seconda componente della chiave.
     * @return la relazione macchinario-lavoro cercata se esiste, null altrimenti.
     */
    public MacchinLavoro findByPrimaryKey(final int idLavoro, final int idMacchinario) {
        MacchinLavoro ml = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select * from " + this.tableName + " where IdLavoro=? and IdMacchinario = ?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, idLavoro);
            pStatement.setInt(2, idMacchinario);
            result = pStatement.executeQuery();
            if (result.next()) {
                ml = new MacchinLavoro();
                ml.setIdLavoro(result.getInt("IdLavoro"));
                ml.setIdMacchinario(result.getInt("IdMacchinario"));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return ml;
    }
}
