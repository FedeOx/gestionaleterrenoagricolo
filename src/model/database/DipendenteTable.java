package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import model.Dipendente;

/**
 * Classe che rappresenta la tabella Dipendenti nel db.
 */
public final class DipendenteTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyDipendenteTable {
        private static final DipendenteTable SINGLETON = new DipendenteTable();
    }

    private DipendenteTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Dipendenti";
    }

    public static DipendenteTable getTable() {
        return LazyDipendenteTable.SINGLETON;
    }

    /**
     * Registra un dipendente nel DB.
     * @param dip il dipendente da memorizzare.
     * @return il risultato dell'operazione compiuta.
     */
    public String persist(final Dipendente dip) {
        if (findByPrimaryKey(dip.getIdDipendente()) != null) {
            System.out.println("Errore:" + "Employee already exists");
            return "Codice dipendente già esistente";
        }
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "Insert into " + this.tableName + " (IdDipendente,Nome,Cognome,Ind_Via,Ind_Numero,Ind_Citta,Telefono,Stipendio,Tipologia)"
                + " values(?,?,?,?,?,?,?,?,?)";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, dip.getIdDipendente());
            pStatement.setString(2, dip.getNome());
            pStatement.setString(3, dip.getCognome());
            pStatement.setString(4, dip.getVia());
            pStatement.setString(5, dip.getNumero());
            pStatement.setString(6, dip.getCitta());
            pStatement.setString(7, dip.getTelefono());
            pStatement.setDouble(8, dip.getStipendio());
            pStatement.setString(9, dip.getTipologia());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Dipendente inserito correttamente";
    }

    /**
     * Ricerca un dipendente per chiave primaria.
     * @param idDipendente la chiave da ricercare.
     * @return il dipendente cercato se esiste, null altrimenti.
     */
    public Dipendente findByPrimaryKey(final int idDipendente) {
        Dipendente dip = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select * from " + this.tableName + " where IdDipendente=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, idDipendente);
            result = pStatement.executeQuery();
            if (result.next()) {
                dip = new Dipendente();
                dip.setIdDipendente(result.getInt("IdDipendente"));
                dip.setNome(result.getString("Nome"));
                dip.setCognome(result.getString("Cognome"));
                dip.setVia(result.getString("Ind_Via"));
                dip.setNumero(result.getString("Ind_Numero"));
                dip.setCitta(result.getString("Ind_Citta"));
                dip.setTelefono(result.getString("Telefono"));
                dip.setStipendio(result.getDouble("Stipendio"));
                dip.setTipologia(result.getString("Tipologia"));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return dip;
    }

    /**
     * @return la lista di tutti i dipendenti.
     */
    public List<Dipendente> findAll() {
        List<Dipendente>  dipendenti = null;
        Dipendente dip = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        Statement statement = null;
        final String query = "select * from " + this.tableName;
        ResultSet result = null;
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(query);
            dipendenti = new LinkedList<>();
            while (result.next()) {
                dip = new Dipendente();
                dip.setIdDipendente(result.getInt("IdDipendente"));
                dip.setNome(result.getString("Nome"));
                dip.setCognome(result.getString("Cognome"));
                dip.setVia(result.getString("Ind_Via"));
                dip.setNumero(result.getString("Ind_Numero"));
                dip.setCitta(result.getString("Ind_Citta"));
                dip.setTelefono(result.getString("Telefono"));
                dip.setStipendio(result.getDouble("Stipendio"));
                dip.setTipologia(result.getString("Tipologia"));
                dipendenti.add(dip);
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return dipendenti;
    }

    /**
     * @return i nomi degli attributi della tabella.
     */
    public String[] getAttributes() {
        return new String[] {"IdDipendente", "Nome", "Cognome", "Via", "Numero", "Città", "Telefono", "Stipendio", "Tipologia"};
    }
}
