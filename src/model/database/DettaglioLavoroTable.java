package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Optional;
import model.DettaglioLavoro;

/**
 * Classe che rappresenta la tabellla Dettagli_Lavori nel DB.
 */
public final class DettaglioLavoroTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyDettaglioLavoro {
        private static final DettaglioLavoroTable SINGLETON = new DettaglioLavoroTable();
    }

    private DettaglioLavoroTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Dettagli_Lavori";
    }

    public static DettaglioLavoroTable getTable() {
        return LazyDettaglioLavoro.SINGLETON;
    }

    /**
     * Registra un dettaglio lavoro nel DB.
     * @param dettLav il dettaglio lavoro da registrare.
     * @return il risultato dell'operazione compiuta.
     */
    public String persist(final DettaglioLavoro dettLav) {
        if (findByPrimaryKey(dettLav.getIdDettaglioLavoro()) != null && findByAlternativeKey(dettLav.getDataInizio(), dettLav.getIdLavoro(), dettLav.getIdMacchinario()) != null) {
            System.out.println("Errore:" + "Work detail detail already exists");
            return "Dettaglio lavoro già esistente";
        }
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "Insert into " + this.tableName + " (IdDettaglioLavoro,DataInizio,IdLavoro,IdMacchinario,DataFine)"
                + " values(?,?,?,?,?)";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, dettLav.getIdDettaglioLavoro());
            pStatement.setTimestamp(2, new java.sql.Timestamp(dettLav.getDataInizio().getTime()));
            pStatement.setInt(3, dettLav.getIdLavoro());
            pStatement.setDouble(4, dettLav.getIdMacchinario());
            java.sql.Date dataFine;
            if (dettLav.getDataFine().isPresent()) {
                dataFine = new java.sql.Date(dettLav.getDataFine().get().getTime());
            } else {
                dataFine = null;
            }
            pStatement.setDate(5, dataFine);
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Dettaglio lavoro inserito correttamente";
    }

    /**
     * Ricerca per chiave primaria un dettaglio lavoro.
     * @param idDettaglioLavoro l'id da cercare.
     * @return il dettaglio lavoro cercato se esiste, null altrimenti.
     */
    public DettaglioLavoro findByPrimaryKey(final int idDettaglioLavoro) {
        DettaglioLavoro dett = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select * from " + this.tableName + " where IdDettaglioLavoro=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, idDettaglioLavoro);
            result = pStatement.executeQuery();
            if (result.next()) {
                dett = new DettaglioLavoro();
                dett.setIdDettaglioLavoro(result.getInt("IdDettaglioLavoro"));
                dett.setDataInizio(new Date(result.getDate("DataInizio").getTime()));
                dett.setIdLavoro(result.getInt("IdLavoro"));
                dett.setIdMacchinario(result.getInt("IdMacchinario"));
                if (result.getDate("DataFine") == null) {
                    dett.setDataFine(Optional.empty());
                } else {
                    dett.setDataFine(Optional.of(new Date(result.getDate("DataFine").getTime())));
                }
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return dett;
    }

    /**
     * Ricerca per chiave secondaria.
     * @param dataInizio il primo componente della chiave.
     * @param idLavoro il secondo componente della chiave.
     * @param idMacchinario il terzo componente della chiave.
     * @return il dettaglio lavoro cercato se esiste, null altrimenti.
     */
    public DettaglioLavoro findByAlternativeKey(final Date dataInizio, final int idLavoro, final int idMacchinario) {
        DettaglioLavoro dett = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select * from " + this.tableName + " where DataInizio=? and IdLavoro=? and IdMacchinario=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setTimestamp(1, new java.sql.Timestamp(dataInizio.getTime()));
            pStatement.setInt(2, idLavoro);
            pStatement.setInt(3, idMacchinario);
            result = pStatement.executeQuery();
            if (result.next()) {
                dett = new DettaglioLavoro();
                dett.setIdDettaglioLavoro(result.getInt("IdDettaglioLavoro"));
                dett.setDataInizio(new Date(result.getDate("DataInizio").getTime()));
                dett.setIdLavoro(result.getInt("IdLavoro"));
                dett.setIdMacchinario(result.getInt("IdMacchinario"));
                if (result.getDate("DataFine") == null) {
                    dett.setDataFine(Optional.empty());
                } else {
                    dett.setDataFine(Optional.of(new Date(result.getDate("DataFine").getTime())));
                }
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return dett;
    }

    /**
     * @return il massimo tra gli id di dettagli di lavori memorizzati.
     */
    public int getMaxId() {
        int id = 0;
        final Connection connection = new DBConnection().getSqlServerConnection();
        Statement statement = null;
        ResultSet result = null;
        final String query = "select max(IdDettaglioLavoro) as MaxId from " + this.tableName;
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(query);
            while (result.next()) {
                id = result.getInt("MaxId"); //Calcolo automatico dell'idDettaglioLavoro, si prende il max nel DB e lo si incrementa successivamente di 1.
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return id;
    }
}
