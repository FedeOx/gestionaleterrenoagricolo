package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import model.Traino;

/**
 * Classe che rappresenta la tabellla Traini nel DB.
 */
public final class TrainoTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyTrainoTable {
        private static final TrainoTable SINGLETON = new TrainoTable();
    }

    private TrainoTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Traini";
    }

    public static TrainoTable getTable() {
        return LazyTrainoTable.SINGLETON;
    }

    /**
     * Memorizza nel DB un traino.
     * @param macchinario il macchinario da memorizzare.
     * @return 1 nel caso in cui l'id del macchinario sia già esistente nel DB, -1 in caso di errore di accesso a DB, 0 in caso di inserimento corretto.
     */
    public String persist(final Traino traino) {
        if (findByPrimaryKey(traino.getIdMacchinario(), traino.getTelaioMezzo(), traino.getData()) != null) {
            System.out.println("Errore:" + "Pulling already exists");
            return "Traino già esistente";
        }
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "Insert into " + this.tableName + " (IdMacchinario,TelaioMezzo,Data)"
                + " values(?,?,?)";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, traino.getIdMacchinario());
            pStatement.setString(2, traino.getTelaioMezzo());
            pStatement.setTimestamp(3, new java.sql.Timestamp(traino.getData().getTime()));
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Traino inserito correttamente";
    }

    /**
     * Restituisce uno specifico traino.
     * @return la guida cercata, null se non esiste.
     */
    public Traino findByPrimaryKey(final int idMacchinario, final String telaioMezzo, final Date data) {
        Traino traino = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select * from " + this.tableName + " where IdMacchinario=? and TelaioMezzo=? and Data=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, idMacchinario);
            pStatement.setString(2, telaioMezzo);
            pStatement.setTimestamp(3, new java.sql.Timestamp(data.getTime()));
            result = pStatement.executeQuery();
            if (result.next()) {
                traino = new Traino();
                traino.setIdMacchinario(result.getInt("IdMacchinario"));
                traino.setTelaioMezzo(result.getString("TelaioMezzo"));
                traino.setData(new Date(result.getDate("Data").getTime()));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return traino;
    }
}
