package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Vector;
import javafx.util.Pair;
import model.Fattura;

/**
 * Classe che rappresenta la tabellla Fatture nel db.
 */
public final class FatturaTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyFatturaTable {
        private static final FatturaTable SINGLETON = new FatturaTable();
    }

    private FatturaTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Fatture";
    }

    public static FatturaTable getTable() {
        return LazyFatturaTable.SINGLETON;
    }

    /**
     * Registra una fattura nel DB.
     * @param fatt la fattura da memorizzare.
     * @return il risultato dell'operazione compiuta.
     */
    public String persist(final Fattura fatt) {
        if (findByPrimaryKey(fatt.getNumero()) != null) {
            System.out.println("Errore:" + "Invoice already exists");
            return "Numero fattura già esistente";
        }
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "Insert into " + this.tableName + " (Numero,DataEmissione,Importo,IdCliente)"
                + " values(?,?,?,?)";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, fatt.getNumero());
            pStatement.setDate(2, new java.sql.Date(fatt.getDataEmissione().getTime()));
            pStatement.setDouble(3, fatt.getImporto());
            pStatement.setInt(4, fatt.getIdCliente());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Fattura inserita correttamente";
    }

    /**
     * Ricerca per chiave primaria.
     * @param numero il numero di fattura da ricercare.
     * @return la fattura associata al numero inserito, null se non esiste.
     */
    public Fattura findByPrimaryKey(final int numero) {
        Fattura fatt = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select * from " + this.tableName + " where Numero=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, numero);
            result = pStatement.executeQuery();
            if (result.next()) {
                fatt = new Fattura();
                fatt.setNumero(result.getInt("Numero"));
                fatt.setDataEmissione(new java.util.Date(result.getDate("DataEmissione").getTime()));
                fatt.setImporto(result.getDouble("Importo"));
                fatt.setIdCliente(result.getInt("IdCliente"));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return fatt;
    }

    /**
     * @return restituisce il fatturato annuo in un formato adatto alla visualizzazione in una JTable.
     */
    public Pair<List<String>, Vector<Vector<String>>> getAnnualRevenue() {
        final Connection connection = this.dataSource.getSqlServerConnection();
        Statement statement = null;
        Vector<Vector<String>> finalVec = null;
        List<String> columnsNames = null;
        ResultSet result = null;
        final String query = "select YEAR(DataEmissione) as Anno, SUM(Importo) as Importo from " + tableName + " group by year(DataEmissione)";
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(query);
            final ResultSetMetaData rsmd = result.getMetaData();
            columnsNames = new ArrayList<>(); 
            for (int i = 1; i <= rsmd.getColumnCount(); ++i) {
                columnsNames.add(rsmd.getColumnName(i));
            }
            finalVec = new Vector<>();
            while (result.next()) {
                final Vector<String> vec = new Vector<>();
                vec.add(Objects.toString(result.getInt("Anno")));
                vec.add(Objects.toString(result.getDouble("Importo")));
                finalVec.add(vec);
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("Errore" + e.getMessage());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                     System.out.println("Errore" + e.getMessage());
            }
        }
        return new Pair<>(columnsNames, finalVec);
    }

    /**
     * @return il massimo tra i numeri di fattura memorizzati.
     */
    public int getMaxNumero() {
        int numero = 0;
        final Connection connection = new DBConnection().getSqlServerConnection();
        Statement statement = null;
        ResultSet result = null;
        final String query = "select max(Numero) as MaxNumero from " + this.tableName;
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(query);
            while (result.next()) {
                numero = result.getInt("MaxNumero"); //Calcolo automatico del numero di fattura, si prende il max nel DB e lo si incrementa successivamente di 1.
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return numero;
    }

    /**
     * @param cliente filtro opzionale per ricercare le sole fatture associate a quel cliente.
     * @return l'elenco di tutte le fatture.
     */
    public List<Fattura> findAll(final Optional<Integer> cliente) {
        List<Fattura>  fatture = null;
        Fattura fatt = null;
        Statement statement = null;
        String query;
        if (!cliente.isPresent()) {
            query = "select * from " + this.tableName;
        } else {
            query = "select * from " + this.tableName + " where IdCliente = " + cliente.get();
        }
        ResultSet result = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(query);
            fatture = new LinkedList<>();
            while (result.next()) {
                fatt = new Fattura();
                fatt = new Fattura();
                fatt.setNumero(result.getInt("Numero"));
                fatt.setDataEmissione(new java.util.Date(result.getDate("DataEmissione").getTime()));
                fatt.setImporto(result.getDouble("Importo"));
                fatt.setIdCliente(result.getInt("IdCliente"));
                fatture.add(fatt);
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return fatture;
    }

    /**
     * @return i nomi degli attributi della tabella.
     */
    public String[] getAttributes() {
        return new String[] {"Numero", "DataEmissione", "Importo", "Cliente"};
    }
}
