package model.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Classe che si occupa della creazione della connessione verso il DB.
 */
public class DBConnection {
    private static final String DBNAME = "ITTerrenoAgricolo";
    /**
     * Crea la connessione.
     * @return la connessione.
     */
    public Connection getSqlServerConnection() {
        final String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        final String dbUri = "jdbc:sqlserver://localhost:1433;databaseName=" + DBNAME;
        final String userName = "Federico";
        final String password = "Federico";

        Connection connection = null;
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(dbUri, userName, password);
        } catch (ClassNotFoundException e) {
            new Exception(e.getMessage());
            System.out.println("ErroreCNF: " + e.getMessage());
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println("ErroreSQL: " + e.getMessage());
        }
        return connection;
    }

}
