package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Vector;

import javafx.util.Pair;
import model.Cliente;

/**
 * Classe che rappresenta la tabellla Clienti nel db.
 */
public final class ClienteTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyClienteTable {
        private static final ClienteTable SINGLETON = new ClienteTable();
    }

    private ClienteTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Clienti";
    }

    public static ClienteTable getTable() {
        return LazyClienteTable.SINGLETON;
    }

    /**
     * Registra un cliente nel DB.
     * @param cliente il cliente da reistrare.
     * @return il risultato dell'operazione compiuta.
     */
    public String persist(final Cliente cliente) {
        if (findByPrimaryKey(cliente.getIdCliente()) != null) {
            System.out.println("Errore:" + "Client already exists");
            return "Codice cliente già esistente";
        }
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "Insert into " + this.tableName + " (IdCliente,Nome,Cognome,Ind_Via,Ind_Numero,Ind_Citta,CAP,Telefono)"
                + " values(?,?,?,?,?,?,?,?)";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, cliente.getIdCliente());
            pStatement.setString(2, cliente.getNome());
            pStatement.setString(3, cliente.getCognome());
            pStatement.setString(4, cliente.getVia());
            pStatement.setString(5, cliente.getNumero());
            pStatement.setString(6, cliente.getCitta());
            pStatement.setString(7, cliente.getCap());
            pStatement.setString(8, cliente.getTelefono());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Cliente inserito correttamente";
    }

    /**
     * Ricerca un cliente per chiave primaria.
     * @param idCliente l'id da ricercare.
     * @return il cliente cercato se esiste, null altrimenti.
     */
    public Cliente findByPrimaryKey(final int idCliente) {
        Cliente cliente = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select * from " + this.tableName + " where IdCliente=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, idCliente);
            result = pStatement.executeQuery();
            if (result.next()) {
                cliente = new Cliente();
                cliente.setIdCliente(result.getInt("IdCliente"));
                cliente.setNome(result.getString("Nome"));
                cliente.setCognome(result.getString("Cognome"));
                cliente.setVia(result.getString("Ind_Via"));
                cliente.setNumero(result.getString("Ind_Numero"));
                cliente.setCitta(result.getString("Ind_Citta"));
                cliente.setCap(result.getString("CAP"));
                cliente.setTelefono(result.getString("Telefono"));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return cliente;
    }

    /**
     * @return la lista di tutti i clienti registrati nel DB.
     */
    public List<Cliente> findAll() {
        List<Cliente>  clienti = null;
        Cliente cl = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "select * from " + this.tableName;
        ResultSet result = null;
        try {
            pStatement = connection.prepareStatement(query);
            result = pStatement.executeQuery();
            clienti = new LinkedList<>();
            while (result.next()) {
                cl = new Cliente();
                cl.setIdCliente(result.getInt("IdCliente"));
                cl.setNome(result.getString("Nome"));
                cl.setCognome(result.getString("Cognome"));
                cl.setVia(result.getString("Ind_Via"));
                cl.setNumero(result.getString("Ind_Numero"));
                cl.setCitta(result.getString("Ind_Citta"));
                cl.setCap(result.getString("CAP"));
                cl.setTelefono(result.getString("Telefono"));
                clienti.add(cl);
            }
        } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            } finally {
                try {
                    if (pStatement != null) {
                        pStatement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                    if (result != null) {
                        result.close();
                    }
                } catch (SQLException e) {
                    new Exception(e.getMessage());
                    System.out.println(e.getMessage());
                }
            }
            return clienti;
    }

    /**
     * @return le spese annue di ogni cliente, formattate per essere visualizzare in una JTable.
     */
    public Pair<List<String>, Vector<Vector<String>>> getAnnualExpenses() {
        final Connection connection = this.dataSource.getSqlServerConnection();
        Statement statement = null;
        Vector<Vector<String>> finalVec = null;
        List<String> columnsNames = null;
        ResultSet result = null;
        final String query = "select C.IdCliente, C.Nome, C.Cognome,  YEAR(F.DataEmissione) as Anno, SUM(F.Importo) as Spesa"
        + " from " + this.tableName + " C join Fatture F on C.IDCliente=F.IdCliente"
        + " group by C.IdCliente, C.Nome, C.Cognome,YEAR(F.DataEmissione)";
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(query);
            final ResultSetMetaData rsmd = result.getMetaData();
            columnsNames = new ArrayList<>(); 
            for (int i = 1; i <= rsmd.getColumnCount(); ++i) {
                columnsNames.add(rsmd.getColumnName(i));
            }
            finalVec = new Vector<>();
            while (result.next()) {
                final Vector<String> vec = new Vector<>();
                vec.add(result.getString("IdCliente"));
                vec.add(result.getString("Nome"));
                vec.add(result.getString("Cognome"));
                vec.add(Objects.toString(result.getInt("Anno")));
                vec.add(Objects.toString(result.getDouble("Spesa")));
                finalVec.add(vec);
            }
        } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore" + e.getMessage());
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                    if (result != null) {
                        result.close();
                    }
                } catch (SQLException e) {
                    new Exception(e.getMessage());
                         System.out.println("Errore" + e.getMessage());
                }
            }
            return new Pair<>(columnsNames, finalVec);
    }

    /**
     * @return i nomi degli attributi della tabella.
     */
    public String[] getAttributes() {
        return new String[] {"IdCliente", "Nome", "Cognome", "Via", "Numero", "Città", "CAP", "Telefono"};
    }
}
