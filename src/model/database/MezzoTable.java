package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Vector;

import javafx.util.Pair;
import model.Mezzo;

/**
 * Classe che rappresenta la tabellla Mezzi nel db.
 */
public final class MezzoTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyMezzoTable {
        private static final MezzoTable SINGLETON = new MezzoTable();
    }

    private MezzoTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Mezzi";
    }

    public static MezzoTable getTable() {
        return LazyMezzoTable.SINGLETON;
    }

    /**
     * Registra un mezzo nel DB.
     * @param mezzo il mezzo da memorizzare.
     * @return il risultato dell'operazione compiuta.
     */
    public String persist(final Mezzo mezzo) {
        if (findByPrimaryKey(mezzo.getTelaio()) != null) {
            System.out.println("Errore:" + "Vehicle already exists");
            return "Telaio mezzo già esistente";
        }
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "Insert into " + this.tableName + " (Telaio,Nome,Targa,Tipologia)"
                + " values(?,?,?,?)";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setString(1, mezzo.getTelaio());
            pStatement.setString(2, mezzo.getNome());
            pStatement.setString(3, mezzo.getTarga().orElse(null));
            pStatement.setString(4, mezzo.getTipologia());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Mezzo agricolo inserito correttamente";
    }

    /**
     * Ricerca per chiave primaria.
     * @param telaio il numero di telaio da cercare.
     * @return il mezzo cercato se esiste, null alrimenti.
     */
    public Mezzo findByPrimaryKey(final String telaio) {
        Mezzo mezzo = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select * from " + this.tableName + " where Telaio=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setString(1, telaio);
            result = pStatement.executeQuery();
            if (result.next()) {
                mezzo = new Mezzo();
                mezzo.setTelaio(result.getString("Telaio"));
                mezzo.setNome(result.getString("Nome"));
                mezzo.setTarga(Optional.ofNullable(result.getString("Targa")));
                mezzo.setTipologia(result.getString("Tipologia"));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return mezzo;
    }

    /**
     * @return la lista di tutti i mezzi.
     */
    public List<Mezzo> findAll() {
        List<Mezzo>  mezzi = null;
        Mezzo me = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "select * from " + this.tableName;
        ResultSet result = null;
        try {
            pStatement = connection.prepareStatement(query);
            result = pStatement.executeQuery();
            mezzi = new LinkedList<>();
            while (result.next()) {
                me = new Mezzo();
                me.setTelaio(result.getString("Telaio"));
                me.setNome(result.getString("Nome"));
                me.setTarga(Optional.ofNullable(result.getString("Targa")));
                me.setTipologia(result.getString("Tipologia"));
                mezzi.add(me);
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return mezzi;
    }

    /**
     * @param tipologia la tipologia per cui filtrare.
     * @return la lista dei mezzi filtrati per tipologia.
     */
    public List<Mezzo> findAllWithFilter(final String tipologia) {
        List<Mezzo>  mezzi = null;
        Mezzo me = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "select * from " + this.tableName + " where Tipologia=?";
        ResultSet result = null;
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setString(1, tipologia);
            result = pStatement.executeQuery();
            mezzi = new LinkedList<>();
            while (result.next()) {
                me = new Mezzo();
                me.setTelaio(result.getString("Telaio"));
                me.setNome(result.getString("Nome"));
                me.setTarga(Optional.ofNullable(result.getString("Targa")));
                me.setTipologia(result.getString("Tipologia"));
                mezzi.add(me);
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return mezzi;
    }

    /**
     * Restituisce tutte le guide che sono state fatte in una certa data,
     * esplicitando anche nome e cognome del conducente e nome e telaio del mezzo utilizzato.
     * @return l'intestazione della tabella JTable e i dati.
     */
    public Pair<List<String>, Vector<Vector<String>>> agriculturalMeansUsage(final Date data) {
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement statement = null;
        Vector<Vector<String>> finalVec = null;
        List<String> columnsNames = null;
        ResultSet result = null;
        final String query = "select D.Nome as NomeDipendente, D.Cognome, M.Nome as NomeMezzo, M.Telaio, convert(date, G.Data) as Data"
        + " from Dipendenti D join Guide G on D.IdDipendente=G.IdDipendente join " + this.tableName + " M on G.TelaioMezzo=M.Telaio"
        + " where convert(date, G.Data) = ?";
        try {
            statement = connection.prepareStatement(query);
            statement.setDate(1, new java.sql.Date(data.getTime()));
            result = statement.executeQuery();
            final ResultSetMetaData rsmd = result.getMetaData();
            columnsNames = new ArrayList<>(); 
            for (int i = 1; i <= rsmd.getColumnCount(); ++i) {
                columnsNames.add(rsmd.getColumnName(i));
            }
            finalVec = new Vector<>();
            while (result.next()) {
                final Vector<String> vec = new Vector<>();
                vec.add(result.getString("NomeDipendente"));
                vec.add(result.getString("Cognome"));
                vec.add(result.getString("NomeMezzo"));
                vec.add(result.getString("Telaio"));
                vec.add(new SimpleDateFormat("dd/MM/yyyy").format(new Date(result.getDate("Data").getTime())));
                finalVec.add(vec);
            }
        } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore" + e.getMessage());
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                    if (result != null) {
                        result.close();
                    }
                } catch (SQLException e) {
                    new Exception(e.getMessage());
                         System.out.println("Errore" + e.getMessage());
                }
            }
            return new Pair<>(columnsNames, finalVec);
    } 

    /**
     * @return i nomi degli attributi della tabella.
     */
    public String[] getAttributes() {
        return new String[] {"Telaio", "Nome", "Targa", "Tipologia"};
    }
}
