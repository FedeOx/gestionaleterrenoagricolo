package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import model.Lavoro;

/**
 * Classe che rappresenta la tabellla Lavori nel db.
 */
public final class LavoroTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyLavoroTable {
        private static final LavoroTable SINGLETON = new LavoroTable(); 
    }

    private LavoroTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Lavori";
    }

    public static LavoroTable getTable() {
        return LazyLavoroTable.SINGLETON;
    }

    /**
     * Memorizza nel DB un lavoro.
     * @param lavoro il lavoro da memorizzare.
     * @return 1 nel caso in cui l'id del lavoro sia già esistente nel DB, -1 in caso di errore di accesso a DB, 0 in caso di inserimento corretto.
     */
    public int persist(final Lavoro lavoro) {
        if (findByPrimaryKey(lavoro.getIdLavoro()) != null) {
            System.out.println("Errore:" + "Job already exists");
            return 1;
        }
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "Insert into " + this.tableName + " (IdLavoro,Nome,Tipologia,TipologiaTrattamento)"
                + " values(?,?,?,?)";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, lavoro.getIdLavoro());
            pStatement.setString(2, lavoro.getNome());
            pStatement.setString(3, lavoro.getTipologia());
            pStatement.setString(4, lavoro.getTipologiaTrattamento().orElse(null));
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return -1;
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return 0;
    }

    /**
     * Ricerca di un lavoro per chiave primaria.
     * @return il lavoro cercato se esiste, null altrimenti.
     */
    public Lavoro findByPrimaryKey(final int idLavoro) {
        Lavoro lavoro = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select * from " + this.tableName + " where IdLavoro=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, idLavoro);
            result = pStatement.executeQuery();
            if (result.next()) {
                lavoro = new Lavoro();
                lavoro.setIdLavoro(result.getInt("IdLavoro"));
                lavoro.setNome(result.getString("Nome"));
                lavoro.setTipologia(result.getString("Tipologia"));
                lavoro.setTipologiaTrattamento(Optional.ofNullable(result.getString("TipologiaTrattamento")));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return lavoro;
    }

    /**
     * Trova tutti i lavori.
     * @return la lista di tutti i lavori.
     */
    public List<Lavoro> findAll() {
        List<Lavoro>  lavori = null;
        Lavoro lav = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "select * from " + this.tableName;
        ResultSet result = null;
        try {
            pStatement = connection.prepareStatement(query);
            result = pStatement.executeQuery();
            lavori = new LinkedList<>();
            while (result.next()) {
                lav = new Lavoro();
                lav.setIdLavoro(result.getInt("IdLavoro"));
                lav.setNome(result.getString("Nome"));
                lav.setTipologia(result.getString("Tipologia"));
                lav.setTipologiaTrattamento(Optional.ofNullable(result.getString("TipologiaTrattamento")));
                lavori.add(lav);
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return lavori;
    }

    /**
     * Trova tutti i lavori filtrati per tipologia.
     * @param tipologia la tipologia per cui filtrare.
     * @return i lavori cercati.
     */
    public List<Lavoro> findAllWithFilter(final String tipologia) {
        List<Lavoro>  lavori = null;
        Lavoro lav = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "select * from " + this.tableName + " where Tipologia=?";
        ResultSet result = null;
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setString(1, tipologia);
            result = pStatement.executeQuery();
            lavori = new LinkedList<>();
            while (result.next()) {
                lav = new Lavoro();
                lav.setIdLavoro(result.getInt("IdLavoro"));
                lav.setNome(result.getString("Nome"));
                lav.setTipologia(result.getString("Tipologia"));
                lav.setTipologiaTrattamento(Optional.ofNullable(result.getString("TipologiaTrattamento")));
                lavori.add(lav);
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return lavori;
    }

    /**
     * @return i nomi degli attributi della tabella.
     */
    public String[] getAttributes() {
        return new String[] {"IdLavoro", "Nome", "Tipologia", "TipologiaTrattamento"};
    }
}
