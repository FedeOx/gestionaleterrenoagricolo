package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javafx.util.Pair;
import model.Macchinario;

/**
 * Classe che rappresenta la tabellla Macchinari nel db.
 */
public final class MacchinarioTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyMacchinarioTable {
        private static final MacchinarioTable SINGLETON = new MacchinarioTable();
    }

    private MacchinarioTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Macchinari";
    }

    public static MacchinarioTable getTable() {
        return LazyMacchinarioTable.SINGLETON;
    }

    /**
     * Memorizza nel DB un macchinario.
     * @param macchinario il macchinario da memorizzare.
     * @return 1 nel caso in cui l'id del macchinario sia già esistente nel DB, -1 in caso di errore di accesso a DB, 0 in caso di inserimento corretto.
     */
    public int persist(final Macchinario macchinario) {
        if (findByPrimaryKey(macchinario.getIdMacchinario()) != null) {
            System.out.println("Errore:" + "Machinery already exists");
            return 1;
        }
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "Insert into " + this.tableName + " (IdMacchinario,Nome)"
                + " values(?,?)";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, macchinario.getIdMacchinario());
            pStatement.setString(2, macchinario.getNome());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return -1;
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return 0;
    }

    /**
     * Restituisce uno specifico macchinario.
     * @param idMacchinario l'id del macchinario da cercare.
     * @return il macchinario cercato, null se non esiste.
     */
    public Macchinario findByPrimaryKey(final int idMacchinario) {
        Macchinario macchinario = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        ResultSet result = null;
        final  String query = "select * from " + this.tableName + " where IdMacchinario=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, idMacchinario);
            result = pStatement.executeQuery();
            if (result.next()) {
                macchinario = new Macchinario();
                macchinario.setIdMacchinario(result.getInt("IdMacchinario"));
                macchinario.setNome(result.getString("Nome"));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {

            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return macchinario;
    }

    /**
     * Restituisce tutti i macchinari.
     * @return la lista di tutti i macchinari.
     */
    public List<Macchinario> findAll() {
        List<Macchinario>  macchinari = null;
        Macchinario macc = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "select * from " + this.tableName;
        ResultSet result = null;
        try {
            pStatement = connection.prepareStatement(query);
            result = pStatement.executeQuery();
            macchinari = new LinkedList<>();
            while (result.next()) {
                macc = new Macchinario();
                macc.setIdMacchinario(result.getInt("IdMacchinario"));
                macc.setNome(result.getString("Nome"));
                macchinari.add(macc);
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return macchinari;
    }

    /**
     * Restituisce tutti i macchinari necessari per un lavoro.
     * @param idLavoro il lavoro per cui cercare i macchinari necessari.
     * @return i macchinari necessari, null se non sono specificati.
     */
    public List<Macchinario> findAllWithFilter(final int idLavoro) {
        List<Macchinario>  macchinari = null;
        Macchinario macc = null;
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement pStatement = null;
        final String query = "select M.IdMacchinario, M.Nome from " + this.tableName + " M join Macchin_Lavori ML on M.IdMacchinario=ML.IdMacchinario"
                + " join Lavori L on ML.IdLavoro=L.IdLavoro"
                + " where L.IdLavoro=?";
        ResultSet result = null;
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, idLavoro);
            result = pStatement.executeQuery();
            macchinari = new LinkedList<>();
            while (result.next()) {
                macc = new Macchinario();
                macc.setIdMacchinario(result.getInt("IdMacchinario"));
                macc.setNome(result.getString("Nome"));
                macchinari.add(macc);
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return macchinari;
    }

    /**
     * Restituisce tutti i traini che sono state fatti in una certa data,
     * esplicitando anche telaio e nome del trattore che ha eseguito il traino.
     * @return l'intestazione della tabella JTable e i dati.
     */
    public Pair<List<String>, Vector<Vector<String>>> machineryUsage(final Date data) {
        final Connection connection = this.dataSource.getSqlServerConnection();
        PreparedStatement statement = null;
        Vector<Vector<String>> finalVec = null;
        List<String> columnsNames = null;
        ResultSet result = null;
        final String query = "select D.Nome as NomeDipendete, D.Cognome,M.Nome as NomeMezzo, M.Telaio, Ma.Nome as NomeMacchinario, convert(date,T.Data) as Data"
        + " from Dipendenti D join Guide G on D.IdDipendente=G.IdDipendente join Mezzi M on G.TelaioMezzo = M.Telaio join Traini T on M.Telaio=T.TelaioMezzo join " + this.tableName + " Ma on T.IdMacchinario=Ma.IdMacchinario"
        + " where convert(date,T.Data) = ?";
        try {
            statement = connection.prepareStatement(query);
            statement.setDate(1, new java.sql.Date(data.getTime()));
            result = statement.executeQuery();
            final ResultSetMetaData rsmd = result.getMetaData();
            columnsNames = new ArrayList<>(); 
            for (int i = 1; i <= rsmd.getColumnCount(); ++i) {
                columnsNames.add(rsmd.getColumnName(i));
            }
            finalVec = new Vector<>();
            while (result.next()) {
                final Vector<String> vec = new Vector<>();
                vec.add(result.getString("NomeDipendete"));
                vec.add(result.getString("Cognome"));
                vec.add(result.getString("NomeMezzo"));
                vec.add(result.getString("Telaio"));
                vec.add(result.getString("NomeMacchinario"));
                vec.add(new SimpleDateFormat("dd/MM/yyyy").format(new Date(result.getDate("Data").getTime())));
                finalVec.add(vec);
            }
        } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore" + e.getMessage());
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                    if (result != null) {
                        result.close();
                    }
                } catch (SQLException e) {
                    new Exception(e.getMessage());
                         System.out.println("Errore" + e.getMessage());
                }
            }
            return new Pair<>(columnsNames, finalVec);
    } 

    /**
     * @return i nomi degli attributi della tabella.
     */
    public String[] getAttributes() {
        return new String[] {"IdMacchinario", "Nome"};
    }
}
