package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Classe che rappresenta l'entità MACCHINARIO.
 */
public class Macchinario {
    private int idMacchinario;
    private String nome;

    /**
     * @return l'idMacchinario.
     */
    public int getIdMacchinario() {
        return this.idMacchinario;
    }
    /**
     * @param idMacchinario l'idMacchinario da settare.
     */
    public void setIdMacchinario(final int idMacchinario) {
        this.idMacchinario = idMacchinario;
    }
    /**
     * @return il nome.
     */
    public String getNome() {
        return this.nome;
    }
    /**
     * @param nome il nome da settare.
     */
    public void setNome(final String nome) {
        this.nome = nome;
    }
    /**
     * Restituisce la lista degli attributi associati a questo oggetto.
     */
    public List<?> concatAttributes() {
        return new ArrayList<>(Arrays.asList(this.idMacchinario, this.nome));
    }
}
