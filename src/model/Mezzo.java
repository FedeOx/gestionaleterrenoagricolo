package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Classe che rappresenta l'entità MEZZO.
 */
public class Mezzo {
    private String telaio;
    private String nome;
    private Optional<String> targa;
    private String tipologia;

    /**
     * @return il telaio.
     */
    public String getTelaio() {
        return this.telaio;
    }
    /**
     * @param telaio il telaio da settare.
     */
    public void setTelaio(final String telaio) {
        this.telaio = telaio;
    }
    /**
     * @return il nome.
     */
    public String getNome() {
        return this.nome;
    }
    /**
     * @param nome il nome da settare.
     */
    public void setNome(final String nome) {
        this.nome = nome;
    }
    /**
     * @return la targa.
     */
    public Optional<String> getTarga() {
        return this.targa;
    }
    /**
     * @param targa la targa da settare.
     */
    public void setTarga(final Optional<String> targa) {
        this.targa = targa;
    }
    /**
     * @return la tipologia.
     */
    public String getTipologia() {
        return this.tipologia;
    }
    /**
     * @param tipologia la tipologia da settare.
     */
    public void setTipologia(final String tipologia) {
        this.tipologia = tipologia;
    }
    /**
     * Restituisce la lista degli attributi associati a questo oggetto.
     */
    public List<?> concatAttributes() {
        return new ArrayList<>(Arrays.asList(this.telaio, this.nome, this.targa.orElse("NULL"), this.tipologia));
    }
}
