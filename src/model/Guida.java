package model;

import java.util.Date;

/**
 * Classe che rappresenta l'entità GUIDA.
 */
public class Guida {
    private String telaioMezzo;
    private Date data;
    private int idDipendente;
    /**
     * @return il telaioMezzo.
     */
    public String getTelaioMezzo() {
        return this.telaioMezzo;
    }
    /**
     * @param telaioMezzo il telaioMezzo da settare.
     */
    public void setTelaioMezzo(final String telaioMezzo) {
        this.telaioMezzo = telaioMezzo;
    }
    /**
     * @return la data.
     */
    public Date getData() {
        return this.data;
    }
    /**
     * @param data la data da settare.
     */
    public void setData(final Date data) {
        this.data = data;
    }
    /**
     * @return l'idDipendente.
     */
    public int getIdDipendente() {
        return this.idDipendente;
    }
    /**
     * @param idDipendente l'idDipendente da settare.
     */
    public void setIdDipendente(final int idDipendente) {
        this.idDipendente = idDipendente;
    }
}
