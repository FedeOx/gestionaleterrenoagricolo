package model;

import java.util.Date;

/**
 * Classe che rappresenta l'entità ACQUISTO.
 */
public class Acquisto {
    private int idAcquisto;
    private Date data;
    private double prezzoTotale;
    private int idDipendente;
    private int idCliente;

    /**
     * @return l'ID dell'acquisto.
     */
    public int getIdAcquisto() {
        return this.idAcquisto;
    }
    /**
     * @param idAcquisto
     */
    public void setIdAcquisto(final int idAcquisto) {
        this.idAcquisto = idAcquisto;
    }
    /**
     * @return
     *         la data dell'acquisto.
     */
    public Date getData() {
        return this.data;
    }
    /**
     * @param data
     * 
     */
    public void setData(final Date data) {
        this.data = data;
    }
    /**
     * @return l'importo totale dell'acquisto.
     */
    public double getPrezzoTotale() {
        return this.prezzoTotale;
    }
    /**
     * @param prezzoTotale
     */
    public void setPrezzoTotale(final double prezzoTotale) {
        this.prezzoTotale = prezzoTotale;
    }
    /**
     * @return l'ID del {@link Dipendente} che ha gestito l'acquisto.
     */
    public int getIdDipendente() {
        return this.idDipendente;
    }
    /** 
     * @param idDipendente
     */
    public void setIdDipendente(final int idDipendente) {
        this.idDipendente = idDipendente;
    }
    /**
     * @return l'ID del {@link Cliente} che ha effettuato l'acquisto.
     */
    public int getIdCliente() {
        return this.idCliente;
    }
    /**
     * @param idCliente
     */
    public void setIdCliente(final int idCliente) {
        this.idCliente = idCliente;
    }
}
