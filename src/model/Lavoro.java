package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Classe che rappresenta l'entità LAVORO.
 */
public class Lavoro {
    private int idLavoro;
    private String nome;
    private String tipologia;
    private Optional<String> tipologiaTrattamento;

    /**
     * @return l'idLavoro.
     */
    public int getIdLavoro() {
        return this.idLavoro;
    }
    /**
     * @param idLavoro l'idLavoro da settare.
     */
    public void setIdLavoro(final int idLavoro) {
        this.idLavoro = idLavoro;
    }
    /**
     * @return il nome.
     */
    public String getNome() {
        return this.nome;
    }
    /**
     * @param nome il nome da settare.
     */
    public void setNome(final String nome) {
        this.nome = nome;
    }
    /**
     * @return la tipologia.
     */
    public String getTipologia() {
        return this.tipologia;
    }
    /**
     * @param tipologia la tipologia da settare.
     */
    public void setTipologia(final String tipologia) {
        this.tipologia = tipologia;
    }
    /**
     * @return la tipologia.
     */
    public Optional<String> getTipologiaTrattamento() {
        return this.tipologiaTrattamento;
    }
    /**
     * @param tipologia la tipologia da settare.
     */
    public void setTipologiaTrattamento(final Optional<String> tipologia) {
        this.tipologiaTrattamento = tipologia;
    }
    /**
     * Restituisce la lista degli attributi associati a questo oggetto.
     */
    public List<?> concatAttributes() {
        return new ArrayList<>(Arrays.asList(this.idLavoro, this.nome, this.tipologia, this.tipologiaTrattamento.orElse("NULL")));
    }
}
