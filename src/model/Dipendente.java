package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Classe che rappresenta l'entità DIPENDENTE.
 */
public class Dipendente {
    private int idDipendente;
    private String nome;
    private String cognome;
    private String via;
    private String numero;
    private String citta;
    private String telefono;
    private double stipendio;
    private String tipologia;
    /**
     * @return l'idDipendente.
     */
    public int getIdDipendente() {
        return this.idDipendente;
    }
    /**
     * @param idDipendente l'idDipendente da settare.
     */
    public void setIdDipendente(final int idDipendente) {
        this.idDipendente = idDipendente;
    }
    /**
     * @return il nome.
     */
    public String getNome() {
        return this.nome;
    }
    /**
     * @param nome il nome da settare.
     */
    public void setNome(final String nome) {
        this.nome = nome;
    }
    /**
     * @return il cognome.
     */
    public String getCognome() {
        return this.cognome;
    }
    /**
     * @param cognome il cognome da settare.
     */
    public void setCognome(final String cognome) {
        this.cognome = cognome;
    }
    /**
     * @return la via.
     */
    public String getVia() {
        return this.via;
    }
    /**
     * @param via la via da settare.
     */
    public void setVia(final String via) {
        this.via = via;
    }
    /**
     * @return il numero.
     */
    public String getNumero() {
        return this.numero;
    }
    /**
     * @param numero il numero da settare.
     */
    public void setNumero(final String numero) {
        this.numero = numero;
    }
    /**
     * @return la citta.
     */
    public String getCitta() {
        return this.citta;
    }
    /**
     * @param citta la citta da settare.
     */
    public void setCitta(final String citta) {
        this.citta = citta;
    }
    /**
     * @return il telefono.
     */
    public String getTelefono() {
        return this.telefono;
    }
    /**
     * @param telefono il telefono da settare.
     */
    public void setTelefono(final String telefono) {
        this.telefono = telefono;
    }
    /**
     * @return lo stipendio.
     */
    public double getStipendio() {
        return this.stipendio;
    }
    /**
     * @param stipendio lo stipendio da settare.
     */
    public void setStipendio(final double stipendio) {
        this.stipendio = stipendio;
    }
    /**
     * @return la tipologia.
     */
    public String getTipologia() {
        return this.tipologia;
    }
    /**
     * @param tipologia la tipologia da settare.
     */
    public void setTipologia(final String tipologia) {
        this.tipologia = tipologia;
    }
    /**
     * Restituisce la lista degli attributi associati a questo oggetto.
     */
    public List<?> concatAttributes() {
        return new ArrayList<>(Arrays.asList(this.idDipendente, this.nome, this.cognome, this.via, this.numero,
                this.citta, this.telefono, this.stipendio, this.tipologia));
    }
}
