package model;

/**
 * Classe che rappresenta l'entità Prod_Tratt.
 */
public class ProdTratt {
    private int idLavoro;
    private int idProdotto;
    /**
     * @return l'idLavoro.
     */
    public int getIdLavoro() {
        return this.idLavoro;
    }
    /**
     * @param idLavoro l'idLavoro da settare.
     */
    public void setIdLavoro(final int idLavoro) {
        this.idLavoro = idLavoro;
    }
    /**
     * @return l'idProdotto.
     */
    public int getIdProdotto() {
        return this.idProdotto;
    }
    /**
     * @param idProdotto l'idProdotto da settare.
     */
    public void setIdProdotto(final int idProdotto) {
        this.idProdotto = idProdotto;
    }
}
