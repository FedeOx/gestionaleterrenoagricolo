package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Classe che rappresenta l'entità PRODOTTO.
 */
public class Prodotto {
    private int idProdotto;
    private String nome;
    private double prezzo;
    private String periodoTipico;
    private int tempoMaturazione;
    private String terrenoIdeale;
    private int scorte;
    private boolean bio;

    /**
     * @return l'idProdotto.
     */
    public int getIdProdotto() {
        return this.idProdotto;
    }
    /**
     * @param idProdotto l'idProdotto da settare.
     */
    public void setIdProdotto(final int idProdotto) {
        this.idProdotto = idProdotto;
    }
    /**
     * @return il nome.
     */
    public String getNome() {
        return this.nome;
    }
    /**
     * @param nome il nome da settare.
     */
    public void setNome(final String nome) {
        this.nome = nome;
    }
    /**
     * @return il prezzo.
     */
    public double getPrezzo() {
        return this.prezzo;
    }
    /**
     * @param prezzo il prezzo da settare.
     */
    public void setPrezzo(final double prezzo) {
        this.prezzo = prezzo;
    }
    /**
     * @return il periodoTipico di semina.
     */
    public String getPeriodoTipico() {
        return this.periodoTipico;
    }
    /**
     * @param periodoTipico il periodoTipico di semina da settare.
     */
    public void setPeriodoTipico(final String periodoTipico) {
        this.periodoTipico = periodoTipico;
    }
    /**
     * @return il tempoMaturazione.
     */
    public int getTempoMaturazione() {
        return this.tempoMaturazione;
    }
    /**
     * @param tempoMaturazione il tempoMaturazione da settare.
     */
    public void setTempoMaturazione(final int tempoMaturazione) {
        this.tempoMaturazione = tempoMaturazione;
    }
    /**
     * @return il terrenoIdeale.
     */
    public String getTerrenoIdeale() {
        return this.terrenoIdeale;
    }
    /**
     * @param terrenoIdeale il terrenoIdeale da settare.
     */
    public void setTerrenoIdeale(final String terrenoIdeale) {
        this.terrenoIdeale = terrenoIdeale;
    }
    /**
     * @return le scorte.
     */
    public int getScorte() {
        return this.scorte;
    }
    /**
     * @param scorte le scorte da settare.
     */
    public void setScorte(final int scorte) {
        this.scorte = scorte;
    }
    /**
     * @return true se è un prodotto biologico.
     */
    public boolean isBio() {
        return this.bio;
    }
    /**
     * @param bio true se il prodotto è biologico.
     */
    public void setBio(final boolean bio) {
        this.bio = bio;
    }
    /**
     * Restituisce la lista degli attributi associati a questo oggetto customizzati per Venditori.
     */
    public List<?> concatAttributesCustom() {
        return new ArrayList<>(Arrays.asList(this.idProdotto, this.nome, this.prezzo, this.scorte, this.bio));
    }
    /**
     * Restituisce la lista degli attributi associati a questo oggetto.
     */
    public List<?> concatAttributes() {
        return new ArrayList<>(Arrays.asList(this.idProdotto, this.nome, this.prezzo, this.periodoTipico,
                this.tempoMaturazione, this.terrenoIdeale, this.scorte, this.bio));
    }
}
