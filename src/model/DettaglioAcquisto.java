package model;

import java.util.Optional;

/**
 * Classe che rappresenta l'entità DETTAGLIO_ACQUISTO.
 */
public class DettaglioAcquisto {
    private int idProdotto;
    private int idAcquisto;
    private int quantita;
    private double prezzoUnitario;
    private double prezzoTotale;
    private Optional<Integer> sconto;

    /**
     * @return l' idProdotto.
     */
    public int getIdProdotto() {
        return idProdotto;
    }
    /**
     * @param idProdotto l' idProdotto da settare.
     */
    public void setIdProdotto(final int idProdotto) {
        this.idProdotto = idProdotto;
    }
    /**
     * @return l' idAcquisto.
     */
    public int getIdAcquisto() {
        return idAcquisto;
    }
    /**
     * @param idAcquisto l' idAcquisto da settare.
     */
    public void setIdAcquisto(final int idAcquisto) {
        this.idAcquisto = idAcquisto;
    }
    /**
     * @return la quantita.
     */
    public int getQuantita() {
        return quantita;
    }
    /**
     * @param quantita la quantita da settare.
     */
    public void setQuantita(final int quantita) {
        this.quantita = quantita;
    }
    /**
     * @return il prezzoUnitario.
     */
    public double getPrezzoUnitario() {
        return prezzoUnitario;
    }
    /**
     * @param prezzoUnitario il prezzoUnitario da settare.
     */
    public void setPrezzoUnitario(final double prezzoUnitario) {
        this.prezzoUnitario = prezzoUnitario;
    }
    /**
     * @return il prezzoTotale.
     */
    public double getPrezzoTotale() {
        return prezzoTotale;
    }
    /**
     * @param prezzoTotale il prezzoTotale da settare.
     */
    public void setPrezzoTotale(final double prezzoTotale) {
        this.prezzoTotale = prezzoTotale;
    }
    /**
     * @return lo sconto.
     */
    public Optional<Integer> getSconto() {
        return sconto;
    }
    /**
     * @param sconto lo sconto da settare.
     */
    public void setSconto(final Optional<Integer> sconto) {
        this.sconto = sconto;
    }
}
