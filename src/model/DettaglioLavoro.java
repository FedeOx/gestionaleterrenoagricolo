package model;

import java.util.Date;
import java.util.Optional;

/**
 * Classe che rappresenta l'entità DETTAGLIO_LAVORO.
 */
public class DettaglioLavoro {
    private int idDettaglioLavoro;
    private Date dataInizio;
    private int idLavoro;
    private int idMacchinario;
    private Optional<Date> dataFine;
    /**
     * @return il idDettaglioLavoro.
     */
    public int getIdDettaglioLavoro() {
        return this.idDettaglioLavoro;
    }
    /**
     * @param idDettaglioLavoro il idDettaglioLavoro da settare.
     */
    public void setIdDettaglioLavoro(final int idDettaglioLavoro) {
        this.idDettaglioLavoro = idDettaglioLavoro;
    }
    /**
     * @return la dataInizio.
     */
    public Date getDataInizio() {
        return this.dataInizio;
    }
    /**
     * @param dataInizio la dataInizio da settare.
     */
    public void setDataInizio(final Date dataInizio) {
        this.dataInizio = dataInizio;
    }
    /**
     * @return l'idLavoro.
     */
    public int getIdLavoro() {
        return this.idLavoro;
    }
    /**
     * @param idLavoro l'idLavoro da settare.
     */
    public void setIdLavoro(final int idLavoro) {
        this.idLavoro = idLavoro;
    }
    /**
     * @return l'idMacchinario.
     */
    public int getIdMacchinario() {
        return this.idMacchinario;
    }
    /**
     * @param idMacchinario l'idMacchinario da settare.
     */
    public void setIdMacchinario(final int idMacchinario) {
        this.idMacchinario = idMacchinario;
    }
    /**
     * @return la dataFine.
     */
    public Optional<Date> getDataFine() {
        return this.dataFine;
    }
    /**
     * @param dataFine la dataFine da settare.
     */
    public void setDataFine(final Optional<Date> dataFine) {
        this.dataFine = dataFine;
    }
}
