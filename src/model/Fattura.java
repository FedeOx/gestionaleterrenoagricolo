package model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Classe che rappresenta l'entità FATTURA.
 */
public class Fattura {
    private int numero;
    private Date dataEmissione;
    private double importo;
    private int idCliente;

    /**
     * @return il numero.
     */
    public int getNumero() {
        return this.numero;
    }
    /**
     * @param numero il numero da settare.
     */
    public void setNumero(final int numero) {
        this.numero = numero;
    }
    /**
     * @return la dataEmissione.
     */
    public Date getDataEmissione() {
        return this.dataEmissione;
    }
    /**
     * @param dataEmissione la dataEmissione da settare.
     */
    public void setDataEmissione(final Date dataEmissione) {
        this.dataEmissione = dataEmissione;
    }
    /**
     * @return l'importo.
     */
    public double getImporto() {
        return this.importo;
    }
    /**
     * @param importo l'importo da settare.
     */
    public void setImporto(final double importo) {
        this.importo = importo;
    }
    /**
     * @return l'idCliente.
     */
    public int getIdCliente() {
        return this.idCliente;
    }
    /**
     * @param idCliente l'idCliente da settare.
     */
    public void setIdCliente(final int idCliente) {
        this.idCliente = idCliente;
    }

    /**
     * Restituisce la lista degli attributi associati a questo oggetto.
     */
    public List<?> concatAttributes() {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALIAN);
        return new ArrayList<>(Arrays.asList(this.numero, dateFormat.format(this.dataEmissione), this.importo, this.idCliente));
    }
}
