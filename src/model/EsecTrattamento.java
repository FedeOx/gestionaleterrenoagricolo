package model;

/**
 * Classe che rappresenta l'entità FATTURA.
 */
public class EsecTrattamento {
    private int idProdSem;
    private int idDettaglioLavoro;
    /**
     * @return l'idProdSem
     */
    public int getIdProdSem() {
        return this.idProdSem;
    }
    /**
     * @param idProdSem l'idProdSem da settare.
     */
    public void setIdProdSem(final int idProdSem) {
        this.idProdSem = idProdSem;
    }
    /**
     * @return l'idDettaglioLavoro
     */
    public int getIdDettaglioLavoro() {
        return this.idDettaglioLavoro;
    }
    /**
     * @param idDettaglioLavoro l'idDettaglioLavoro da settare.
     */
    public void setIdDettaglioLavoro(final int idDettaglioLavoro) {
        this.idDettaglioLavoro = idDettaglioLavoro;
    }

}
