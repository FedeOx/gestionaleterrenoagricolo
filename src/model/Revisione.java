package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Classe che rappresenta l'entità REVISIONE.
 */
public class Revisione {
    private String telaioMezzo;
    private Date data;
    private double costo;
    private Optional<String> descrizione;

    /**
     * @return il telaioMezzo.
     */
    public String getTelaioMezzo() {
        return this.telaioMezzo;
    }
    /**
     * @param telaioMezzo il telaioMezzo da settare.
     */
    public void setTelaioMezzo(final String telaioMezzo) {
        this.telaioMezzo = telaioMezzo;
    }
    /**
     * @return la data.
     */
    public Date getData() {
        return this.data;
    }
    /**
     * @param data la data da settare.
     */
    public void setData(final Date data) {
        this.data = data;
    }
    /**
     * @return il costo.
     */
    public double getCosto() {
        return this.costo;
    }
    /**
     * @param costo il costo da settare.
     */
    public void setCosto(final double costo) {
        this.costo = costo;
    }
    /**
     * @return la descrizione.
     */
    public Optional<String> getDescrizione() {
        return this.descrizione;
    }
    /**
     * @param descrizione la descrizione da settare.
     */
    public void setDescrizione(final Optional<String> descrizione) {
        this.descrizione = descrizione;
    }
    /**
     * Restituisce la lista degli attributi associati a questo oggetto.
     */
    public List<?> concatAttributes() {
        return new ArrayList<>(Arrays.asList(this.telaioMezzo, this.data, this.costo, this.descrizione.orElse("NULL")));
    }
}
