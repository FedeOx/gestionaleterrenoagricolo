package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Classe che rappresenta l'entità CLIENTE.
 */
public class Cliente {
    private int idCliente;
    private String nome;
    private String cognome;
    private String via;
    private String numero;
    private String citta;
    private String cap;
    private String telefono;

    /**
     * @return l'idCliente.
     */
    public int getIdCliente() {
        return this.idCliente;
    }
    /**
     * @param idCliente l'idCliente da settare.
     */
    public void setIdCliente(final int idCliente) {
        this.idCliente = idCliente;
    }
    /**
     * @return il nome.
     */
    public String getNome() {
        return this.nome;
    }
    /**
     * @param nome il nome da settare.
     */
    public void setNome(final String nome) {
        this.nome = nome;
    }
    /**
     * @return il cognome.
     */
    public String getCognome() {
        return this.cognome;
    }
    /**
     * @param cognome il cognome da settare.
     */
    public void setCognome(final String cognome) {
        this.cognome = cognome;
    }
    /**
     * @return la via.
     */
    public String getVia() {
        return this.via;
    }
    /**
     * @param via la via da settare.
     */
    public void setVia(final String via) {
        this.via = via;
    }
    /**
     * @return il numero civico.
     */
    public String getNumero() {
        return this.numero;
    }
    /**
     * @param numero il numero civico da settare.
     */
    public void setNumero(final String numero) {
        this.numero = numero;
    }
    /**
     * @return la citta.
     */
    public String getCitta() {
        return this.citta;
    }
    /**
     * @param citta la citta da settare.
     */
    public void setCitta(final String citta) {
        this.citta = citta;
    }
    /**
     * @return il cap.
     */
    public String getCap() {
        return this.cap;
    }
    /**
     * @param cap il cap da settare.
     */
    public void setCap(final String cap) {
        this.cap = cap;
    }
    /**
     * @return il telefono.
     */
    public String getTelefono() {
        return this.telefono;
    }
    /**
     * @param telefono il telefono da settare.
     */
    public void setTelefono(final String telefono) {
        this.telefono = telefono;
    }
    /**
     * Restituisce la lista degli attributi associati a questo oggetto.
     */
    public List<?> concatAttributes() {
        return new ArrayList<>(Arrays.asList(this.idCliente, this.nome, this.cognome, this.via, this.numero, this.citta, this.cap, this.telefono));
    }
}
