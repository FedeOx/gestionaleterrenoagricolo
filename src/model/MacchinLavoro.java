package model;

/**
 * Classe che rappresenta la tabellla Macchin_Lavoro nel db.
 */
public class MacchinLavoro {
    private int idLavoro;
    private int idMacchinario;
    /**
     * @return l'idLavoro.
     */
    public int getIdLavoro() {
        return idLavoro;
    }
    /**
     * @param idLavoro l' idLavoro da settare.
     */
    public void setIdLavoro(final int idLavoro) {
        this.idLavoro = idLavoro;
    }
    /**
     * @return l' idMacchinario.
     */
    public int getIdMacchinario() {
        return idMacchinario;
    }
    /**
     * @param idMacchinario l' idMacchinario da settare.
     */
    public void setIdMacchinario(final int idMacchinario) {
        this.idMacchinario = idMacchinario;
    }
}
