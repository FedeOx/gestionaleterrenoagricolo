package model;

import java.util.Date;

/**
 * Classe che rappresenta l'entità TRAINO.
 */
public class Traino {
    private int idMacchinario;
    private String telaioMezzo;
    private Date data;
    /**
     * @return l'idMacchinario.
     */
    public int getIdMacchinario() {
        return idMacchinario;
    }
    /**
     * @param idMacchinario l'idMacchinario da settare.
     */
    public void setIdMacchinario(final int idMacchinario) {
        this.idMacchinario = idMacchinario;
    }
    /**
     * @return il telaioMezzo.
     */
    public String getTelaioMezzo() {
        return telaioMezzo;
    }
    /**
     * @param telaioMezzo il telaioMezzo da settare.
     */
    public void setTelaioMezzo(final String telaioMezzo) {
        this.telaioMezzo = telaioMezzo;
    }
    /**
     * @return la data.
     */
    public Date getData() {
        return data;
    }
    /**
     * @param data la data da settare.
     */
    public void setData(final Date data) {
        this.data = data;
    }
}
